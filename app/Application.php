<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;
    const STATE_UNPAID = 'unpaid';
    const STATE_SONGS_PAID = 'paid';
    const STATE_DEFERRED = 'deferred';
    const STATE_SUBMITTED = 'submitted';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['gender', 'performing_name', 'performing_rights_association', 'bio', 'city', 'state'];
    protected $required = ['performing_name', 'bio', 'gender', 'profile_image_file_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the user that owns the file.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get songs in the application
     */
    public function songs()
    {
        return $this->hasMany('App\Song');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    /**
     * Get songs in the application
     */
    public function profileImage()
    {
        return $this->belongsTo('App\File', 'profile_image_file_id');
    }

    /**
     * Get songs in the application
     */
    public function parentAuth()
    {
        return $this->belongsTo('App\File', 'parent_auth_file_id');
    }

    /**
     * Get songs in the application
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function isValid()
    {
        if (\Config('app.skipApplicationPage')) {
            return true;
        }
        foreach ($this->required as $fill) {
            if (empty($this->$fill)) {
                return false;
            }
        }

        return true;
    }
    public function songsAreValid()
    {
        $isValid = true;
        foreach ($this->songs as $song) {
            if (! $song->isValid()) {
                $isValid = false;
                break;
            }
        }

        return $isValid;
    }
    public function songCountIsValid()
    {
        return $this->songs->count() > 0;
    }
    public function songCount()
    {
        return $this->songs->count();
    }
    public function getTotalPaidAttribute()
    {
        $total = 0;
        foreach ($this->transactions as $transaction) {
            if ($transaction->isSuccessful) {
                $total += $transaction->amount;
            }
        }

        return $total / 100;
    }
    public function getFormattedStatusAttribute()
    {
        if ($this->status == self::STATE_SONGS_PAID) {
            return 'Paid';
        } elseif ($this->status == self::STATE_UNPAID) {
            return 'Started';
        } elseif ($this->status == self::STATE_DEFERRED) {
            return 'Deferred Payment';
        } elseif ($this->status == self::STATE_SUBMITTED) {
            return 'Submitted';
        }

        return 'Unknown';
    }
    public function getBillAmountAttribute()
    {
        return \App\Helpers\General::filteredSongsCount($this->songs) * 50 * 100;
    }
    public function billAmount()
    {
        return $this->getBillAmountAttribute();
    }
    public function canBeSubmitted()
    {
        if ($this->status == static::STATE_SUBMITTED) {
            return false;
        }

        return true;
    }

    public function getUniqueCategories()
    {
        $cats = [];
        foreach ($this->songs as $song) {
            if ($song->category) {
                if (! array_key_exists($song->category->id, $cats)) {
                    $cats[$song->category->id] = $song->category;
                }
            }
        }

        return collect($cats);
    }
    public function getFormattedIdAttribute()
    {
        return config('app.applicationPrefix') . '-' . $this->id;
    }
}
