<?php
namespace App\Console\Commands;

use App\Category;
use Illuminate\Console\Command;

class Soundtracks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'soundtracks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create empty parent row for Soundtrack category. For multi track judging. ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $category = Category::where('name', 'Best Soundtrack Album')->firstOrFail();
        $tracks = \App\Song::where('category_id', $category->id)->orderBy('application_id')->orderBy('id')->get();
        foreach($tracks as $track){
            $this->comment("Starting {$track->id} - {$track->title}");
            \App::make('EloquentService')->processParentSong($category, $track, $this->output);
        }
        $this->comment("Finish. ");
    }
}
