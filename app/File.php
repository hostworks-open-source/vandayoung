<?php

namespace App;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    const MANIFEST_STATUS_SENT = 'sent';
    const MANIFEST_STATUS_SENDING = 'sending';
    const MANIFEST_STATUS_SUCCESS = 'success';
    const MANIFEST_STATUS_FAILED = 'failed';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'google_storage_key', 'name_sha1', 'file_sha1', 'filesize', 'mime_type', 'manifest_status'];

    /**
     * Get the user that owns the file.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function song()
    {
        return $this->hasOne('App\Song');
    }

    public function profileApplication()
    {
        return $this->hasOne('App\Application', 'profile_image_file_id');
    }

    public function profileUser()
    {
        return $this->hasOne('App\User', 'profile_image_file_id');
    }

    public function parentAuthApplication()
    {
        return $this->hasOne('App\Application', 'parent_auth_file_id');
    }

    public function queueSheetSong()
    {
        return $this->hasOne('App\Song', 'queue_sheet_file_id');
    }

    public function isVideo()
    {
        return General::isVideo($this->name);
    }

    public function getFileExtension()
    {
        return pathinfo($this->name, PATHINFO_EXTENSION);
    }

    public function isProcessing()
    {
        return in_array($this->manifest_status, [static::MANIFEST_STATUS_SENT, static::MANIFEST_STATUS_SENDING]);
    }

    public function isSending()
    {
        return $this->manifest_status == static::MANIFEST_STATUS_SENDING;
    }

    public function hasProcessingError()
    {
        if ($this->manifest_url) {
            return false;
        }

        if ($this->manifest_status == static::MANIFEST_STATUS_SENT && $this->created_at->timestamp > strtotime('-1 day')) {
            return false;
        }

        if ($this->manifest_status == static::MANIFEST_STATUS_FAILED) {
            return true;
        }

        return ! is_null($this->manifest_status) && ($this->created_at->timestamp < strtotime('-1 day'));
    }

    public function canDirectDownload()
    {
        return false; // not anymore.
    }

    public function canStream()
    {
        return $this->manifest_url && $this->manifest_status == static::MANIFEST_STATUS_SUCCESS;
    }

    public function getDirectDownloadUrlAttribute()
    {
        if (! $this->canStream()) {
            return url("/media/download/{$this->name_sha1}/{$this->name}");
        }
    }
}
