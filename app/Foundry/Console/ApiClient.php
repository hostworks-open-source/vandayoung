<?php

namespace App\Foundry\Console;

use GuzzleHttp\Client;
use App\Foundry\Console\Configuration;
use Illuminate\Contracts\Cache\Repository;

class ApiClient
{
    private $configuration;

    private $client;

    private $cache;

    public function __construct(Configuration $configuration, Client $client, Repository $cache)
    {
        $this->configuration = $configuration;
        $this->client = $client;
        $this->cache = $cache;
    }

    public function storage($id)
    {
        $key = 'foundry'.md5('storages'.implode('.', func_get_args()));

        return $this->cache->remember($key, $this->configuration->getCacheTimeout(), function () use ($id) {
            $response = (string) $this->client->get($this->configuration->getApiUrl().'/storage/'.$id, [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->configuration->getApiKey(),
                ],
            ])->getBody();

            return json_decode($response, true);
        });
    }
}
