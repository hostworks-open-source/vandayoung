<?php

namespace App\Foundry\Console;

class Configuration
{
    const API_URL = '';

    const UAT_API_URL = '';

    private $apiKey;

    private $cacheTimeout;

    private $uat;

    public function __construct($apiKey, $cacheTimeout = 60, $uat = false)
    {
        $this->apiKey = $apiKey;
        $this->cacheTimeout = $cacheTimeout;
        $this->uat = $uat;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function getCacheTimeout()
    {
        return $this->cacheTimeout;
    }

    public function getApiUrl()
    {
        return $this->uat ? static::UAT_API_URL : static::API_URL;
    }
}
