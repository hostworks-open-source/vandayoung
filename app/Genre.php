<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'genres';

    /**
     * Get the application
     */
    public function songs()
    {
        return $this->hasMany('App\Songs');
    }
}