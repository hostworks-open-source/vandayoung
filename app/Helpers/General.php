<?php

namespace App\Helpers;

use DateTime;
use DateInterval;
use App\Foundry\Console\ApiClient;

class General
{
    public static function templateWithPrefix($templateName)
    {
        $prefix = config('app.templatePrefix');

        if (view()->exists($view = "{$prefix}/{$templateName}")) {
            return $view;
        }

        return $templateName;
    }

    public static function stateIsAvailable($state)
    {
        $states = explode(',', config('app.availableApplicationStates'));

        return in_array($state, $states);
    }

    public static function filterQueryString($myParams, $otherParams)
    {
        return http_build_query(array_merge($otherParams, $myParams));
    }

    public static function isVideo($fileName)
    {
        return in_array(strtolower(pathinfo($fileName, PATHINFO_EXTENSION)), [
            'mov', 'wmv', 'mp4', 'wma', 'mpeg', 'avi', 'mpg',
        ]);
    }

    public static function appsHaveClosed()
    {
        return time() > strtotime(config('app.applicationCutOff'));
    }

    public static function filteredSongsCount($songs)
    {
        return $songs->filter(function ($item) {
            return $item->children->count() == 0;
        })->count();
    }

    public static function console_storage_config($bucket)
    {
        return app(ApiClient::class)->storage($bucket);
    }

    public static function google_credentials($bucket = null)
    {
        $storage = static::console_storage_config($bucket ?: config('app.foundry_incoming_bucket'));

        return json_decode(data_get($storage, 'data.credentials'), true);
    }

    public static function google_sign_policy($application)
    {
        $storage = static::console_storage_config(config('app.foundry_bucket_incoming'));

        $privateKey = json_decode(data_get($storage, 'data.credentials'))->private_key;

        $policyDocument = sprintf('{"expiration": "%s","conditions": [
          ["starts-with", "$key", "" ],
          {"acl": "bucket-owner-full-control" },
          {"bucket": "%s"},
          {"x-goog-meta-SmaApplicationId": "%s"}
        ]}',
            (new DateTime())->add(new DateInterval('PT1H'))->format(DATE_ISO8601),
            data_get($storage, 'data.base_path'),
            $application->id
        );

        $policy = base64_encode($policyDocument);
        openssl_sign($policy, $messageDigest, $privateKey, OPENSSL_ALGO_SHA256);
        $signature = base64_encode($messageDigest);

        return compact('policy', 'signature');
    }
}
