<?php

namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Panel;
use App\Round;
use App\Score;
use App\Search;
use App\Song;
use App\SongRound;
use App\SongRoundAverage;
use App\User;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\Application;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    const FILTER_ROUND_PENDING = 'round-pending';
    const FILTER_ROUND_COMPLETED = 'round-completed';
    const FILTER_STATUS = 'status';
    const FILTER_ARCHIVED = 'archived';
    const FILTER_COMPLETED_BY_ME = 'completed-by-me';
    const FILTER_COMPLETED_BY_ALL = 'completed-by-all';
    const FILTER_SHORTLISTED = 'shortlisted';
    const FILTER_NOT_IN_ROUND = 'not-in-round';
    const FILTER_PENDING_ASSIGNED_TO_ME = 'pending-assigned-to-me';
    const FILTER_COMPLETED_ASSIGNED_TO_ME = 'completed-assigned-to-me';
    const FILTER_PENDING_BY_ME = 'pending-by-me';
    const FILTER_PAID_APPLICATION = 'paid';
    const SORT_ASC = 'a-z';
    const SORT_DESC = 'z-a';
    const SORT_DATE_DESC = 'date-desc';
    const SORT_DATE_ASC = 'date-asc';
    const SORT_SCORE_ASC = 'score-asc';
    const SORT_SCORE_DESC = 'score-desc';
    const SEARCH_TYPE_NAME = 'name';
    const SEARCH_TYPE_EMAIL = 'email';
    const SEARCH_TYPE_TITLE = 'title';
    const SEARCH_TYPE_APPLICATION_ID = 'application';
    const JUDGE_TYPE_PANEL = 'panel';
    const JUDGE_TYPE_USER = 'user';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is.admin');
    }

    public function dashboard(){
        $startOfThisWeek=date_create()->setTimestamp(strtotime('monday this week'));
        $statsService = \App::make('StatsService');
        $stats = ['applications'=>[
                'today'=>$statsService->getCountOfApplications(date_create(date("Y-m-d")), date_create()),
                'week'=>$statsService->getCountOfApplications($startOfThisWeek, date_create()),
                'total'=>$statsService->getCountOfApplications(date_create('0000-00-00'), date_create()),
            ],'users'=>[
                'today'=>$statsService->getCountOfUsers(date_create(date("Y-m-d")), date_create()),
                'week'=>$statsService->getCountOfUsers($startOfThisWeek, date_create()),
                'total'=>$statsService->getCountOfUsers(date_create('0000-00-00'), date_create()),
            ],'works'=>[
                'today'=>$statsService->getCountOfWorks(date_create(date("Y-m-d")), date_create()),
                'week'=>$statsService->getCountOfWorks($startOfThisWeek, date_create()),
                'total'=>$statsService->getCountOfWorks(date_create('0000-00-00'), date_create()),
            ],'worksSubmitted'=>[
                'today'=>$statsService->getCountOfWorksWithStatus(date_create(date("Y-m-d")), date_create(), Application::STATE_SUBMITTED),
                'week'=>$statsService->getCountOfWorksWithStatus($startOfThisWeek, date_create(), Application::STATE_SUBMITTED),
                'total'=>$statsService->getCountOfWorksWithStatus(date_create('0000-00-00'), date_create(), Application::STATE_SUBMITTED),
            ],'worksPaid'=>[
                'today'=>$statsService->getCountOfWorksWithStatus(date_create(date("Y-m-d")), date_create(), Application::STATE_SONGS_PAID),
                'week'=>$statsService->getCountOfWorksWithStatus($startOfThisWeek, date_create(), Application::STATE_SONGS_PAID),
                'total'=>$statsService->getCountOfWorksWithStatus(date_create('0000-00-00'), date_create(), Application::STATE_SONGS_PAID),
            ],'worksDeferred'=>[
                'today'=>$statsService->getCountOfWorksWithStatus(date_create(date("Y-m-d")), date_create(), Application::STATE_DEFERRED),
                'week'=>$statsService->getCountOfWorksWithStatus($startOfThisWeek, date_create(), Application::STATE_DEFERRED),
                'total'=>$statsService->getCountOfWorksWithStatus(date_create('0000-00-00'), date_create(), Application::STATE_DEFERRED),
            ],'worksStarted'=>[
                'today'=>$statsService->getCountOfWorksWithStatusBySongDate(date_create(date("Y-m-d")), date_create(), Application::STATE_UNPAID),
                'week'=>$statsService->getCountOfWorksWithStatusBySongDate($startOfThisWeek, date_create(), Application::STATE_UNPAID),
                'total'=>$statsService->getCountOfWorksWithStatusBySongDate(date_create('0000-00-00'), date_create(), Application::STATE_UNPAID),
            ],
        ];
        return view('admin/dashboard', ['stats'=>$stats]);
    }

    public function listSongs(Request $request){
        $user = \Auth::user();
        $filter = $request->get('filter');
        $filterArray = ['filter'=>$filter];
        $roundId = $request->get('round');
        $rounds = Round::orderBy('starts', 'ASC')->get();
        if(!empty($roundId)){
            $filterArray['round']=$roundId;
        }
        if(!empty($request->get('sort'))) {
            $filterArray['sort'] = $request->get('sort');
        }
        if(!empty($request->get('category_id'))) {
            $filterArray['category_id'] = $request->get('category_id');
        }
        if(!empty($request->get('extra'))){
            $filterArray['extra'] = $request->get('extra');
        }
        if(!empty($request->get('search_term'))){
            $filterArray['search_term'] = $request->get('search_term');
            $filterArray['search_type'] = $request->get('search_type');
        }

        $categories = Category::all();

        $savedSearch = \App::make('EloquentService')->saveOrGetSearch($request->except('next'), $user->id);
        if($nextId = $request->get('next')){
            $previousSongs = explode(',', $savedSearch->results);
            foreach($previousSongs as $key => $song){
                if($song == $nextId){
                    $nextSong = isset($previousSongs[$key+1]) ? $previousSongs[$key+1] : false;
                    if($nextSong) {
                        return redirect(route('previewSong', ['id' => $nextSong, 'search' => $savedSearch->id]));
                    } else { // There is no next song, so just redirect back to THIS page.
                        return redirect(route('adminListSongs', $request->except('next')));
                    }
                }
            }
        }

        $searchService = \App::make("SearchService");
        $searchService->searchFromRequestObject($request, $user);
        $searchService->fetchAllFields();
        $savedSearch->results = $searchService->get()->implode('id',',');
        $savedSearch->save();

        $searchService->searchFromRequestObject($request, $user);
        $searchService->fetchAllFields();

        $songs = $searchService->songSearch->paginate(\Config('app.paginateMax'));

        $justAssigned = $request->session()->get('justAssigned');
        if(!empty($justAssigned)){
            $request->session()->set('justAssigned', null);
            if(isset($justAssigned['panel'])){
                $justAssigned['panel'] = Panel::findOrFail($justAssigned['panel']);
            }
        }

        $songScores = [];
        if($searchService->hasScore()){
            if($searchService->showOnlyMyScore()){
                $songScores = \App::make('JudgingService')->getSongScoresByJudge($songs, $roundId, $user->id);
            } else {
                $songScores = \App::make('JudgingService')->getSongAveragesByRound($songs, $roundId);
            }
        }
        $judges = \App\User::where('role', User::USER_ROLE_JUDGE)->orderBy('firstname', 'ASC')->get();
        $admins = \App\User::where('role', User::USER_ROLE_ADMIN)->orderBy('firstname', 'ASC')->get();
        $panels = \App\Panel::where('id', '>', 0)->orderBy('name', 'ASC')->get();

        if($user->role == User::USER_ROLE_JUDGE and $searchService->hadNoFilter()){
            foreach($rounds as $round){
                if($round->isActive()){
                    return redirect(route('adminListSongs', ['filter'=>self::FILTER_PENDING_ASSIGNED_TO_ME, 'round'=>$round->id]));
                }
            }
            return redirect('details');
        }

        $availableSearchTypes = [self::SEARCH_TYPE_TITLE, self::SEARCH_TYPE_APPLICATION_ID, self::SEARCH_TYPE_EMAIL, self::SEARCH_TYPE_NAME];

        return view(\App\Helpers\General::templateWithPrefix('admin.songs'), [
            'inputValues'=>$request->except(['search_term', 'search_type']),
            'availableSearchTypes'=>$availableSearchTypes,
            'justAssigned'=>$justAssigned,
            'savedSearch'=>$savedSearch,
            'songScores'=>$songScores,
            'roundId'=>$request->get('round'),
            'categories'=>$categories,
            'hasScore'=>$searchService->hasScore(),
            'currentRoute'=>'adminListSongs',
            'admins'=>$admins,
            'panels'=>$panels,
            'songs'=>$songs,
            'rounds'=>$rounds,
            'judges'=>$judges,
            'filterArray'=>$filterArray
        ]);
    }


    public function viewScores(Request $request, $id, $roundId){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $judge = User::findOrFail($id);
        $round = Round::findOrFail($roundId);
        $rounds = Round::all();

        $scoreSearch = Score::where('user_id', $judge->id)->where('round_id', $round->id);

        $scoreSearch->leftJoin('songs as s', function($join){
           $join->on('scores.song_id', '=', 's.id');
        });
        $scoreSearch->select('scores.*');
        $scoreSearch->orderBy('s.category_id', 'asc');
        $scoreSearch->orderBy('score', 'desc')->get();

        $scores = $scoreSearch->get();

        return view(\App\Helpers\General::templateWithPrefix('admin.scores'), ['judge'=>$judge, 'scores'=>$scores, 'currentRound'=>$round, 'rounds'=>$rounds]);

    }
    public function deleteAllSongRoundsForJudgeByRound(Request $request){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $judge = User::findOrFail($request->get('judge'));
        $round = Round::findOrFail($request->get('round'));
        $count = 0;
        foreach($judge->songRounds as $songRound){
            if($songRound->round_id == $round->id){
                $songRound->delete();
                $count++;
            }
        }
        $request->session()->flash('alert-success', "Removed $count from assignments. ");
        return back();
    }
    public function getPanels(Request $request){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $panels = Panel::all();
        return view('admin/panels', ['panels'=>$panels]);
    }

    public function listApplications(Request $request){

        $filter = $request->get('filter');
        $filterArray = ['filter'=>$filter];
        $sort = $request->get('sort');
        $filterArray['sort'] = $sort;
        $applicationSearch = \App\Application::where('applications.id', ">", 0);
        switch($sort){
            case self::SORT_DESC:
                    $applicationSearch->join('users as u',  'u.id', '=', 'applications.user_id')->select('applications.*')
                    ->orderBy('u.firstname', 'desc');
                break;
            case self::SORT_ASC:
                    $applicationSearch->join('users as u',  'u.id', '=', 'applications.user_id')->select('applications.*')
                    ->orderBy('u.firstname', 'asc');
                break;
            case self::SORT_DATE_ASC:
                    $applicationSearch->orderBy('created_at', 'ASC');
                break;
            case self::SORT_DATE_DESC:
                    $applicationSearch->orderBy('created_at', 'DESC');
                break;
            default:
                break;
        }
        switch($filter){
            case self::FILTER_STATUS:
                    $applicationSearch->where('status', $request->get('state'));
                    $filterArray['state'] = $request->get('state');
                break;
            case self::FILTER_ARCHIVED:
                    $applicationSearch->onlyTrashed();
                break;
            default:
                break;
        }
        if($searchTerm = $request->get('search_term')){
            $filterArray['search_term'] = $searchTerm;
            $filterArray['search_type'] = $request->get('search_type');
            switch($filterArray['search_type']){
                case self::SEARCH_TYPE_APPLICATION_ID:
                    $searchArray = explode('-', $searchTerm);
                    $searchId = end($searchArray);
                    //$filterArray = [];
                    $filterArray['search_term'] = $searchTerm;
                    $filterArray['search_type'] = $request->get('search_type');
                    $applicationSearch = \App\Application::where('id', $searchId);
                    break;
                case self::SEARCH_TYPE_EMAIL:
                    $applicationSearch = $applicationSearch->whereHas('user', function ($query) use ($searchTerm) {
                        $query->where('email', 'LIKE', "%$searchTerm%");
                    });
                    break;
                case self::SEARCH_TYPE_NAME:
                    $nameParts = explode(' ', $searchTerm);
                    $applicationSearch = $applicationSearch->whereHas('user', function ($query) use ($nameParts, $searchTerm) {
                        if(str_word_count($searchTerm) == 1){
                            $query->where(function($query) use($nameParts) {
                                $query->where('surname', 'LIKE', "%{$nameParts[0]}%");
                                $query->orWhere('firstname', 'LIKE', "%{$nameParts[0]}%");
                            });
                        } else {
                            $query->where('firstname', 'LIKE', "%{$nameParts[0]}%");
                            if(isset($nameParts[1])){
                                $query->where('surname', 'LIKE', "%{$nameParts[1]}%");
                            }
                        }
                    });
                    break;
            }
        }
        if($request->get('action') == 'export'){
            set_time_limit(180);
            \App::make('ExportService')->exportApplicationsToExcel($applicationSearch);
            $this->logMemoryUsage();
            exit(); // the library sends some custom output
        }
        $applications = $applicationSearch->paginate(\Config('app.paginateMax'));

        $judges = \App\User::where('role', User::USER_ROLE_JUDGE)->orderBy('firstname', 'ASC')->get();
        $rounds = Round::orderBy('starts', 'ASC')->get();

        $availableSearchTypes = [self::SEARCH_TYPE_APPLICATION_ID, self::SEARCH_TYPE_EMAIL, self::SEARCH_TYPE_NAME];

        return view(\App\Helpers\General::templateWithPrefix('admin.applications'), ['applications'=>$applications, 'rounds'=>$rounds, 'judges'=>$judges, 'filterArray'=>$filterArray, 'inputValues'=>$request->except(['search_term', 'search_type']), 'availableSearchTypes'=>$availableSearchTypes, 'currentRoute'=>'adminListApplications']);
    }
    public function exportScoresToExcel(){
        set_time_limit(180);
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        \App::make('ExportService')->exportScoresToSpreadsheet();
        $this->logMemoryUsage();
        exit(); // the library sends some custom output
    }
    public function changeApplicationStatusTo(Request $request, $id, $status){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $application = \App\Application::findOrFail($id);
        $states = [Application::STATE_UNPAID, Application::STATE_SONGS_PAID, Application::STATE_DEFERRED, Application::STATE_SUBMITTED];
        if(!in_array($status, $states)){
            abort(400, "That state is not valid. ");
        }
        $application->status = $status;
        $application->save();
        return back();
    }
    public function previewSong(Request $request, $id){
        $user = \Auth::user();
        $song = Song::findOrFail($id);
        $panelIds = [];
        foreach($user->panels as $panel){
           $panelIds[]= $panel->id;
        }
        $scoreRange = range(Config('app.lowestScore'), Config('app.highestScore'));
        $judgingSongRounds = SongRound::where(function($query) use($user, $panelIds) {
           $query->where('judge_id', $user->id);
            $query->orWhereIn('panel_id', $panelIds);
        })->where('song_id', $id)->get();
        $scores = Score::where('user_id', $user->id)->where('song_id', $song->id)->get();
        $scoresByRound = [];
        foreach($scores as $dbScore){
            $scoresByRound[$dbScore->song_round_id] = $dbScore;
        }

        //$judgingSongRounds = SongRound::where(['song_id'=>$id, 'judge_id'=>$user->id])->get();
        \Log::info("There was this many songRounds {$judgingSongRounds->count()}");
        $savedSearch = Search::find($request->get('search'));
        return view('admin/song', ['savedSearch'=>$savedSearch, 'scoresByRound'=>$scoresByRound, 'song'=>$song, 'scores'=>$scores, 'judgingRounds'=>$judgingSongRounds, 'scoreRange'=>$scoreRange]);
    }

    public function listUsers(Request $request){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $userSearch = User::where('id', '>', 0);
        $filter = $request->get('filter');
        $filterArray = ['filter'=>$filter];
        $sort = $request->get('sort');
        $filterArray['sort'] = $sort;
        switch($sort){
            case self::SORT_DESC:
                    $userSearch->orderBy('firstname', 'desc');
                break;
            case self::SORT_ASC:
                    $userSearch->orderBy('firstname', 'asc');
                break;
            case self::SORT_DATE_ASC:
                    $userSearch->orderBy('created_at', 'ASC');
                break;
            case self::SORT_DATE_DESC:
                    $userSearch->orderBy('created_at', 'DESC');
                break;
            default:
                    $userSearch->orderBy('created_at', 'DESC');
                break;
        }
        if($searchTerm = $request->get('search_term')){
            $filterArray['search_term'] = $searchTerm;
            $filterArray['search_type'] = $request->get('search_type');
            switch($filterArray['search_type']){
                case self::SEARCH_TYPE_EMAIL:
                    $userSearch->where('email', 'LIKE', "%$searchTerm%");
                    break;
                case self::SEARCH_TYPE_NAME:
                    $nameParts = explode(' ', $searchTerm);
                    if(str_word_count($searchTerm) == 1){
                            $userSearch->where(function($query) use($nameParts) {
                                $query->where('surname', 'LIKE', "%{$nameParts[0]}%");
                                $query->orWhere('firstname', 'LIKE', "%{$nameParts[0]}%");
                            });
                        } else {
                            $userSearch->where('firstname', 'LIKE', "%{$nameParts[0]}%");
                            if(isset($nameParts[1])){
                                $userSearch->where('surname', 'LIKE', "%{$nameParts[1]}%");
                            }
                        }
                    break;
            }
        }
        $availableSearchTypes = [self::SEARCH_TYPE_EMAIL, self::SEARCH_TYPE_NAME];
        $users = $userSearch->paginate(\Config::get('app.paginateMax'));
        $rounds = Round::all();
        return view(\App\Helpers\General::templateWithPrefix('admin.users'), ['rounds'=>$rounds, 'availableSearchTypes'=>$availableSearchTypes, 'users'=>$users, 'filterArray'=>$filterArray, 'inputValues'=>$request->except(['search_term', 'search_type']), 'currentRoute'=>'adminListUsers']);
    }

    public function impersonateUser(Request $request, $id){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $otherUser = \App\User::findOrFail($id);

        \Auth::login($otherUser);

        $request->session()->put('impersonatingUser', $user->id);

        return redirect('details');
    }
    public function deleteUsersToRound(Request $request){
        $user = \Auth::user();
        if ($user->role != User::USER_ROLE_ADMIN) {
            abort(403, "Unauthorized");
        }
        $songRound = SongRound::findOrfail($request->get('songRoundId'));
        foreach($songRound->scores as $score){
            $score->delete();
        }
        \App::make('JudgingService')->calculateAndSaveScoreAverage($songRound->song->id, $songRound->round->id);
        $songRound->delete();

        $request->session()->flash('alert-success', "Removed {$songRound->song->title} from {$songRound->displayName} - {$songRound->round->name}");

        return back();
    }
    public function removeScoreFromRound(Request $request){
        $user = \Auth::user();
        if ($user->role != User::USER_ROLE_ADMIN) {
            abort(403, "Unauthorized");
        }
        $score = Score::findOrfail($request->get('score_id'));
        $score->delete();

        \App::make('JudgingService')->calculateAndSaveScoreAverage($score->song->id, $score->round->id);
        $request->session()->flash('alert-success', "Removed {$score->user->fullName}'s score for {$score->round->name}. ");

        return back();
    }

    public function assignUsersToRound(Request $request){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $judgeParts = explode('-', $request->get('judge'));
        switch($judgeParts[0]){
            case self::JUDGE_TYPE_USER:
                $judge = User::findOrFail($judgeParts[1]);
                break;
            case self::JUDGE_TYPE_PANEL:
                $panel = Panel::findOrFail($judgeParts[1]);
                break;
        }

        $round = Round::findOrFail($request->get('round'));
        if($request->get('is_random') == 1){
            if(isset($panel)) {
                return $this->processSongsViaRandom($request, $panel, $user);
            } else {
                $request->session()->flash('alert-danger', "Please choose a panel. ");
                return back();
            }
        } else {
            $selections = $request->get('songs');
        }
        $count = 0;
        if(!empty($selections)){
            foreach($selections as $selection){
                if(is_numeric($selection)){
                    Song::findOrFail($selection);
                    switch($judgeParts[0]){
                        case self::JUDGE_TYPE_PANEL:
                            $exists = SongRound::where('round_id', $round->id)->where('panel_id', $panel->id)->where('song_id', $selection)->count();
                            if($exists > 0){
                                $request->session()->flash('alert-info', "Tracks were already assigned. ");
                                continue;
                            }
                            \App::make('JudgingService')->createSongRoundForPanel($selection, $round->id, $panel->id);
                            $count++;
                            break;
                        case self::JUDGE_TYPE_USER:
                            $exists = SongRound::where('round_id', $round->id)->where('judge_id', $judge->id)->where('song_id', $selection)->count();
                            if($exists > 0){
                                $request->session()->flash('alert-info', "Tracks were already assigned. ");
                                continue;
                            }
                            \App::make('JudgingService')->createSongRoundForJudge($selection, $round->id, $judge->id);
                            $count++;
                            break;
                    }
                }
            }
            $request->session()->flash('alert-success', sprintf("Assigned $count %s to {$round->name}. ", str_plural(\Config('app.filesName'))));
        } else {
            $request->session()->flash('alert-danger', "No selection made. ");
        }


        return back();
    }
    public function processSongsViaRandom($request, $panel, $user){
        $savedSearch = Search::findOrFail($request->get('search'));
        $searchService = \App::make('SearchService');
        $searchService->searchAnyOrder($savedSearch, $user);
        $searchService->orderByRandom();
        $searchService->fetchAllFields();
        $judgeCount = $panel->users->count();
        $songs = $searchService->songSearch->get();
        $countOfAssignedPerJudge = ['panel'=>$panel->id, 'counts'=>[]];
        $judges = $panel->users;
        if($judgeCount > 0){
            $lastJudgeKey = 0;
            foreach($songs as $song){
                $judge = $judges->get($lastJudgeKey);
                $exists = SongRound::where('round_id', $request->get('round'))->where('judge_id', $judge->id)->where('song_id', $song->id)->count();
                if($exists == 0){
                    \App::make('JudgingService')->createSongRoundForJudge($song->id, $request->get('round'), $judge->id);
                    if(isset($countOfAssignedPerJudge['counts'][$lastJudgeKey])){
                        $countOfAssignedPerJudge['counts'][$lastJudgeKey]++;
                    } else{
                        $countOfAssignedPerJudge['counts'][$lastJudgeKey] = 1;
                    }
                }
                if($judges->get($lastJudgeKey+1)){
                    $lastJudgeKey++;
                } else {
                    $lastJudgeKey = 0;
                }
            }
            $message = '';
            foreach($countOfAssignedPerJudge['counts'] as $key => $count){
                $message .= sprintf("Assigned %d to %s. ", $count, $judges->get($key)->fullName);
            }
            \Log::info($message);
            $request->session()->flash('alert-success', "Tracks were assigned. ");
            $request->session()->set('justAssigned', $countOfAssignedPerJudge);
            return back();
        } else {
            $request->session()->flash('alert-danger', "That panel does not have any judges. ");
            return back();
        }
    }
    public function assignScoreToSong(Request $request){
        $user = \Auth::user();
        $submittedScore = (int) $request->get('score');
        $songRound = SongRound::findOrFail($request->get('songRound'));
        if(!$songRound->round->isActive()){
            return abort(403, "Unauthorized");
        }

        $song = Song::find($songRound->song_id);

        if ($song->category_id == 19) {
            // Category ID is best soundtrack album, score is applied to the album
            // This is a hack, to apply the same score to every song in album
            $applicationID = $song->application_id;
            $songIDs = Song::where('application_id', $applicationID)->get()->pluck('id');

            $songIDs->map(function($songid) use($user, $submittedScore, $songRound) {
                $nextSongRound = SongRound::where('song_id', $songid)
                    ->where('judge_id', $songRound->judge_id)
                    ->where('panel_id', $songRound->panel_id)->first();

                $this->singleScoreUpdate($user, $nextSongRound, $submittedScore);
            });

            return response("", 200);
        } else {
            if ($this->singleScoreUpdate($user, $songRound, $submittedScore)) {
                return response("", 200);
            }
        }

        return abort(403, "Unauthorized");
    }



    private function singleScoreUpdate($user, $songRound, $submittedScore)
    {
        dump($songRound);
        dump($submittedScore);
        dump($user);

        if(
            ($songRound->panel and $songRound->panel->users->where('id', $user->id)->count() > 0)
            or
            ($songRound->judge and $songRound->judge->id === $user->id)
        ) {
            $score = Score::firstOrCreate(['song_round_id'=>$songRound->id, 'round_id'=> $songRound->round->id, 'song_id'=>$songRound->song->id, 'user_id'=>$user->id]);
            $score->user_id = $user->id;
            $score->score = $submittedScore;
            $score->song_id = $songRound->song->id;
            $score->round_id = $songRound->round->id;
            $score->song_round_id = $songRound->id;

            $score->save();

            \App::make('JudgingService')->calculateAndSaveScoreAverage($songRound->song->id, $songRound->round->id);
            return true;
        }
        return false;
    }
    /**
     * Submit a score for 0 in order to mark it as read
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function markAsRead(Request $request){
        $user = \Auth::user();
        $songRound = SongRound::findOrFail($request->get('songRound'));
        if(!$songRound->round->isActive()){
            return abort(403, "Unauthorized");
        }
        if(($songRound->panel and $songRound->panel->users->where('id', $user->id)->count() > 0) or ($songRound->judge and $songRound->judge->id === $user->id)) {
            $scoreSearch = Score::where('song_round_id', $songRound->id)->where('round_id', $songRound->round->id)->where('song_id', $songRound->song->id)->where('user_id', $user->id)->get();
            if($scoreSearch->count() == 0){
                $score = Score::create(['song_round_id'=>$songRound->id]);
                $score->user_id = $user->id;
                $score->score = 0;
                $score->song_id = $songRound->song->id;
                $score->round_id = $songRound->round->id;

                $score->save();

                \App::make('JudgingService')->calculateAndSaveScoreAverage($songRound->song->id, $songRound->round->id);
            }

            return response("", 200);
        }
        return abort(403, "Unauthorized");
    }
    public function getManageUser(Request $request, $id = null){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $viewData = [];
        if($id !== null){
            $viewData['editUser'] = User::findOrFail($id);
        }
        return view('admin/user', $viewData);
    }
    public function postManageUser(Request $request, $id = null){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $data = $request->all();
        $validations = [
            'firstname' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'min:8',
            'role'=>"required|in:" . User::USER_ROLE_ADMIN . "," . User::USER_ROLE_USER . "," . User::USER_ROLE_JUDGE,
        ];
        if($id !== null){
            $editUser = User::findOrFail($id);
            $validations['email'] = 'admin_update_email|' . $validations['email'];
        } else {
            $validations['password'] = 'required|' . $validations['password'];
            $validations['email'] = 'unique:users|' . $validations['email'];
        }
        $validator = Validator::make($data, $validations);
        if ($validator->fails()) {
            $errors = $this->formatValidationErrors($validator);
            $to = $id == null ? 'admin/user' : 'admin/user/'.$id;
            return redirect()->to($to)
                ->withInput($data)
                ->withErrors($errors, $this->errorBag());
        }
        if(isset($editUser)){
            $editUser->update([
                'firstname' => $data['firstname'],
                'surname' => $data['surname'],
                'email' => $data['email'],
            ]);
            $message = "Account updated.";
        } else {
            $editUser = User::create([
                'firstname' => $data['firstname'],
                'surname' => $data['surname'],
                'email' => $data['email'],
            ]);
            $message = sprintf("Account created for %s.", $editUser->firstname);
        }
        if(!empty($request->get('password'))){
            $editUser->password = bcrypt($request->get('password'));
        }

        $editUser->role = $request->get('role');
        $editUser->save();

        $request->session()->flash('alert-success', $message);
        return redirect('admin/users');
    }
    public function shortlistSongToRound(Request $request){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $songRoundAverage = SongRoundAverage::where('song_id', $request->get('song_id'))->where('round_id', $request->get('round_id'))->first();
        if($songRoundAverage){
            if($request->get('action') == 'shortlist'){
                $songRoundAverage->shortlisted = true;
                $message = "Shortlisted " . \Config('app.filesName');
            } else {
                $songRoundAverage->shortlisted = false;
                $message = "Unshortlisted " . \Config('app.filesName');
            }
            $songRoundAverage->save();
            $request->session()->flash('alert-success', $message);
        } else {
            $request->session()->flash('alert-danger', "Can not shortlist a song that has no score. ");
        }
        return back();
    }
    public function getManagePanel(Request $request, $id = null){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $viewVars = [];
        $adminUsers = User::where('role', User::USER_ROLE_ADMIN)->get();
        $judgeUsers = User::where('role', User::USER_ROLE_JUDGE)->get();
        $viewVars['adminUsers'] = $adminUsers;
        $viewVars['judgeUsers'] = $judgeUsers;

        if($id !== null){
            $viewVars['panel'] = Panel::findOrFail($id);
        }
        return view('admin/panel', $viewVars);
    }
    public function postManagePanel(Request $request, $id = null){
        $user = \Auth::user();
        if($user->role != User::USER_ROLE_ADMIN){
            abort(403, "Unauthorized");
        }
        $this->validate($request, [
            'name' => 'required|max:255',
            'judges'=>''
        ]);

        if(is_null($id)){
            $panel = Panel::create();
            $message = "Panel created. ";
        } else {
            $panel = Panel::findOrFail($id);
            $message = "Panel Updated. ";
        }
        $panel->users()->detach();
        if(is_array($request->get('judges'))){
            foreach($request->get('judges') as $judgeId){
                $panel->users()->attach($judgeId);
            }
        }
        $panel->name = $request->get('name');
        $panel->save();
        $request->session()->flash('alert-success', $message);
        return redirect('admin/panel/'.$panel->id);
    }
}
