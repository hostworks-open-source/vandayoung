<?php

namespace App\Http\Controllers;

use App\File;
use App\User;
use DateTime;
use App\Country;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ApplicationController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update user details for the comp
     *
     */
    public function getAccountUpdate()
    {
        return view('details', ['countries' => Country::all(), 'user' => auth()->user()]);
    }

    /**
     * Update user details
     *
     */
    public function postAccountUpdate(Request $request)
    {
        $validations = [
            'firstname' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|update_email|email|max:255',
            'dob' => 'date_format:d/m/Y|between:10,10',
            'address' => 'required|max:100',
            'address2' => 'max:100',
            'city' => 'required|max:70',
            'country' => 'required|numeric|exists:countries,id',
            'phone' => 'required',
            'state' => 'required|max:30',
            'postcode' => 'required|min:4|max:10',
            'profile_image'=>'valid_file',
            'gender' => 'in:male,female,na',
            'additional_composers'=>'max:1000',
            'bio' => 'max:2500|max_words:300',
        ];

        if (config('app.userHasDob')) {
            $validations['dob'] = "required|{$validations['dob']}";
        }

        if (config('app.profileHasBio')) {
            $validations['bio'] = "required|{$validations['bio']}";
        }

        if (config('app.profileHasProfileImage')) {
            $validations['profile_image'] = "required|{$validations['profile_image']}";
        }

        if (config('app.profileHasGender')) {
            $validations['gender'] = "required|{$validations['gender']}";
        }

        $this->validate($request, $validations, [
            'dob.required' => 'The date of birth field is required.',
            'dob.between' => 'Must be valid date of birth. ',
            'email.update_email' => 'This email is already taken.',
        ]);

        $user = auth()->user();

        $user->fill($request->only([
            'firstname',
            'surname',
            'email',
            'address',
            'address2',
            'city',
            'state',
            'postcode',
            'phone',
            'gender',
            'additional_composers',
            'bio',
        ]));

        $user->country_id = $request->get('country');

        if (config('app.userHasDob')) {
            $user->dob = DateTime::createFromFormat('d/m/Y', $request->get('dob'))->format('Y-m-d');
        }

        if (config('app.profileHasAreYouAnApraMember')) {
            $user->an_apra_member = $request->get('an_apra_member') == 1;
        }

        if (config('app.profileHasAreWorksRegistered')) {
            $user->works_are_registered = $request->get('works_are_registered') == 1;
        }

        if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {
            // Delete the existing profile image, if necessary
            if ($user->profileImage && Storage::disk('users')->has($file = "{$user->id}/{$user->profileImage->name}")) {
                Storage::disk('users')->delete($file);
                $user->profileImage()->delete();
            }

            $user->profileImage()->associate(File::create([
                'user_id' => $user->id,
                'name' => $request->file('profile_image')->getClientOriginalName(),
                'filesize' => $request->file('profile_image')->getSize(),
                'mime_type' => $request->file('profile_image')->getMimeType(),
            ]));

            $request->file('profile_image')->move(
                storage_path("app/public/users/{$user->id}"),
                $request->file('profile_image')->getClientOriginalName()
            );
        }

        $user->save();

        session()->flash('alert-success', 'Your details were updated!');

        return redirect('details');
    }



    public function getUpdatePassword(Request $request)
    {
        return view('auth/passwordUpdate', ['user'=>\Auth::user()]);
    }

    public function postUpdatePassword(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'old_password'  => 'required|is_old_password',
            'password'  => 'required|min:8',
        ]);
        $user->password = bcrypt($request->get('password'));
        $user->save();
        $request->session()->flash('alert-success', 'Your password was updated!');

        return redirect('details');
    }

    public function homeRedirect()
    {
        $user = \Auth::user();
        if ($user and in_array($user->role, [User::USER_ROLE_ADMIN])) {
            return redirect('admin/dashboard');
        }

        return redirect('applications');
    }

    public function getUserApplications(Request $request)
    {
        $user = \Auth::user();
        $applications = [];

        foreach ($user->applications as $app) {
            $applications[ $app->id ] = $app;
            try {
                $songs = \App\Song::where('application_id', $app->id)->orderBy('created_at', 'desc')->get();
                $applications[ $app->id ]->title = $songs[0]->title;
            } catch (\Exception $e) {
            }
        }

        return view(\App\Helpers\General::templateWithPrefix('index'), ['applications'=>$applications]);
    }

    protected function redirectToApplicationOrCreate()
    {
        $user = \Auth::user();
        if ($user->applications->count() == 0) {
            return redirect('applications/create');
        }
        $application = $user->applications->last();
        if ($application) {
            return redirect('app/'.$application->id);
        }

        return redirect('details');
    }

    /**
     * Update application status
     *
     */
    protected function getApplication(Request $request, $id = null)
    {
        $user = \Auth::user();
        $application = null;
        if (\App\Helpers\General::appsHaveClosed() and empty($request->session()->get('impersonatingUser'))) {
            return redirect('applications');
        }
        if (! is_null($id)) {
            $application = Application::findOrFail($id);
            if ($user->role != User::USER_ROLE_ADMIN and $application->user_id != $user->id) {
                abort(403);
            }
            if (\Config('app.skipApplicationPage')) {
                return redirect('submissions/add/'.$id);
            }
        }
        if (\Config('app.skipApplicationPage')) {
            $application = Application::create();
            $application->user_id = $user->id;
            $application->status = Application::STATE_UNPAID;
            $application->save();

            return redirect('submissions/add/'.$application->id);
        }

        $countries = Country::all();

        return view('application/edit', ['countries'=>$countries, 'application'=>$application]);
    }

    /**
     * Update application details for the comp
     *
     * @param array $data
     */
    protected function postApplication(Request $request, $id = null)
    {
        $user = \Auth::user();

        $fileService = \App::make('FileService');

        if (! is_null($id)) {
            $application = \App\Application::findOrFail($id);
            if ($application->user_id != $user->id) {
                return abort(403);
            }
        } else {
            $application = Application::create();
            $application->user_id = $user->id;
            $application->status = Application::STATE_UNPAID;
        }

        /** PRE PROCESS FILES AND KEEP THEM DESPITE VALIDATION FAILURE */
        $parentAuthFileId = null;
        if ($request->hasFile('parent_auth')) {
            if ($request->file('parent_auth')->isValid()) {
                $parentAuthFile = $fileService->requestToFileModel('parent_auth', $request, $user->id);
                $parentAuthFileId = $parentAuthFile->id;
            }
        } elseif (is_numeric($request->input('parent_auth'))) {
            $parentAuthFileId = $request->input('parent_auth');
        }
        $profileImageFileId = null;
        if ($request->hasFile('profile_image')) {
            if ($request->file('profile_image')->isValid()) {
                $profileImageFile = $fileService->requestToFileModel('profile_image', $request, $user->id);
                $profileImageFileId = $profileImageFile->id;
            }
        } elseif (is_numeric($request->input('profile_image'))) {
            $profileImageFileId = $request->input('profile_image');
        }
        /** END PRE PROCESS FILES */

        /* START PROFILE IMAGE */
        if (is_numeric($application->profile_image_file_id) and $profileImageFileId != $application->profile_image_file_id) {
            $deleteMeId = $application->profile_image_file_id;
            $application->profile_image_file_id = null;
            $application->save();
            if (is_numeric($deleteMeId)) {
                $fileService->deleteFile($deleteMeId); // IF THERE IS A FILE ID IN THE DB, THAT HAS NOW CHANGED, DELETE THE OLD ONE
            }
        }
        if (is_numeric($profileImageFileId)) {
            $application->profile_image_file_id = $profileImageFileId;
        }
        /* END PROFILE IMAGE */

        /* PARENT AUTH FILE */
        if (is_numeric($application->parent_auth_file_id) and $parentAuthFileId != $application->parent_auth_file_id) {
            $deleteMeId = $application->parent_auth_file_id;
            $application->parent_auth_file_id = null;
            $application->save();
            if (is_numeric($deleteMeId)) {
                $fileService->deleteFile($deleteMeId); // IF THERE IS A FILE ID IN THE DB, THAT HAS NOW CHANGED, DELETE THE OLD ONE
            };
        }
        if (is_numeric($parentAuthFileId)) {
            $application->parent_auth_file_id = $parentAuthFileId;
        }
        /* END PARENT AUTH */
        $application->save();
        $except = $request->except(['parent_auth', 'profile_image']);
        $input = array_merge($except, ['parent_auth'=>$parentAuthFileId, 'profile_image'=>$profileImageFileId]);
        $validator = \Validator::make($input, [
            'gender' => 'required|in:male,female,na',
            'performing_name' => 'required|max:100',
            'performing_rights_association' => 'max:100',
            'bio' => 'required|max:2500|max_words:300',
            'profile_image'=>'required|valid_file',
        ], $messages = [
            'bio.max_words' => 'Exceeds word limit.',
            'profile_image.valid_file' => 'A valid file is required.',
        ]);
        $validator->sometimes('parent_auth', 'required|valid_file', function ($input) use ($user) {
            $then = date_create($user->dob);
            if ($then) {
                $then = $then->getTimestamp();
                $min = strtotime('+18 years', $then);

                return (time() < $min);
            }

            return false;
        });

        $autoFields = ['gender', 'performing_name', 'performing_rights_association', 'bio', 'phone', 'on_behalf'];

        $application->update($request->only($autoFields));
        $application->on_behalf = ($request->get('on_behalf') == '1');

        $application->save();

        if ($validator->fails()) {
            $errors = $this->formatValidationErrors($validator);

            return redirect()->to('app/'.$application->id)
                ->withInput($input)
                ->withErrors($errors, $this->errorBag());
        }

        $request->session()->flash('alert-success', 'Your application was saved!');

        return redirect('submissions/add/'.$application->id);
    }


    protected function getFileData(File $file)
    {
        if ($file && ($file->user_id == auth()->id())) {
            return $file;
        }

        abort(403, 'Unauthorized action.');
    }
}
