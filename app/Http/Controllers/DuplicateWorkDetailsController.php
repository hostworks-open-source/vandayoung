<?php

namespace App\Http\Controllers;

use App\Song;
use App\Application;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DuplicateWorkDetailsController extends Controller
{
    public function store(Application $application)
    {
        if (auth()->id() !== $application->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have permission to modify this application',
            ], 403);
        }

        $source = Song::where('application_id', $application->id)->findOrFail(request('source'));
        $destination = Song::where('application_id', $application->id)->whereIn('id', request('destination'))->get();

        if ($destination->count() !== count(request('destination'))) {
            return response()->json([
                'success' => false,
                'message' => 'One or more selected works do not belong to this application',
            ], 403);
        }

        $application->songs()->whereIn('id', request('destination'))->update([
            'year' => $source->year,
            'lyrics' => $source->lyrics,
            'genre_id' => $source->genre_id,
            'other_genre' => $source->other_genre,
            'category_id' => $source->category_id,
            'queue_sheet_file_id' => $source->queue_sheet_file_id,
            'main_score_file_id' => $source->main_score_file_id,
            'lead_sheet_file_id' => $source->lead_sheet_file_id,
            'main_score_mid_file_id' => $source->main_score_mid_file_id,
            'first_broadcast' => $source->first_broadcast,
            'first_venue' => $source->first_venue,
            'additional_composers' => $source->additional_composers,
        ]);

        session()->flash('alert-success', 'Song details were successfully duplicated');

        return response()->json([
            'success' => true,
            'message' => 'Song details have been successfully duplicated.',
        ], 200);
    }
}
