<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Country;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class FileController extends Controller
{
    /**
     * Stream the requested file for the given submission.
     *
     * @param  Submission $submission Submission to stream the given media from
     * @param  string     $media      The file to be streamed
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function media($sha1Name, $name, Request $request)
    {
        $file = \App\File::where('name_sha1', $sha1Name)->firstOrFail();
        $fileService = \App::make('FileService');
        if($file->isVideo()){
            $filePath = $fileService->videoPath($file->name_sha1, pathinfo($file->name, PATHINFO_EXTENSION));
        } else {
            $filePath = sprintf('%s%d/%s', Storage::disk('users')->getDriver()->getAdapter()->getPathPrefix(), $file->user_id, $file->name);
        }
        // Audio and video files need to be streamed to the browser, to facilitate
        // file chunking and ranges, in order to allow users to scrub playback.

        \Log::info("$sha1Name regular file");
        return response()->download($filePath, $file->name, [
            'Content-Type'        => $file->mime_type,
            'Content-Length'      => $file->filesize,
            'Content-Disposition' => sprintf('inline; filename="%s"', $file->name),
        ]);
    }
    /**
     * Stream a response file back to the browser.
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function streamResponse($sha1Name, $name, Request $request)
    {
        $file = \App\File::where('name_sha1', $sha1Name)->firstOrFail();
        $fileService = \App::make('FileService');
        $headers      = [ 'Content-Type' => $file->mime_type, ];
        $stream       = fopen($fileService->filePath($file->name_sha1), 'r');
        $size         = $file->filesize;
        $responseCode = Response::HTTP_OK;
        $range        = $request->header('Range');
        if (! is_null($range)) {
            $posCurrent = strpos($range, '=');
            $posTo      = strpos($range, '-');
            $unit       = substr($range, 0, $posCurrent);
            $start      = intval(substr($range, $posCurrent+1, $posTo));
            $success    = fseek($stream, $start);
            if (! $success) {
                $size         = $file->filesize - $start;
                $responseCode = Response::HTTP_PARTIAL_CONTENT;
                $headers['Accept-Ranges'] = $unit;
                $headers['Content-Range'] = sprintf('%s %s-%s/%s', $unit, $start, $file->filesize-1, $file->filesize);
            }
        }
        $headers['Content-Length'] = $size;
        return response()->stream(function () use ($stream) {
            fpassthru($stream);
        }, $responseCode, $headers);
    }
}
