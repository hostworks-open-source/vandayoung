<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class GeneralController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Exit impersonating a user
     */
    public function exitImpersonatingUser(Request $request){
        $oldUserId = $request->session()->get('impersonatingUser');
        $originalUser = \App\User::findOrFail($oldUserId);
        \Auth::login($originalUser);
        $request->session()->put('impersonatingUser', null);
        return redirect('admin/users');
    }
}
