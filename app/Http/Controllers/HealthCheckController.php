<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

/**
 * Health check controller
 */
class HealthCheckController extends Controller
{

    /**
     * Simple database check to ensure the site is available.
     *
     * @return $this|\Symfony\Component\HttpFoundation\Response
     */
    public function check()
    {
        if (DB::connection()->getDatabaseName()) {
            return response('Everything is good')
                ->header('Content-Type', 'text/plain')
                ->setStatusCode(Response::HTTP_OK);
        }

        return response('Could not connect to database')
            ->header('Content-Type', 'text/plain')
            ->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
    }
}