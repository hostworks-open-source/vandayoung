<?php

namespace App\Http\Controllers;

use App\File;
use App\Song;
use App\User;
use DateTime;
use App\Genre;
use Validator;
use App\Category;
use App\Application;
use App\Helpers\General;
use App\Service\FileService;
use Illuminate\Http\Request;
use App\Service\EloquentService;
use App\Foundry\Console\ApiClient;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class SubmissionController extends Controller
{
    protected $apiClient;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(ApiClient $apiClient)
    {
        $this->middleware('auth');
        $this->middleware('submissions.closed');
        $this->apiClient = $apiClient;
    }

    /**
     * Page to pay for an application submission
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function pay(Request $request, $id)
    {
        $user = auth()->user();
        $application = Application::findOrFail($id);

        if ($user->id != $application->user_id) {
            return abort(403);
        }

        if (! $user->isValid()) {
            return redirect('details')->with('alert-danger', 'Please finish your details.');
        }

        if ($application->isValid()) {
            if (! $application->songCountIsValid()) {
                return redirect("submissions/add/{$application->id}")
                    ->with('alert-danger', sprintf('Please upload at least one %s', config('app.filesName')));
            }

            if ($application->songsAreValid()) {
                $billingUrl = config('app.gatewayUrl').'?'.http_build_query([
                    'companyToPay' => 'APRA',
                    'amount' => $application->billAmount,
                    'reference' => sprintf('%s-%s', config('app.applicationPrefix'), $application->id),
                ]);

                if ($request->get('submit')) {
                    $application->forceFill(['status' => Application::STATE_SUBMITTED])->save();

                    Mail::send('emails.submitted', ['app' => $application, 'user' => $user], function ($message) use ($user) {
                        $message->to($user->email, "{$user->firstname} {$user->surname}")
                            ->subject('Application Received');
                    });

                    return redirect("submitted");
                }

                if ($request->get('defer')) {
                    $application->forceFill(['status' => Application::STATE_DEFERRED])->save();

                    Mail::send('emails.deferredPayment', ['app' => $application, 'user' => $user, 'billingUrl' => $billingUrl],
                        function ($message) use ($user) {
                            $message->to($user->email, "{$user->firstname} {$user->surname}")
                                ->subject('Deferred Payment');
                        }
                    );

                    return redirect("deferred");
                }

                return redirect($billingUrl);
            }

            return redirect("submissions/add/{$application->id}")
                ->with('alert-danger', sprintf('At least one %s is missing details.', config('app.filesName')));
        }

        return redirect("app/{$application->id}")->with('alert-danger', 'Please finish your application.');
    }

    public function freeSubmitConfirmation()
    {
        return view('submission/submitted', ['user' => auth()->user()]);
    }

    public function emailPreviewSubmitted()
    {
        return view('emails.submitted', ['user' => auth()->user()]);
    }


    public function unremove(Request $request, $application)
    {
        $application = Application::withTrashed()->findOrFail($application);

        $application->restore();

        Song::withTrashed()->where('application_id', $application->id)->get()->map(function($song) {
            $song->restore();
        });

        $request->session()->flash('alert-success', "Application restored");
        return back();
    }

    /**
     * Take the meta data for a file submission (Works, Songs, Video)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|static
     */
    public function song(Request $request)
    {
        $user = auth()->user();
        $file = File::findOrFail($request->input('file'));
        $category = Category::find($request->get('category'));

        if ($file->user_id !== $user->id) {
            abort(403);
        }

        $validations = [
            'file' => 'required|numeric|exists:files,id,user_id,'.auth()->id(),
            'title' => 'required',
            'first_broadcast' => 'date_format:d/m/Y|between:10,10',
            'year' => 'digits:4',
            'genre' => 'numeric|exists:genres,id',
            'category' => 'numeric|exists:categories,id|has_file_for_category|category_check_max_entries',
            'other_genre' => 'max:40',
            'additional_composers' => 'max:1000',
            'queue_sheet' => '',
        ];

        $validations['genre'] .= config('app.filesHaveGenre') ? '|required' : null;
        $validations['category'] .= config('app.filesHaveCategory') ? '|required' : null;
        $validations['queue_sheet'] .= config('app.filesHaveQueueSheet') && data_get($category, 'cuesheet_required', false) ? '|required' : null;
        $validations['year'] .= config('app.filesHaveYear') ? '|required' : null;
        $validations['first_broadcast'] .= config('app.filesHaveReleaseDate') ? '|required' : null;
        $validations['first_venue'] = config('app.filesHaveReleaseVenue') ? 'required' : null;
        $validations['main_score'] = config('app.filesHaveMainScore') ? 'required' : null;
        $validations['lead_sheet'] = config('app.filesHaveLeadSheet') ? 'required' : null;
        $validations['main_score_mid'] = config('app.filesHaveMidOfMainScore') ? 'sometimes|max:1024' : null;

        $validator = Validator::make($request->all(), $validations, [
            'main_score_mid.size' => 'The main score must be no more than 1MB',
        ]);

        $validator->sometimes('lead_sheet', 'mimes:pdf', function ($input) {
            // Do the mime check only if the lead_sheet is not an integer (existing file).
            return ! is_numeric($input->lead_sheet);
        });

        $validator->sometimes('other_genre', 'required', function ($input) {
            return $input->genre == "36";
        });

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $song = Song::firstOrNew(['file_id' => $file->id])->fill($request->only([
            'title', 'year', 'lyrics', 'other_genre', 'first_venue', 'additional_composers',
        ]));

        $song->file_id = $request->get('file');
        $song->genre_id = $request->get('genre');
        $song->category_id = $request->get('category');

        if (config('app.filesHaveReleaseDate')) {
            $song->first_broadcast = DateTime::createFromFormat('d/m/Y', $request->get('first_broadcast'))->format('Y-m-d');
        }

        $this->handleSongFiles($request, $song);

        $song->save();

        return response()->json([
            'success' => true,
            'message' => 'Song details were updated',
        ], 200);
    }

    private function handleSongFiles($request, $song)
    {
        $fileService = app(FileService::class);

        foreach (Song::FILE_TYPES as $type) {
            if ($request->hasFile($type) && $request->file($type)->isValid()) {
                if ($song->{$type}) {
                    $fileService->deleteFile($song->{$type}->id);
                    $song->{$type}()->dissociate();
                }

                $song->attachFile($type, $request->file($type));
            }
        }
    }

    public function previewSubmission($id, Request $request)
    {
        $application = Application::with('songs')->findOrFail($id);
        $loggedInUser = auth()->user();
        $user = $application->user;

        if ($user->id == $loggedInUser->id or $loggedInUser->role == User::USER_ROLE_ADMIN) {
            $songs = $application->songs;
            $genres = Genre::where('id', '>', 0)->orderBy('name', 'ASC')->get();

            return view('submission/preview', compact('songs', 'application', 'genres', 'user'));
        }
        abort(403);
    }

    public function previewRedirect()
    {
        $user = auth()->user();
        $application = \App\Application::where('user_id', $user->id)->firstOrfail();

        return redirect('application/'.$application->id);
    }

    public function emailPreview()
    {
        $user = auth()->user();
        $application = $user->applications->last();
        $transaction = $application->transactions->last();
        $amount = is_numeric($transaction->amount) ? ($transaction->amount / 100) : 0;

        return view('emails.paymentTwo', ['application'=>$application, 'user'=>$application->user, 'amount'=>$amount]);
    }

    public function emailPreviewNewUser()
    {
        $user = auth()->user();

        return view('emails.newUser', ['user'=>$user]);
    }

    public function forgotPassword()
    {
        $user = auth()->user();

        return view('emails.password', ['user'=>$user, 'token'=>'foobah']);
    }

    /**
     * Landing page for attaching files to your application
     *
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    protected function index(Request $request, $id=null)
    {
        $application = Application::findOrFail($id);

        if (auth()->id() !== $application->user_id) {
            return abort(403);
        }

        $categories = Category::where('active', true)->orderBy('name', 'ASC')->get();
        $genres = Genre::where('id', '>', 0)->orderBy('name', 'ASC')->get();
        $songs = Song::where('application_id', $application->id)->orderBy('created_at', 'desc')->get();
        $years = range(date('Y'), date('Y') - 100);

        $storage = $this->apiClient->storage(config('app.foundry_bucket_incoming'));

        return view('submission.index', [
            'songs' => $songs,
            'application' => $application,
            'genres' => $genres,
            'years' => $years,
            'categories' => $categories->groupBy('optgroup'),
            'optGroups' => $categories->unique('optgroup')->pluck('optgroup'),
            'googleBucketName' => data_get($storage, 'data.base_path'),
            'googleEmail' => json_decode(data_get($storage, 'data.credentials'))->client_email,
            'policy' => General::google_sign_policy($application),
        ]);
    }

    /**
     * Landing page for attaching files to your application
     *
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function remove(Request $request, $id)
    {
        $application = Application::findOrFail($id);

        if (! in_array(auth()->user()->role, [User::USER_ROLE_ADMIN])) {
            if (auth()->id() !== $application->user_id) {
                return abort(403);
            }
        }

        Song::where('application_id', $application->id)->delete();

        $application->delete();

        if ($request->route()->getName() == 'admin_delete_application') {
            $request->session()->flash('alert-success', "Soft deleted the application. ");

            return back();
        }

        return redirect('/');
    }

    protected function file(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        if ($request->file('file')->isValid()) {
            return App::make('FileService')->requestToFileModel('file', $request, auth()->id());
        }

        return abort(500, 'Internal Server Error');
    }
}
