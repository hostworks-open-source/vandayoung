<?php

namespace App\Http\Controllers;

use App\File;
use App\Application;
use Illuminate\Http\Request;

class SubmissionFileController extends Controller
{
    public function index(Application $application)
    {
        if (auth()->id() !== $application->user_id) {
            return abort(403);
        }

        $application->load('songs.file');

        return $application->songs->pluck('file.name', 'id')->filter();
    }


    public function store(Application $application)
    {
        if (auth()->id() !== $application->user_id) {
            return abort(403);
        }

        return $application->songs()->create(['file_id' => request('file.id')]);
    }
}
