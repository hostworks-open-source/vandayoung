<?php

namespace App\Http\Controllers;

use App\Song;
use App\Application;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmissionSongController extends Controller
{
    public function destroy(Application $application, Song $song)
    {
        if (auth()->id() !== $application->user_id || $application->id !== $song->application->id) {
            abort(403);
        }

        $song->forceDelete();
        $song->file()->delete();

        return $song->forceDelete();
    }
}
