<?php

namespace App\Http\Controllers;

use App\Application;
use App\Country;
use App\Transaction;
use App\User;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;

class TransactionController extends Controller
{
    public function processPostBack(Request $request){
        $reference = $request->get('refid');
        $vandaCheck = substr($reference, 0, strpos($reference, '-')); // get the VANDA in VANDA-1231
        $txnid = $request->get('txnid');
        \Log::info("Vanda is: " . $vandaCheck);
        if($vandaCheck != "VANDA"){
            \Log::warning("Ignored: " . $reference);
            abort(400);
        }
        $transactionExists = Transaction::where('txnid', $txnid)->count();
        if($transactionExists > 0){
            \Log::warning("Transaction $reference exists $transactionExists times: ");
            return redirect('payment/confirmation');
        }
        $applicationId = substr($reference, strpos($reference, '-')+1); // get 'from' the - in VANDA-12312
        \Log::info("App ID is: " . $applicationId);
        $application = \App\Application::find($applicationId);

        $transaction = \App\Transaction::create();
        if($application){
            $transaction->application_id = $application->id;
            $transaction->user_id = $application->user->id;
        }
        $amountInCents = (int)$request->get('amount');
        $transaction->amount = $amountInCents;
        $transaction->settlement_date = $request->get('settdate');
        $transaction->surcharge = $request->get('suramount');
        $transaction->restext = $request->get('restext');
        $transaction->rescode = $request->get('rescode');
        $transaction->expiry = urldecode($request->get('expirydate'));
        $transaction->reference = $reference;
        $summaryCode = $request->get('summarycode');
        $transaction->summary_code = $summaryCode;
        $transaction->txnid = $txnid;
        $transaction->save();

        if($summaryCode == "1"){
            if($application) {
                \Log::info("App Bill Amount: " . $application->billAmount . " & Paid Amount: $amountInCents");
                if($application->billAmount == $amountInCents){
                    $application->status = Application::STATE_SONGS_PAID;
                    $application->save();
                }
            }
            $this->sendConfirmationEmail($transaction);
        }
        return redirect('payment/confirmation');
    }
    protected function sendConfirmationEmail($transaction){
        if($transaction->user){
            $amount = is_numeric($transaction->amount) ? ($transaction->amount / 100) : 0;
            $user = $transaction->user;
            Mail::send('emails.paymentTwo', ['application'=>$transaction->application, 'transaction' => $transaction, 'user'=>$user, 'amount'=>$amount], function ($m) use ($transaction, $user) {

                $m->to($user->email, "{$user->firstname} {$user->surname}")->subject('Payment Received');
            });
        }
    }
    public function processGetBack(){
        return redirect('payment/confirmation');
    }
    public function confirmation(){
        return view('payment/confirmation', ['user'=>\Auth::user()]);
    }
    public function deferredConfirmation(){
        return view('payment/deferred', ['user'=>\Auth::user()]);
    }
}
