<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;

class TranscodingController extends Controller
{
    public function update(Request $request)
    {
        $status = $request->input('status');
        $file = File::where('google_storage_key', $request->input('original_filename'))->firstOrFail();

        if (in_array($status, ['Probe Failed', 'Fetch failed', 'Failed Transcode'])) {
            $file->forceFill(['manifest_status' => File::MANIFEST_STATUS_FAILED])->save();
            return;
        }

        if (in_array($status, ['Pending', 'Probing', 'Probed', 'Transcoding', 'Generating manifest'])) {
            $file->forceFill(['manifest_status' => File::MANIFEST_STATUS_SENT])->save();
            return;
        }

        if ($status == 'Ready for playback') {
            $file->forceFill([
                'manifest_status' => File::MANIFEST_STATUS_SUCCESS,
                'manifest_url' => $request->input('manifest_url'),
                'embed_code' => $request->input('embed_code'),
            ])->save();
        }

        return;
    }
}
