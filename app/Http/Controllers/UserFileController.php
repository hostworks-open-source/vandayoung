<?php

namespace App\Http\Controllers;

use App\File;
use Ramsey\Uuid\Uuid;
use App\Helpers\General;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

class UserFileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store()
    {
        $googleStorageKey = null;

        $this->validate(request(), ['file.name' => 'required']);

        // The Symfony extension guesser seems to have trouble guessing the
        // extension of audio/mp3 files, so just use what was supplied.
        $fileParts = explode('.', request('file.name'));

        $googleStorageKey = sprintf('%s.%s', Uuid::uuid4()->toString(), array_pop($fileParts));

        return auth()->user()->files()->create([
            'name' => request('file.name'),
            'google_storage_key' => $googleStorageKey,
            'name_sh1' => sha1($googleStorageKey.uniqid()),
            'mime_type' => request('file.type', ''),
            'filesize' => request('file.size', ''),
            'manifest_status' => File::MANIFEST_STATUS_SENDING,
        ]);
    }
}
