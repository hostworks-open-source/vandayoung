<?php
namespace app\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;
class SubmissionsClosed
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if(!$this->auth->guest() and (in_array($this->auth->user()->role, [User::USER_ROLE_ADMIN, User::USER_ROLE_JUDGE]) or $request->session()->get('impersonatingUser'))){
            return $next($request);
        }
        if(\App\Helpers\General::appsHaveClosed()){
            return redirect('applications');
        }
        return $next($request);
    }
}