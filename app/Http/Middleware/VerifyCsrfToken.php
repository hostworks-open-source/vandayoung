<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'payment/postback',
        'submissions/file',
        'manifest/process',
        'api/v1/transcode/create',
        'submissions/emptysong',
        'submissions/song',
    ];
}
