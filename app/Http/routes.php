<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ApplicationController@homeRedirect');
Route::get('/applications', 'ApplicationController@getUserApplications');

Route::get('/healthcheck',  'HealthCheckController@check');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/update', 'ApplicationController@getUpdatePassword');
Route::post('password/update', 'ApplicationController@postUpdatePassword');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/*Route::get('details', [
    'middleware' => 'auth',
    'uses' => 'Auth\AuthController@getUpdate'
]);
Route::post('details', [
    'middleware' => 'auth',
    'uses' => 'Auth\AuthController@postUpdate'
]);*/
Route::post('users/files', 'UserFileController@store');
Route::get('details', 'ApplicationController@getAccountUpdate');
Route::post('details', 'ApplicationController@postAccountUpdate');
Route::get('applications/create', 'ApplicationController@getApplication');
Route::get('redirect/app', 'ApplicationController@redirectToApplicationOrCreate');
Route::post('app', 'ApplicationController@postApplication');
Route::get('app/{id}', 'ApplicationController@getApplication');
Route::post('app/{id}', 'ApplicationController@postApplication');
Route::get('api/file/{file}', 'ApplicationController@getFileData');
Route::get('application/{id}', 'SubmissionController@previewSubmission');
Route::get('preview', 'SubmissionController@previewRedirect');
Route::get('emailpreview', 'SubmissionController@emailPreview');
Route::get('emailpreview/newuser', 'SubmissionController@emailPreviewNewUser');
Route::get('emailpreview/forgotpassword', 'SubmissionController@forgotPassword');
Route::get('emailpreview/defferedpayment', 'SubmissionController@emailPreviewDefferedPayment');
Route::get('emailpreview/submitted', 'SubmissionController@emailPreviewSubmitted');
Route::get('pay/{id}', 'SubmissionController@pay');
Route::get('judge/submit/{id}', 'SubmissionController@pay');
Route::get('submissions/add/{id}', 'SubmissionController@index');
Route::get('deferred', 'TransactionController@deferredConfirmation');
Route::get('submitted', 'SubmissionController@freeSubmitConfirmation');
Route::post('submissions/file', 'SubmissionController@file');
Route::get('submissions/{application}/files', 'SubmissionFileController@index');
Route::post('submissions/{application}/files', 'SubmissionFileController@store');
Route::post('submissions/{application}/files/duplicate', 'DuplicateWorkDetailsController@store');
Route::post('submissions/song', 'SubmissionController@song');
Route::post('submissions/emptysong', 'SubmissionController@emptySong');
Route::delete('submissions/{application}/songs/{song}', 'SubmissionSongController@destroy');
Route::get('submissions/remove/{id}', 'SubmissionController@remove');
Route::get('admin/submissions/remove/{id}', 'SubmissionController@remove')->name('admin_delete_application');
Route::get('admin/submissions/unremove/{id}', 'SubmissionController@unremove');
Route::get('media/stream/{sha1Name}/{name}', 'FileController@media');
Route::get('media/download/{sha1Name}/{name}', 'FileController@media');
Route::post('payment/postback', 'TransactionController@processPostBack');
Route::get('payment/postback', 'TransactionController@processGetBack');
Route::get('payment/confirmation', 'TransactionController@confirmation');
Route::get('admin/songs', 'AdminController@listSongs')->name('adminListSongs');
Route::get('admin/users', 'AdminController@listUsers')->name('adminListUsers');
Route::post('admin/remove/scores/from/round', 'AdminController@removeScoreFromRound');
Route::get('admin/applications', 'AdminController@listApplications')->name('adminListApplications');
Route::get('admin/preview/song/{id}', 'AdminController@previewSong')->name("previewSong");
Route::get('admin/change/app/state/{id}/{status}', 'AdminController@changeApplicationStatusTo')->name("changeAppStatus");
Route::get('admin/impersonate/account/{id}', 'AdminController@impersonateUser');
Route::get('admin/stop/impersonating', 'GeneralController@exitImpersonatingUser');
Route::post('admin/songs/to/round', 'AdminController@assignUsersToRound');
Route::get('admin/shortlist/song/to/round', 'AdminController@shortlistSongToRound')->name('shortListSongToRound');
Route::post('admin/delete/all/assignments', 'AdminController@deleteAllSongRoundsForJudgeByRound')->name('deleteAllSongRoundsForJudgeByRound');
Route::post('admin/remove/songs/to/round', 'AdminController@deleteUsersToRound');
Route::post('admin/score', 'AdminController@assignScoreToSong');
Route::post('admin/markasread', 'AdminController@markAsRead');
Route::get('admin/user/{id}', 'AdminController@getManageUser');
Route::post('admin/user/{id}', 'AdminController@postManageUser');
Route::get('admin/user', 'AdminController@getManageUser');
Route::post('admin/user', 'AdminController@postManageUser');
Route::get('admin/dashboard', 'AdminController@dashboard');
Route::get('admin/panels', 'AdminController@getPanels');
Route::post('transcoding/media', 'TranscodingController@update');
Route::any('api/v1/transcode/create', 'TranscodingController@receiveMockNewFilePost');
Route::get('admin/panel/{id}', 'AdminController@getManagePanel');
Route::post('admin/panel/{id}', 'AdminController@postManagePanel');
Route::get('admin/panel', 'AdminController@getManagePanel');
Route::post('admin/panel', 'AdminController@postManagePanel');
Route::get('admin/view/scores/{id}/{roundId}', 'AdminController@viewScores')->name('adminViewScores');
Route::get('admin/export/scores', 'AdminController@exportScoresToExcel')->name('adminExportScores');

