<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manifest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manifests';

    public function file()
    {
        return $this->belongsTo('App\File');
    }
}