<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'panels';

    protected $fillable = ['name'];

    public function users() {
        return $this->belongsToMany('App\User', 'panel_users', 'panel_id', 'user_id');
    }

    public function panelUsers(){
        return $this->hasMany('App\PanelUser');
    }
    public function getUuidAttribute(){
        return 'panel-'.$this->id;
    }
    public function songRound(){
        return $this->hasMany('App\SongRound');
    }
    public function getDisplayNameAttribute(){
       return $this->name;
    }
}