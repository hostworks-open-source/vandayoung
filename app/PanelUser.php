<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'panel_users';

    public function panel(){
        $this->belongsTo('App\Panel');
    }
}