<?php

namespace App\Providers;

use App\File;
use App\Song;
use Validator;
use App\Category;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request, Router $router)
    {
        if (app()->environment('staging', 'production')) {
            URL::forceSchema('https');
        }

        $router->model('application', Application::class);
        $router->model('song', Song::class);
        $router->model('file', File::class);

        /**
         * Tests it's an email that non one else has, without choking on your email entry in the DB.
         */
        Validator::extend('update_email', function ($attribute, $value, $parameters, $validator) use ($request) {
            $count = \App\User::where('id', '!=', \Auth::user()->id)
                ->where('email', $value)
                ->count();

            return $count == 0;
        });
        Validator::extend('admin_update_email', function ($attribute, $value, $parameters, $validator) use ($request) {
            $count = \App\User::where('id', '!=', $request->get('id'))
                ->where('email', $value)
                ->count();

            return $count == 0;
        });
        /**
         * Belongs to user OR is uploaded valid file
         */
        Validator::extend('valid_file', function($attribute, $value, $parameters, $validator) use($request) {
            if(is_numeric($value)){
                $count = File::where('user_id', auth()->id())
                    ->where('id', $value)
                    ->count();
                return $count == 1;
            }
            return $request->hasFile($attribute) && $request->file($attribute)->isValid();
        });
        /**
         * Is the logged in users pre-existing password.
         */
        Validator::extend('is_old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, auth()->user()->password);
        });
        /**
         * Word Count
         */
        Validator::extend('max_words', function ($attribute, $value, $parameters) {
            return count(preg_split('/\s+/', $value)) <= $parameters[0];
        });
        Validator::replacer('max_words', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':word_count', $parameters[0], $message);
        });

        /**
         * File in DB's filename ends with file extension
         */
        Validator::extend('has_file_for_category', function($attribute, $value, $parameters, $validator) use($request) {
            $category = Category::findOrFail($request->get('category'));
            $file = File::findOrFail($request->get('file'));
            $fileTypes = explode(',', $category->file_types);

            foreach ($fileTypes as $fileType) {
                $type = ".{$fileType}";
                $result = substr_compare($file->name, $type, strlen($file->name) - strlen($type), strlen($type), true);
                if ($result === 0) {
                    return true;
                }
            }

            return false;
        });


        Validator::replacer('category_check_max_entries', function ($message, $attribute, $rule, $parameters) use ($request) {
            $category = \App\Category::findOrFail($request->get('category'));

            return str_replace(':max_entries', $category->max_entries, $message);
        });
        /**
         * User does not have more than X entries in this category.
         */
        Validator::extend('category_check_max_entries', function ($attribute, $value, $parameters, $validator) use ($request) {
            $category = Category::findOrFail($request->get('category'));

            if (is_numeric($category->max_entries)) {
                return auth()->user()->files()->whereHas('song', function ($query) use ($category) {
                    $query->where('category_id', $category->id);
                })->count() <= $category->max_entries;
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EloquentService', function ($app) {
            return new \App\Service\EloquentService();
        });
        $this->app->bind('FileService', function () {
            return new \App\Service\FileService();
        });
        $this->app->bind('StatsService', function () {
            return new \App\Service\StatsService();
        });
        $this->app->bind('ExportService', function ($app) {
            return new \App\Service\ExportService($app->make('ExcelService'), $app->make('FileService'));
        });
        $this->app->bind('ExcelService', function () {
            return new \App\Service\ExcelService();
        });
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
        $this->app->bind('JudgingService', function () {
            return new \App\Service\JudgingService();
        });
        $this->app->bind('SearchService', function () {
            return new \App\Service\SearchService();
        });
    }
}
