<?php

namespace App\Providers;

use GuzzleHttp\Client;
use App\Foundry\Console\ApiClient;
use App\Foundry\Console\Configuration;
use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->singleton(ApiClient::class, function () {
            return new ApiClient(
                new Configuration(config('services.console.api_key'), config('services.console.cache_timeout'), config('services.console.uat')),
                new Client(['httperrors' => false]),
                app('cache.store')
            );
        });
    }
}
