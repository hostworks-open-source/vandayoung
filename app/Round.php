<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $table = 'rounds';

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'song_rounds')->withPivot('judge_id')->withTimestamps();
    }

    public function songRounds()
    {
        return $this->belongsToMany('App\SongRound');
    }

    public function songRoundAverages()
    {
        return $this->belongsToMany('App\SongRoundAverage');
    }

    public function scores()
    {
        return $this->hasMany('App\Scores');
    }
    public function isActive(){
        $now = \date_create();
        return ($now >= \date_create($this->starts) && $now < \date_create($this->ends));
    }
}