<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scores';
    protected $fillable = ['song_round_id'];

    public function song()
    {
        return $this->belongsTo('App\Song');
    }

    public function round()
    {
        return $this->belongsTo('App\Round');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function songRound(){
        return $this->belongsTo('App\SongRound');
    }
}