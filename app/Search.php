<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'searches';
    protected $fillable = ['mysearch', 'user_id'];

    public function getQueryArrayAttribute(){
        return json_decode($this->mysearch, true);
    }
    public function get($key){
        $query = $this->getQueryArrayAttribute();
        if(isset($query[$key])){
            return $query[$key];
        }
        return null;
    }
}