<?php
namespace App\Service;
class EloquentService
{
    public function saveOrGetSearch($params, $userId){
        $query = json_encode($params);
        $search = \App\Search::firstOrCreate(['mysearch' => $query, 'user_id'=>$userId]);
        return $search;
    }
}
