<?php
namespace App\Service;
class ExcelService {
    public $phpExcel;
    public $excelIoFactory;
    public function getPhpExcel(){
        return new \PHPExcel();
    }
    public function createWriter($phpExcel, $writerType = 'Excel2007'){
        $this->excelIoFactory = \PHPExcel_IOFactory::createWriter($phpExcel, $writerType);
        return $this->excelIoFactory;

    }
    public function sendExcel2007Headers($fileName){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$fileName");
        header('Cache-Control: max-age=0');
    }
}
