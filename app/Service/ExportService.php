<?php
namespace App\Service;
use App\Application;
use App\Round;
use App\Score;

class ExportService {
    /**
     * @var ExcelService App/Service/ExcelService
     */
    public $excelService;
    /**
     * @var FileService App/Service/FileService
     */
    public $fileService;

    public function __construct($excelService, $fileService)
    {
        $this->excelService = $excelService;
        $this->fileService = $fileService;
        $this->uploadPath = base_path() . \Config::get('app.fileUploadPath');
    }


    public function checkSheetTitle($pValue)
    {
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        // Some of the printable ASCII characters are invalid:  * : / \ ? [ ]
        if (str_replace($invalidCharacters, '', $pValue) !== $pValue) {
        
            //Hack to remove bad characters from sheet name instead of throwing an exception
            $pValue = str_replace($invalidCharacters, '', $pValue);
        }

        $pValue = substr($pValue, 0, 30);

        return $pValue;
    }

    /**
     * @return mixed App/File
     * @param $queryBuilder
     */
    public function exportApplicationsToExcel($queryBuilder){
        $excel = $this->excelService->getPhpExcel();
        $writer = $this->excelService->createWriter($excel);
        $objSheet = $excel->getActiveSheet();
        $objSheet->setTitle('Applications');
        $objSheet->getStyle('A1:U1')->getFont()->setBold(true)->setSize(12);
        $headers = ['ID', 'Firstname', 'Surname', 'Email', 'Completed', 'Status', ucfirst(\Config('app.filesName')) . ' Count', 'State', 'Country', 'Gender', 'Birthday', 'Updated'];
        if(\Config('app.canMakePayments')){
           $headers[] = 'Has Paid';
           $headers[] = 'Paid Total';
        }
        if(\Config('app.songsHaveCategories')){
            $headers[] = 'Categories';
        }
        $objSheet->fromArray($headers, null, 'A1');

        $fileName = date("d-m-Y_H-i-s_") . uniqid() . "_applications.xlsx";

        $this->populateSpreadsheetFromQueryBuilder($queryBuilder, $objSheet);

        $this->excelService->sendExcel2007Headers($fileName);
        $writer->save('php://output');

    }
    public function populateSpreadsheetFromQueryBuilder($queryBuild, $activeSheet){
        $applications = $queryBuild->get();
        $saveArray = [];
        foreach($applications as $application){
            $hasCompleted = in_array($application->status, [Application::STATE_SUBMITTED, Application::STATE_DEFERRED, Application::STATE_SONGS_PAID]);

            if (empty($application->user)) {
                continue;
            }

            $state = !empty($application->state) ? $application->state : $application->user->state;

            $country = $application->country ? $application->country->nicename : ($application->user->country ? $application->user->country->nicename : '');
            $gender = $application->user->gender;

            if($application->dob != "0000-00-00 00:00:00" and $date = date_create($application->dob)){
                $date = $date->format('d/m/Y');
            } else if ($application->user->dob != "0000-00-00 00:00:00" and $date = date_create($application->user->dob)){
                $date = $date->format('d/m/Y');
            } else {
                $date = '';
            }
            $newRow = [
                \Config('app.applicationPrefix') . '-' . $application->id,
                $application->user->firstname,
                $application->user->surname,
                $application->user->email,
                $hasCompleted,
                $application->formattedStatus,
                $application->songs()->count(),
                $state,
                $country,
                $gender,
                $date,
                $application->updated_at->format('d/m/Y H:i:s')
            ];
            if(\Config('app.canMakePayments')){
                $newRow[] = $application->status == Application::STATE_SONGS_PAID;
                $newRow[] = $application->totalPaid;
            }
            if(\Config('app.songsHaveCategories')){
                $newRow[] = $application->getUniqueCategories()->implode('name', ', ');
            }
            $saveArray[] = $newRow;
        }
        $activeSheet->fromArray($saveArray, null, 'A2');
    }

    public function exportScoresToSpreadsheet(){
        $excel = $this->excelService->getPhpExcel();
        $writer = $this->excelService->createWriter($excel);
        $rounds = Round::all();
        foreach($rounds as $key => $round){
            if($key == 0){
                $worksheet = $excel->getActiveSheet();
            } else {
                $worksheet = $excel->createSheet();
            }
            $worksheet->setTitle($this->checkSheetTitle($round->name));

            $query = Score::where('round_id', $round->id)->orderBy('user_id');
            $query->leftJoin('songs as s', function($join){
                $join->on('scores.song_id', '=', 's.id');
            });
            $query->select('scores.*');
            $query->orderBy('s.category_id', 'asc');
            $query->orderBy('score', 'desc');

            $scores = $query->get();

            $save = [];
            $headerTemplate = ["Title", "Application", "Score", 'Shortlisted', 'Judge', 'Author'];
            if(\Config('app.songsHaveCategories')){
                $headerTemplate[] = "Category";
            }
            $save[] = $headerTemplate;
            foreach($scores as $score){
                if (empty($score->song) || empty($score->song->application)) {
                   continue;
                }
                $row = [$score->song->title, $score->song->application->formattedId, $score->score, $score->song->isShortListed($round->id), $score->user->fullName];
                if($score->song->application->user){
                    $row[] = $score->song->application->user->fullName;
                } else {
                    $row[] = '';
                }
                if(\Config('app.songsHaveCategories')){
                    if($score->song->category){
                        $row[] = $score->song->category->name;
                    }
                }
                $save[] = $row;
            }

            $worksheet->getStyle('A1:U1')->getFont()->setBold(true)->setSize(12);

            $worksheet->fromArray($save, null, 'A1');
        }

        # make ID shorter to prevent errors with filename title length
        #   production.ERROR: PHPExcel_Exception: Maximum 31 characters allowed in sheet title. 
        $uniqueID = substr(uniqid(), 0, 3);

        $fileName = date("d-m-Y_H-i-s_") . $uniqueID . "_scores.xlsx";

        $this->excelService->sendExcel2007Headers($fileName);
        $writer->save('php://output');
    }
}
