<?php

namespace App\Service;

use App\File;
use Illuminate\Support\Facades\Storage;

class FileService
{
    public function requestToFileModel($fileKey, $request, $userId = null)
    {
        $file  = $request->file($fileKey);
        $fileName = sha1($file->getClientOriginalName() . uniqid());

        return File::create([
            'user_id' => $userId,
            'name' => $file->getClientOriginalName(),
            'name_sha1' => $fileName,
            'filesize' => $file->getSize(),
            'mime_type' => $file->getMimeType(),
        ]);
    }

    public function deleteFile($fileId)
    {
        if ($file = File::find($fileId)) {
            $disk = Storage::disk('users');

            if ($disk->has($file->name_sha1)) {
                $disk->delete("{$file->user->id}/{$file->name_sha1}");
            }

            $file->forceDelete();
        }
    }
}
