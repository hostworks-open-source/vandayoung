<?php
namespace App\Service;
use Illuminate\Support\Facades\DB;
use App\SongRoundAverage;
use App\Song;
use App\Score;
use App\SongRound;
class JudgingService {
    public function calculateAndSaveScoreAverage($songId, $roundId){
        $avg = DB::table('scores')
            ->where('song_id', $songId)
            ->where('round_id', $roundId)
            ->avg('score');
        $count = DB::table('scores')
            ->where('song_id', $songId)
            ->where('round_id', $roundId)
            ->count('score');

        $scoreAverage = SongRoundAverage::where('song_id', $songId)->where('round_id', $roundId)->first();
        if($count == 0){
            if($scoreAverage){
                $scoreAverage->delete();
            }
            return;
        }
        if(!$scoreAverage){
            $scoreAverage = SongRoundAverage::create(['round_id'=>$roundId, 'song_id'=>$songId]);
        }
        $scoreAverage->song_id = $songId;
        $scoreAverage->round_id = $roundId;
        $scoreAverage->average = $avg;
        $scoreAverage->has_completed = $this->_areAllVotesInForSong($songId, $roundId);
        $scoreAverage->save();
    }

    public function _areAllVotesInForSong($songId, $roundId){
        $song = Song::findOrFail($songId);
        $panels = [];
        $users = [];
        foreach($song->songRounds as $mySongRound){
            if($mySongRound->panel){
                $panels[] = $mySongRound->panel;
            }
            if($mySongRound->judge){
                $users[] = $mySongRound->judge;
            }
        }
        foreach($panels as $panel){
            foreach($panel->users as $user){
                $voteCount = Score::where('song_id', $song->id)->where('round_id', $roundId)->where('user_id', $user->id)->count();
                if($voteCount < 1){
                    return false;
                }
            }
        }
        foreach($users as $user){
            $voteCount = Score::where('song_id', $song->id)->where('round_id', $roundId)->where('user_id', $user->id)->count();
            if($voteCount < 1){
                return false;
            }
        }
        return true;
    }
    public function getSongAveragesByRound($songs, $roundId){
        $songAverages = [];
        foreach($songs as $song){
            $avg = SongRoundAverage::where('round_id', $roundId)->where('song_id', $song->id)->first();
            if($avg){
                $songAverages[$song->id] = $avg->average;
            }
        }
        return $songAverages;
    }
    public function getSongScoresByJudge($songs, $roundId, $judgeId){
        $songScores = [];
        foreach($songs as $song){
            $score = Score::where('round_id', $roundId)->where('song_id', $song->id)->where('user_id', $judgeId)->first();
            if($score){
                $songScores[$song->id] = $score->score;
            }
        }
        return $songScores;
    }

    public function createSongRoundForPanel($songId, $roundId, $panelId){
        $songRound = new SongRound;
        $songRound->song_id = $songId;
        $songRound->round_id = $roundId;
        $songRound->panel_id = $panelId;
        $songRound->save();
        return $songRound;
    }

    public function createSongRoundForJudge($songId, $roundId, $userId){
        $songRound = new SongRound;
        $songRound->song_id = $songId;
        $songRound->round_id = $roundId;
        $songRound->judge_id = $userId;
        $songRound->save();
        return $songRound;
    }

    public function createSongRoundForUser($selection, $roundId, $panelId){
        $songRound = SongRound::create();
        $songRound->song_id = $selection;
        $songRound->round_id = $roundId;
        $songRound->panel_id = $panelId;
        $songRound->save();
        return $songRound;
    }
}