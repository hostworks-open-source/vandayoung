<?php
namespace App\Service;
use App\Song;
use App\Http\Controllers\AdminController;
use App\Application;
use App\Category;
use App\User;
use Illuminate\Support\Facades\DB;

class SearchService {
    public $songSearch;
    protected $hasJoin = false;
    protected $showOnlyMyScore = false;
    protected $hasScore = false;
    protected $rounds;
    protected $hadNoFilter = false;

    public function setSongSearch($songSearch){
        $this->songSearch = $songSearch;
    }
    public function myAssignedJoin($roundId, $user){
        $this->songSearch->whereHas('songRounds', function ($query) use ($roundId, $user) {
            $query->where(function($query) use ($roundId, $user){
                $panelIds = explode(',', $user->panels->implode('id', ','));
                $query->where('judge_id', $user->id)
                    ->orWhereIn('panel_id', $panelIds);
            });
            $query->where('round_id', $roundId);
        });
        $this->songSearch->leftJoin('scores as sc', function($join) use ($roundId, $user){
            $join->on('songs.id', '=', 'sc.song_id')
                ->where('sc.round_id', '=', $roundId)
                ->where('sc.user_id', '=', $user->id);
        });
    }
    public function processFilter($filter, $request, $roundId, $user){
        switch($filter){
            case AdminController::FILTER_PENDING_ASSIGNED_TO_ME:
                $this->myAssignedJoin($roundId, $user);
                $this->hasScore = true;
                $this->showOnlyMyScore = true;
                $this->hasJoin = true;
                break;
            case AdminController::FILTER_COMPLETED_ASSIGNED_TO_ME:
                $this->myAssignedJoin($roundId, $user);
                $this->songSearch->whereNotNull('sc.id');
                $this->hasScore = true;
                $this->showOnlyMyScore = true;
                $this->hasJoin = true;
                break;
            case AdminController::FILTER_NOT_IN_ROUND:
                $this->songSearch->whereDoesntHave('rounds', function ($query) use ($request) {
                    $query->where('round_id', $request->get('round'));
                });
                $this->hasScore=true;
                break;
            case AdminController::FILTER_ROUND_COMPLETED:
                $this->songSearch->whereHas('songRoundAverages', function ($query) use ($roundId) {
                    $query->where('round_id', $roundId);
                    $query->where('has_completed', true);
                });
                $this->hasScore=true;
                break;
            case AdminController::FILTER_SHORTLISTED:
                $this->songSearch->whereHas('songRoundAverages', function ($query) use ($roundId) {
                    $query->where('round_id', $roundId);
                    $query->where('shortlisted', true);
                });
                $this->hasScore=true;
                break;
            case AdminController::FILTER_ROUND_PENDING:
                $this->songSearch->leftjoin('song_round_averages as songRoundAvg', function ($join) use ($roundId) {
                    $join->on('songs.id', '=', 'songRoundAvg.song_id');
                    $join->where('songRoundAvg.round_id', '=', $roundId);
                });
                $this->songSearch->where(function($query) {
                    $query->whereNull('songRoundAvg.id');
                    $query->orWhere('songRoundAvg.has_completed', 0);
                });
                $this->songSearch->whereHas('rounds', function($query) use($roundId){
                    $query->where('rounds.id', $roundId);
                });
                $this->hasScore=true;
                $this->hasJoin=true;
                break;
            default:
                $this->hadNoFilter = true;
                break;
        }
    }
    public function processSearch($searchTerm, $searchType){
        if($searchTerm){
            $filterArray['search_term'] = $searchTerm;
            $filterArray['search_type'] = $searchType;
            switch($filterArray['search_type']){
                case AdminController::SEARCH_TYPE_APPLICATION_ID:
                    $searchArray = explode('-', $searchTerm);
                    $searchId = end($searchArray);
                    //$filterArray = [];
                    $this->songSearch = \App\Song::where('application_id', $searchId);
                    break;
                case AdminController::SEARCH_TYPE_EMAIL:
                    $this->songSearch->whereHas('file', function($query) use ($searchTerm){
                        $query->whereHas('user', function ($query) use ($searchTerm) {
                            $query->where('email', 'LIKE', "%$searchTerm%");
                        });
                    });
                    break;
                case AdminController::SEARCH_TYPE_TITLE:
                    $this->songSearch->where('title', 'LIKE', "%$searchTerm%");
                    break;
                case AdminController::SEARCH_TYPE_NAME:
                    $nameParts = explode(' ', $searchTerm);
                    $this->songSearch->whereHas('file', function($query) use ($nameParts, $searchTerm){
                        $query->whereHas('user', function ($query) use ($nameParts, $searchTerm) {
                            if(str_word_count($searchTerm) == 1){
                                $query->where(function($query) use($nameParts) {
                                    $query->where('surname', 'LIKE', "%{$nameParts[0]}%");
                                    $query->orWhere('firstname', 'LIKE', "%{$nameParts[0]}%");
                                });
                            } else {
                                $query->where('firstname', 'LIKE', "%{$nameParts[0]}%");
                                if(isset($nameParts[1])){
                                    $query->where('surname', 'LIKE', "%{$nameParts[1]}%");
                                }
                            }
                        });
                    });

                    break;
            }
        }
    }
    public function processSort($sort, $roundId, $user){
        switch($sort){
            case AdminController::SORT_SCORE_ASC:
                if($this->showOnlyMyScore){
                    $this->songSearch->leftJoin('scores as scr', function($join) use ($roundId, $user) {
                        $join->on('songs.id', '=', 'scr.song_id')
                            ->where('scr.round_id', '=', $roundId)
                            ->where('scr.user_id', '=', $user->id);
                    })->orderBy('scr.score', 'asc');
                } else {
                    $this->songSearch->leftJoin('song_round_averages as savg', function($join) use ($roundId) {
                        $join->on('songs.id', '=', 'savg.song_id')
                            ->where('savg.round_id', '=', $roundId);
                    })->orderBy('savg.average', 'asc');
                }
                $this->hasJoin=true;
                break;
            case AdminController::SORT_SCORE_DESC:
                if($this->showOnlyMyScore){
                    $this->songSearch->leftJoin('scores as scr', function($join) use ($roundId, $user) {
                        $join->on('songs.id', '=', 'scr.song_id')
                            ->where('scr.round_id', '=', $roundId)
                            ->where('scr.user_id', '=', $user->id);
                    })->orderBy('scr.score', 'desc');
                } else {
                    $this->songSearch->leftJoin('song_round_averages as savg', function($join) use ($roundId) {
                        $join->on('songs.id', '=', 'savg.song_id')
                            ->where('savg.round_id', '=', $roundId);
                    })->orderBy('savg.average', 'desc');
                }
                $this->hasJoin=true;
                break;

            case AdminController::SORT_ASC:
                $this->songSearch->orderBy('songs.title', 'ASC');
                break;
            case AdminController::SORT_DESC:
                $this->songSearch->orderBy('songs.title', 'DESC');
                break;
            case AdminController::SORT_DATE_ASC:
                $this->songSearch->orderBy('created_at', 'ASC');
                break;
            case AdminController::SORT_DATE_DESC:
                $this->songSearch->orderBy('created_at', 'DESC');
                break;
        }
    }
    public function processExtra($extra){
        if(!empty($extra)){
            switch($extra){
                case AdminController::FILTER_PAID_APPLICATION:
                    $this->songSearch->whereHas('application', function ($query) {
                        $query->where('status', Application::STATE_SONGS_PAID);
                    });
                    break;
            }
        }
    }
    public function hadNoFilter(){
        return $this->hadNoFilter;
    }
    public function searchFromRequestObject($request, $user){
        $this->songSearch = Song::where('songs.id', '>', 0);
        $this->processFilter($request->get('filter'), $request, $request->get('round'), $user);
        $this->processExtra($request->get('extra'));
        $this->processSearch($request->get('search_term'), $request->get('search_type'));
        $this->processSort($request->get('sort'), $request->get('round'), $user);
        $this->processCategoryFilter($request->get('category_id'));
        //$this->filterOutChildrenOfMultiTracks();
    }
    public function searchAnyOrder($request, User $user){
        $this->songSearch = Song::where('songs.id', '>', 0);
        $this->processFilter($request->get('filter'), $request, $request->get('round'), $user);
        $this->processExtra($request->get('extra'));
        $this->processSearch($request->get('search_term'), $request->get('search_type'));
        $this->processCategoryFilter($request->get('category_id'));
        $this->filterOutChildrenOfMultiTracks();
    }
    public function processCategoryFilter($categoryId){
        if($categoryId){
            $category = Category::findOrFail($categoryId);
            $this->songSearch->where('songs.category_id', $category->id);
        }
    }
    public function filterOutChildrenOfMultiTracks(){
        $multitrackCategories = \App\Category::where('is_multi_track', null)->get();
        if($multitrackCategories->count() > 0){
            $excludeIds = explode(',', $multitrackCategories->implode('id', ','));
            $this->songSearch->where(function($query) use($excludeIds){
                $query->whereIn('songs.category_id', $excludeIds);
                $query->orWhere('songs.category_id', null);
            });
        }
    }
    public function hasJoin(){
        return $this->hasJoin;
    }
    public function hasScore(){
        return $this->hasScore;
    }
    public function showOnlyMyScore(){
        return $this->showOnlyMyScore;
    }
    public function orderByRandom(){
        $this->songSearch->orderBy(DB::raw('RAND()'));
    }
    public function justLoadIds(){
        $this->songSearch->select("songs.id");
    }
    public function fetchAllFields(){
        $this->songSearch->select("songs.*");
    }
    public function get(){
        return $this->songSearch->get();
    }
}