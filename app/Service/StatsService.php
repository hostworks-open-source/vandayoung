<?php
namespace App\Service;
use DB;
class StatsService {
    public function getCountOfApplications($from, $to){
        $appCount = DB::select('select count(*) as count from applications where created_at > ? and created_at < ? and deleted_at IS NULL', [$from, $to]);
        if(isset($appCount[0])){
            return $appCount[0]->count;
        }
        return 0;
    }
    public function getCountOfUsers($from, $to){
        $userCount = DB::select('select count(*) as count from users where created_at > ? and created_at < ?', [$from, $to]);
        if(isset($userCount[0])){
            return $userCount[0]->count;
        }
        return 0;
    }
    public function getCountOfWorks($from, $to){
        $worksCount = DB::select('select count(*) as count from songs where created_at > ? and created_at < ? and deleted_at IS NULL', [$from, $to]);
        if(isset($worksCount[0])){
            return $worksCount[0]->count;
        }
        return 0;
    }
    public function getCountOfWorksWithStatus($from, $to, $status){
        $worksCount = DB::select('
            select count(*) as count from songs
            join applications on songs.application_id = applications.id
            where applications.status = ? and applications.updated_at > ? and applications.updated_at < ? and applications.deleted_at IS NULL', [$status, $from, $to]);
        if (isset($worksCount[0])) {
            return $worksCount[0]->count;
        }
        return 0;
    }
    public function getCountOfWorksWithStatusBySongDate($from, $to, $status){
        $worksCount = DB::select('
            select count(*) as count from songs
            join applications on songs.application_id = applications.id
            where applications.status = ? and songs.created_at > ? and songs.created_at < ? and applications.deleted_at IS NULL', [$status, $from, $to]);
        if(isset($worksCount[0])){
            return $worksCount[0]->count;
        }
        return 0;
    }
}
