<?php

namespace App;

use App\File;
use App\Genre;
use App\Category;
use App\Application;
use App\Helpers\General;
use App\Service\FileService;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Song extends Model
{
    use SoftDeletes;

    const FILE_TYPES = ['queue_sheet', 'main_score', 'lead_sheet', 'main_score_mid'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'songs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_id', 'title', 'year', 'lyrics', 'other_genre', 'first_venue', 'additional_composers'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the application
     */
    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    /**
     * Get the genre
     */
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    /**
     * Get the genre
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the application
     */
    public function file()
    {
        return $this->belongsTo(File::class);
    }

    /**
     * Get the application
     */
    public function queue_sheet()
    {
        return $this->belongsTo(File::class, 'queue_sheet_file_id');
    }

    public function main_score()
    {
        return $this->belongsTo(File::class, 'main_score_file_id');
    }

    public function lead_sheet()
    {
        return $this->belongsTo(File::class, 'lead_sheet_file_id');
    }

    public function main_score_mid()
    {
        return $this->belongsTo(File::class, 'main_score_mid_file_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Song', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Song', 'parent_id');
    }

    public function isValid()
    {
        $required = ['title'];
        if (config('app.filesHaveGenre')) {
            $required[] = 'genre_id';
        }
        if (config('app.filesHaveCategory')) {
            $required[] = 'category_id';
        }
        if (config('app.filesHaveQueueSheet') and ($this->category and $this->category->cuesheet_required)) {
            $required[] =  'queue_sheet_file_id';
        }
        if (config('app.filesHaveYear')) {
            $required[] = 'year';
        }
        if (config('app.filesHaveReleaseDate')) {
            $required[] = 'first_broadcast';
        }
        if (config('app.filesHaveReleaseVenue')) {
            $required[] = "first_venue";
        }
        foreach ($required as $col) {
            if (empty($this->$col)) {
                return false;
            }
        }

        return true;
    }

    public function rounds()
    {
        return $this->belongsToMany('App\Round', 'song_rounds')->withPivot('judge_id')->withTimestamps();
    }

    public function songRounds()
    {
        return $this->hasMany('App\SongRound');
    }

    public function songRoundAverages()
    {
        return $this->hasMany('App\SongRoundAverage');
    }

    public function scores()
    {
        return $this->hasMany('App\Score');
    }

    public function getUniqueRoundsAttribute()
    {
        $roundIds = [];
        $uniqueRounds = [];
        foreach ($this->rounds as $round) {
            if (! in_array($round->id, $roundIds)) {
                $roundIds[] = $round->id;
                $uniqueRounds[] = $round;
            }
        }

        return $uniqueRounds;
    }

    public function isVideo()
    {
        if (! $this->file) {
            return false;
        }

        return $this->file->isVideo();
    }

    public function attachFile($type, $file)
    {
        if (! in_array($type, static::FILE_TYPES)) {
            throw new InvalidArgumentException("Invalid song file of type {$type}");
        }

        DB::beginTransaction();

        $this->{$type}()->associate(File::create([
            'name' => $name = $file->getClientOriginalName(),
            'user_id' => auth()->id(),
            'name_sha1' => sha1($name.uniqid()),
            'mime_type' => $file->getMimeType(),
            'filesize' => $file->getSize(),
        ]));

        $file->move(
            storage_path('app/public/users/'.auth()->id()),
            $file->getClientOriginalName()
        ) ? DB::commit() : DB::rollBack();
    }

    public function isShortListed($roundId)
    {
        $songAverage = $this->songRoundAverages()->where('round_id', $roundId)->first();
        if ($songAverage and $songAverage->shortlisted) {
            return true;
        }

        return false;
    }

    public function getDirectDownloadUrlAttribute()
    {
        if (! $this->isVideo()) {
            return url("/media/download/{$this->name_sha1}/{$this->name}");
        }
    }
}
