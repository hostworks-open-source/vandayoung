<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SongRound extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'song_rounds';

    public function song()
    {
        return $this->belongsTo('App\Song');
    }

    public function round()
    {
        return $this->belongsTo('App\Round');
    }

    public function judge()
    {
        return $this->hasOne('App\User', 'id', 'judge_id');
    }

    public function panel(){
        return $this->belongsTo('App\Panel');
    }

    public function songRoundAverage(){
        return $this->hasOne('App\SongRoundAverage');
    }

    public function scores(){
        return $this->hasMany('App\Score');
    }

    public function getEntityAttribute(){
        if($this->panel){
            return $this->panel;
        }
        if($this->judge){
            return $this->judge;
        }
    }
}