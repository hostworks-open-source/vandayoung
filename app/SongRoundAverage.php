<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongRoundAverage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'song_round_averages';
    protected $fillable = ['round_id', 'song_id'];

    public function song()
    {
        return $this->belongsTo('App\Song');
    }

    public function round()
    {
        return $this->belongsTo('App\Round');
    }
    public function songRound(){
        return $this->belongsTo('App\SongRound', 'song_round_id');
    }
}