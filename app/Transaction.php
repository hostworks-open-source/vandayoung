<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * Get the application
     */
    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getIsSuccessfulAttribute(){
        return $this->summary_code == "1";
    }
}