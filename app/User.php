<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements
    AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    const USER_ROLE_ADMIN = 'admin';
    const USER_ROLE_JUDGE = 'judge';
    const USER_ROLE_USER = 'user';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'surname', 'email', 'address', 'address2', 'city', 'state', 'postcode', 'phone', 'confirm_terms', 'password', 'gender', 'additional_composers', 'bio'];
    protected $required = ['firstname', 'surname', 'email', 'address', 'city', 'state', 'postcode', 'phone'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get files owned by user
     */

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function scores()
    {
        return $this->hasMany('App\Score');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    /**
     * Get applications owned by user
     */
    public function applications()
    {
        return $this->hasMany('App\Application');
    }

    public function panels()
    {
        return $this->belongsToMany('App\Panel', 'panel_users', 'user_id', 'panel_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function isValid()
    {
        if (\Config('app.profileHasBio')) {
            $this->required[] = 'bio';
        }
        if (\Config('app.profileHasProfileImage')) {
            $this->required[] = 'profile_image_file_id';
        }
        if (\Config('app.profileHasGender')) {
            $this->required[] = 'gender';
        }
        foreach ($this->required as $fill) {
            if (empty($this->$fill)) {
                return false;
            }
        }

        return true;
    }
    public function songRounds()
    {
        return $this->hasMany('App\SongRound', 'judge_id', 'id');
    }
    /**
     * Get profile image
     */
    public function profileImage()
    {
        return $this->belongsTo('App\File', 'profile_image_file_id');
    }
    public function getProfileImageUrlAttribute()
    {
        if (! $this->profileImage) {
            return;
        }

        return url("/app/users/{$this->id}/{$this->profileImage->name}");
    }
    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->surname;
    }
    public function getDisplayNameAttribute()
    {
        return $this->getFullNameAttribute();
    }
    public function getUuidAttribute()
    {
        return 'user-'.$this->id;
    }
}
