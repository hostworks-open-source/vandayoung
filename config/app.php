<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', true),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'http://localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Australia/Sydney',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => 'single',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\ConsoleServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Gate'      => Illuminate\Support\Facades\Gate::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,

    ],

    /*
     * FILE UPLOAD PATH
     */
    'fileUploadPath'=>'/uploads/',
    'transcodePath'=>env('TRANSCODE_DIR', ''),
    'transcoderUrl'=>env('TRANSCODER_URL',  ''),
    'transcoderApiKey'=>env('TRANSCODER_API_KEY',  ''),
    'transcoderApiUser'=>env('TRANSCODER_API_USER',  ''),

    'domain'=>env('APP_DOMAIN'),

    'gatewayUrl'=>env('GATEWAY_URL', ""),

    'paginateMax'=>env('PAGINATE_MAX', 100),

    'headerBackground'=>env('HEADER_BACKGROUND', '#95C5F5'),

    'logoHasContainer'=>env('LOGO_HAS_CONTAINER', true),

    'logoImage'=>env('LOGO_IMG'),

    'logoPath'=>env('LOGO_PATH'),

    'filesHaveGenre'=>env('FILES_HAVE_GENRE', true),

    'filesHaveCategory'=>env('FILES_HAVE_CATEGORY', false),

    'filesHaveQueueSheet'=>env('FILES_HAVE_QUEUE_SHEET', false),

    'filesHaveReleaseDate'=>env('FILES_HAVE_RELEASE_DATE', false),

    'filesHaveReleaseVenue'=>env('FILES_HAVE_RELEASE_VENUE', false),

    'filesMaxPerCategory'=>env('FILES_MAX_PER_CATEGORY', 3),

    'filesHaveYear'=>env('FILES_HAVE_YEAR', true),

    'filesHaveLyrics'=>env('FILES_HAVE_LYRICS', true),

    'filesHaveMainScore'=>env('FILES_HAVE_MAIN_SCORE', true),

    'filesHaveLeadSheet'=>env('FILES_HAVE_LEAD_SHEET', true),

    'filesHaveMidOfMainScore'=>env('FILES_HAVE_MID_OF_MAIN_SCORE', true),

    'filesName'=>env('FILES_NAME', 'song'),
    'filesNamePlural'=>env('FILES_NAME_PLURAL', 'songs'),

    'termsUrl'=>env('TERMS_URL', ''),

    'siteName'=>env('SITE_NAME'),
    'siteCompanyName'=>env('SITE_COMPANY_NAME'),

    'supportEmail'=>env('SUPPORT_EMAIL'),

    'supportPhone'=>env('SUPPORT_PHONE'),

    'liveChatUrl'=>env('LIVE_CHAT_URL'),

    'skipApplicationPage'=>env('SKIP_APPLICATION_PAGE', false),

    'canMakePayments'=>env('CAN_MAKE_PAYMENTS', true),

    'acceptedFileExtensions'=>env('ACCEPTED_FILE_EXTENSIONS', '.m4a,.mp3'),

    'profileHasAreYouAnApraMember'=>env('PROFILE_ARE_YOU_AN_APRA_MEMBER', false),

    'profileHasAreWorksRegistered'=>env('PROFILE_ARE_WORKS_REGISTERED', false),

    'profileHasProfileImage'=>env('PROFILE_HAS_PROFILE_IMAGE', false),

    'profileHasGender'=>env('PROFILE_HAS_GENDER', false),

    'profileHasAdditionalComposers'=>env('PROFILE_HAS_MORE_COMPOSERS', false),

    'profileHasBio'=>env('PROFILE_HAS_BIO', false),

    'applicationPrefix'=>env('APPLICATION_PREFIX', 'VANDA'),

    'templatePrefix'=>env('TEMPLATE_PREFIX', ''),

    'fileIcon'=>env('FILE_ICON', 'glyphicon-headphones'),
    'fileIconColor'=>env('FILE_ICON_COLOR', '#9A9A9A'),

    'userHasDob'=>env('USER_HAS_DOB', true),

    'filesHaveAdditionalComposers'=>env('FILES_HAVE_ADDITIONAL_COMPOSERS', false),

    'ffmpegLocation'=>env('FFMPEG_LOCATION', '/usr/local/ffmpeg/ffmpeg'),
    'httpTimeout'=>10,
    'httpValidateSsl'=>env('VALIDATE_SSL', true),

    'availableApplicationStates'=>env('AVAILABLE_APPLICATION_STATES', implode(",", [\App\Application::STATE_DEFERRED, \App\Application::STATE_UNPAID, \App\Application::STATE_SONGS_PAID])),
    'applicationCutOff'=>env('APPLICATION_CUT_OFF', false),

    'lowestScore'=>env('LOWEST_SCORE', 1),
    'highestScore'=>env('HIGHEST_SCORE', 100),
    'scoringHelp'=>env('SCORING_HELP', "Please give a score out of 100. "),
    'songsHaveCategories'=>env('SONGS_HAVE_CATEGORIES', false),
    'transcoderJobsPath'=>env('TRANSCODER_JOBS_PATH'),

    'foundry_bucket_incoming' => env('FOUNDRY_BUCKET_INCOMING'),
];
