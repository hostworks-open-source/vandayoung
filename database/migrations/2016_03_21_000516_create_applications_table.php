<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('gender', ['male', 'female', 'na']);
            $table->dateTime('dob');
            $table->boolean('on_behalf');
            $table->char('performing_name', 100);
            $table->char('performing_rights_association', 100);
            $table->text('bio');
            $table->char('address', 100);
            $table->char('address2', 100);
            $table->char('city', 70);
            $table->char('state', 30);
            $table->char('postcode', 10);
            $table->unsignedInteger('country_id')->nullable();
            $table->char('phone', 20);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('profile_image_file_id')->nullable();
            $table->unsignedInteger('parent_auth_file_id')->nullable();
            $table->timestamps();
            $table->foreign('profile_image_file_id')->references('id')->on('files');
            $table->foreign('parent_auth_file_id')->references('id')->on('files');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applications', function (Blueprint $table) {
            //
        });
    }
}
