<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->char('year', 4);
            $table->text('lyrics');
            $table->unsignedInteger('file_id')->nullable();
            $table->unsignedInteger('application_id')->nullable();
            $table->unsignedInteger('genre_id')->nullable();
            $table->timestamps();
        });
        Schema::table('songs', function (Blueprint $table) {
            $table->foreign('file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('application_id')->references('id')->on('applications')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('genre_id')->references('id')->on('genres')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('songs');
        Schema::drop('genres');
    }
}
