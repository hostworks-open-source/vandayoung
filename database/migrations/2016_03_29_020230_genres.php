<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Genres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO `genres` (`id`, `name`)
VALUES
	(1, 'Southern Rock'),
	(2, 'Soundtrack'),
	(3, 'Sound Clip'),
	(4, 'Soul'),
	(5, 'Sonata'),
	(6, 'Slow Rock'),
	(7, 'Slow Jam'),
	(8, 'Ska'),
	(9, 'Showtunes'),
	(10, 'Satire'),
	(11, 'Samba'),
	(12, 'Salsa'),
	(13, 'Rock & Roll'),
	(14, 'Rock'),
	(15, 'Rhythmic Soul'),
	(16, 'Revival'),
	(17, 'Retro'),
	(18, 'Reggae'),
	(19, 'R&B'),
	(20, 'Rave'),
	(21, 'Rap'),
	(22, 'Punk Rock'),
	(23, 'Punk'),
	(24, 'Psychedelic Rock'),
	(25, 'Psychedelic'),
	(26, 'Progressive Rock'),
	(27, 'Primus'),
	(28, 'Pranks'),
	(29, 'Power Ballad'),
	(30, 'Porn Groove'),
	(31, 'Pop/Funk'),
	(32, 'Pop-Folk'),
	(33, 'Pop'),
	(34, 'Polsk Punk'),
	(35, 'Polka'),
	(36, 'Other'),
	(37, 'Opera'),
	(38, 'Oldies'),
	(39, 'Noise'),
	(40, 'New Wave'),
	(41, 'New Age'),
	(42, 'Negerpunk'),
	(43, 'Native American'),
	(44, 'National Folk'),
	(45, 'Musical'),
	(46, 'Metal'),
	(47, 'Merengue'),
	(48, 'Meditative'),
	(49, 'Lo-Fi'),
	(50, 'Latin'),
	(51, 'Jungle'),
	(52, 'JPop'),
	(53, 'Jazz+Funk'),
	(54, 'Jazz'),
	(55, 'Instrumental Rock'),
	(56, 'Instrumental Pop'),
	(57, 'Instrumental'),
	(58, 'Industrial'),
	(59, 'Indie'),
	(60, 'Humour'),
	(61, 'House'),
	(62, 'Hip-Hop'),
	(63, 'Heavy Metal'),
	(64, 'Hard Rock'),
	(65, 'Hardcore'),
	(66, 'Grunge'),
	(67, 'Gothic Rock'),
	(68, 'Gothic'),
	(69, 'Gospel'),
	(70, 'Goa'),
	(71, 'Gangsta Rap'),
	(72, 'Game'),
	(73, 'Fusion'),
	(74, 'Funk'),
	(75, 'Freestyle'),
	(76, 'Folk/Rock'),
	(77, 'Folklore'),
	(78, 'Folk'),
	(79, 'Fast-Fusion'),
	(80, 'Euro-Techno'),
	(81, 'Euro-House'),
	(82, 'Eurodance'),
	(83, 'Ethnic'),
	(84, 'Electronic'),
	(85, 'Easy Listening'),
	(86, 'Duet'),
	(87, 'Drum Solo'),
	(88, 'Drum & Bass'),
	(89, 'Dream'),
	(90, 'Disco'),
	(91, 'Death Metal'),
	(92, 'Darkwave'),
	(93, 'Dance Hall'),
	(94, 'Dance'),
	(95, 'Cult'),
	(96, 'Crossover'),
	(97, 'Country'),
	(98, 'Contemporary Christian'),
	(99, 'Comedy'),
	(100, 'Club-House'),
	(101, 'Club'),
	(102, 'Classic Rock'),
	(103, 'Classical'),
	(104, 'Christian Rock'),
	(105, 'Christian Rap'),
	(106, 'Christian Gangsta Rap'),
	(107, 'Chorus'),
	(108, 'Chanson'),
	(109, 'Chamber Music'),
	(110, 'Celtic'),
	(111, 'Cabaret'),
	(112, 'BritPop'),
	(113, 'Booty Bass'),
	(114, 'Blues'),
	(115, 'Bluegrass'),
	(116, 'Black Metal'),
	(117, 'Big Band'),
	(118, 'Bebob'),
	(119, 'Beat'),
	(120, 'Bass'),
	(121, 'Ballad'),
	(122, 'Avantgarde'),
	(123, 'Anime'),
	(124, 'Ambient'),
	(125, 'Alt. Rock'),
	(126, 'Alternative'),
	(127, 'Acoustic'),
	(128, 'Acid Punk'),
	(129, 'Acid Jazz'),
	(130, 'Acid'),
	(131, 'A Cappella'),
	(132, 'Space'),
	(133, 'Speech'),
	(134, 'Swing'),
	(135, 'Symphonic Rock'),
	(136, 'Symphony'),
	(137, 'Synthpop'),
	(138, 'Tango'),
	(139, 'Techno'),
	(140, 'Techno-Industrial'),
	(141, 'Terror'),
	(142, 'Thrash Metal'),
	(143, 'Top 40'),
	(144, 'Trailer'),
	(145, 'Trance'),
	(146, 'Tribal'),
	(147, 'Trip-Hop'),
	(148, 'Vocal');

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
