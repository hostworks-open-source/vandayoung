<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('application_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('application_id')->references('id')->on('applications');
            $table->bigInteger('amount');
            $table->char('settlement_date', 8);
            $table->char('surcharge', 30);
            $table->char('restext', 30);
            $table->char('rescode', 30);
            $table->char('expiry', 5);
            $table->char('reference', 30);
            $table->char('summary_code', 30);
            $table->char('txnid', 30);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
