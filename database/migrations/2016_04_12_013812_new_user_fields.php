<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('address', 100);
            $table->char('address2', 100);
            $table->char('city', 70);
            $table->char('state', 30);
            $table->char('postcode', 10);
            $table->unsignedInteger('country_id')->nullable();
            $table->char('phone', 20);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->dateTime('dob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
