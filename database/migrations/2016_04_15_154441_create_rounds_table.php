<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('starts');
            $table->timestamp('ends');
        });
        Schema::create('song_rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('song_id')->nullable();
            $table->foreign('song_id')->references('id')->on('songs');
            $table->unsignedInteger('round_id')->nullable();
            $table->foreign('round_id')->references('id')->on('rounds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
