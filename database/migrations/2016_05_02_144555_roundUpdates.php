<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoundUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('song_rounds', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('rounds', function (Blueprint $table) {
            $table->char("name")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_rounds', function (Blueprint $table) {
            //
        });
    }
}
