<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoundUpdatesWithJudge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('song_rounds', function (Blueprint $table) {
            $table->unsignedInteger('judge_id')->nullable();
            $table->foreign('judge_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_rounds', function (Blueprint $table) {
            //
        });
    }
}
