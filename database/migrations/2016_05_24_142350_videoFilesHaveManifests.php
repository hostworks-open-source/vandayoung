<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VideoFilesHaveManifests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->char('manifest_status', 15)->nullable();
            $table->char('manifest_url')->nullable();
            $table->char('manifest_response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->removeColumn('manifest_status');
            $table->removeColumn('manifest_url');
            $table->removeColumn('manifest_response');
        });
    }
}
