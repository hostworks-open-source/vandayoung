<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoundtrackCategoryTweaks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->text('help')->nullable();
            $table->text('cuesheet_name')->nullable();
            $table->boolean('cuesheet_required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->removeColumn('help');
            $table->removeColumn('cuesheet_name');
            $table->removeColumn('cuesheet_required');
        });
    }
}
