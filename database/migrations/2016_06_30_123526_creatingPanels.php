<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatingPanels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('panel_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('panel_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id', 'panels_user_id')->references('id')->on('users');
            $table->foreign('panel_id', 'panels_panel_id')->references('id')->on('panels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_users', function (Blueprint $table) {
            $table->dropForeign('panels_user_id');
            $table->dropForeign('panels_panel_id');
        });
        Schema::drop('panel_users');
        Schema::drop('panels');
    }
}
