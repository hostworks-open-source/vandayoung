<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SongRoundAverages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_round_averages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('round_id');
            $table->unsignedInteger('song_id');
            $table->float('average', 7, 4);
            $table->timestamps();
            $table->foreign('round_id', 'average_round_id')->references('id')->on('rounds');
            $table->foreign('song_id', 'average_song_id')->references('id')->on('songs');
        });
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('song_id')->nullable();
            $table->unsignedInteger('round_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('score');
            $table->foreign('song_id', 'score_song_id')->references('id')->on('songs');
            $table->foreign('round_id', 'score_round_id')->references('id')->on('rounds');
            $table->foreign('user_id', 'score_user_id')->references('id')->on('users');
        });
        Schema::table('song_rounds', function (Blueprint $table) {
            $table->dropColumn('score');
            $table->unsignedInteger('panel_id')->nullable();
            $table->foreign('panel_id', 'song_rounds_panel_id')->references('id')->on('panels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->dropForeign('average_round_id');
            $table->dropForeign('average_song_id');
        });
        Schema::drop('song_round_averages');
        Schema::table('scores', function (Blueprint $table) {
            $table->dropForeign('score_song_id');
            $table->dropForeign('score_round_id');
            $table->dropForeign('score_user_id');
        });
        Schema::drop('scores');
        Schema::table('song_rounds', function (Blueprint $table) {
            $table->unsignedInteger('score')->nullable();
            $table->dropForeign('song_rounds_panel_id');
            $table->dropColumn('panel_id');
        });
    }
}
