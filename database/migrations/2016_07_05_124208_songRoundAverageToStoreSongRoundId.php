<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SongRoundAverageToStoreSongRoundId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->unsignedInteger('song_round_id')->nullable();
            $table->foreign('song_round_id', 'song_round_average_song_round_id')->references('id')->on('song_rounds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->dropForeign('song_round_average_song_round_id');
            $table->dropColumn('song_round_id');
        });
    }
}
