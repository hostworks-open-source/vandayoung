<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotingCompletedBools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->boolean('has_completed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->dropColumn('has_completed');
        });
    }
}
