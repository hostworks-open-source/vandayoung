<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShortlistBool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->boolean('shortlisted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('song_round_averages', function (Blueprint $table) {
            $table->dropColumn('shortlisted');
        });
    }
}
