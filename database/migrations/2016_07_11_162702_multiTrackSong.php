<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultiTrackSong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->boolean('is_multi_track')->nullable();
            $table->string('multi_track_name')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id', 'mutli_track_parent_id')->references('id')->on('songs');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('is_multi_track')->nullable();
            $table->string('multi_track_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->dropColumn('is_multi_track');
            $table->dropColumn('multi_track_name');
            $table->dropForeign('mutli_track_parent_id');
            $table->dropColumn('parent_id');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('is_multi_track');
            $table->dropColumn('multi_track_name');
        });
    }
}
