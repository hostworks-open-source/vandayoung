<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraSongFileFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->unsignedInteger('main_score_file_id')->after('queue_sheet_file_id')->nullable()->default(null);
            $table->unsignedInteger('lead_sheet_file_id')->after('main_score_file_id')->nullable()->default(null);
            $table->unsignedInteger('main_score_mid_file_id')->after('lead_sheet_file_id')->nullable()->default(null);

            $table->foreign('main_score_file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('lead_sheet_file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('main_score_mid_file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
