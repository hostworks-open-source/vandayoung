<?php

use Illuminate\Database\Seeder;

class SmaCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $entries = [
            [
                'name'              => 'Best Music for a Documentary',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Music for a Short Film',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Original Song Composed for the Screen',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 0,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 0,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Music for a Mini-Series or Telemovie',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-5 for your top 5 favourites (5 for the most favoured). ',
            ],
            [
                'name'              => 'Best Music for a Television Series or Serial',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Television Theme',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 0,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 0,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Music for Children’s Television',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Music for an Advertisement',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 0,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 0,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Feature Film Score of the Year',
                'file_types'        => 'mp4,mov',
                'optgroup'          => 'Video',
                'max_entries'       => 3,
                'has_cue_sheet'     => 1,
                'help'              => 'Max of 3 entries per category.',
                'cuesheet_required' => 1,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-09-30',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
            [
                'name'              => 'Best Soundtrack Album',
                'file_types'        => 'mp3,m4a',
                'optgroup'          => 'Audio',
                'max_entries'       => 50,
                'has_cue_sheet'     => 1,
                'cuesheet_name'     => 'Album Cover',
                'cuesheet_required' => 0,
                'help'              => 'Please start a new application for each sound track album. Submit each song as a separate work, but keep to a maximum of 3 applications. Attach an album cover to indicate the tracklisting for each soundtrack. ',
                'cuesheet_required' => 0,
                'airdate_from'      => '2015-07-01',
                'airdate_to'        => '2016-06-30',
                'is_multi_track'    => true,
                'multi_track_name'  =>'Soundtrack',
                'judging_help'      => 'Acknowledge all works as viewed and give a score of 1-10 for your top 10 favourites (10 for the most favoured). ',
            ],
        ];
        foreach ($entries as $entry) {
            $result = DB::table('categories')->where('name', $entry['name'])->get();
            if (count($result) > 0) {
                DB::table('categories')->where('name', $entry['name'])->update($entry);
                echo 'Updated: '.print_r($entry, true);
            } else {
                DB::table('categories')->insert($entry);
                echo 'Inserted: '.print_r($entry, true);
            }
        }
    }
}
