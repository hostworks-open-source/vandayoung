<?php

use Illuminate\Database\Seeder;

class VandaTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $rounds = [
            [
                'starts'=>'2016-07-11',
                'ends'=>'2016-07-12',
                'name'=>'Round 1'
            ],
            [
                'starts'=>'2016-07-12',
                'ends'=>'2016-07-13',
                'name'=>'Round 2'
            ], [
                'starts'=>'2016-07-13',
                'ends'=>'2016-07-14',
                'name'=>'Round 3'
            ]
        ];
        foreach ($rounds as $round) {
            $result = DB::table('rounds')->where('name', $round['name'])->get();
            if (count($result) > 0) {
                DB::table('rounds')->where('name', $round['name'])->update($round);
                echo 'Updated: '.print_r($round, true);
            } else {
                DB::table('rounds')->insert($round);
                echo 'Inserted: '.print_r($round, true);
            }
        }
    }
}
