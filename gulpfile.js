var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('sma.scss')
       .sass('vanda.scss')
       .scripts([
            'main.js'
       ], 'public/js/main.js')
       .version(['css/sma.css', 'css/vanda.css', 'js/main.js']);
});
