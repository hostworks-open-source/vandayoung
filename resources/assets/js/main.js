$(document).ready(function() {
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
          jqXHR.setRequestHeader('X-CSRF-TOKEN', window.Laravel.csrfToken);
          jqXHR.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    });

    /**
     * Display place holders over the top of file inputs on pages where you are "editing" the content that contains a file. Get file name via AJAX.
     */
    $('.fileButton').each(function(index, obj) {
      loadFileInputAjaxPlaceholder(obj);
    });

    /**
     * On the songs page, add the click handlers for the modal's submit button
     */
    $('.songForm .song-save').each(function(index, element) {
        $(element).click(postSongSubmitForm);
      });

    /**
     * On the songs page, add the click handlers for the modal's delete button
     */
    $('.deleteSongForm .song-delete').each(function(index, element) {
        $(element).click(setupButtonDelete);
      });

    $('#submitAndPay').click(function() {
      window.location = '/application/' + $(this).data('applicationId');
    });

    $('#removeApplication').click(function() {
      window.location = '/submissions/remove/' + $('#applicationId').attr('value');
      $('#removeApplicationModal').modal('hide');
    });

    $('.panelToggle .panel-heading').click(function(e) {
        var myParent = $(this).closest('.panel');
        myParent.find('.panel-body').toggle();
        myParent.find('.panel-footer').toggle();
        myParent.find('.glyphicon').toggleClass('glyphicon-plus');
        myParent.find('.glyphicon').toggleClass('glyphicon-minus');
      });
    $('.panelToggle').find('.panel-body').delay(500).fadeOut(1);
    $('.panelToggle').find('.panel-footer').delay(500).fadeOut(1);

    var checkboxStates = false;
    $('.selectAll').click(function(e) {
        if (checkboxStates) {
          $(this).closest('form').find('tbody').find('input[type="checkbox"]').prop('checked', false);
          checkboxStates = false;
        } else {
          $(this).closest('form').find('tbody').find('input[type="checkbox"]').prop('checked', true);
          checkboxStates = true;
        }
      });

    $('#country').change(function(e) {
        var selected = $(this).find('option:selected');
        checkAndApplyCountryState(selected.text());
      });
    function checkAndApplyCountryState(stateText) {
      if (stateText != '') {
        if (stateText == 'AUSTRALIA') {
          $('#stateDropdown').show();
          $('#stateDropdown').find('select').attr('name', 'state');
          $('#stateText').find('input').attr('name', 'uselessState');
          $('#stateText').hide();
        } else {
          $('#stateText').show();
          $('#stateText').find('input').attr('name', 'state');
          $('#stateDropdown').find('select').attr('name', 'uselessState');
          $('#stateDropdown').hide();
        }
      } else {
        $('#stateText').hide();
        $('#stateDropdown').hide();
        $('#stateText').find('input').attr('name', 'uselessState');
        $('#stateDropdown').find('select').attr('name', 'uselessState');
      }
    }
    if ($('#country').length) {
      checkAndApplyCountryState($('#country').find('option:selected').text());
    }
    $('.clickable-row').click(function() {
        if($(this).data('href')){
            window.document.location = $(this).data('href');
        }
      });

    $('.judgeScoreForm').submit(function(e) {
        var myThis = this;
        var url = '/admin/score'; // the script where you handle the form input.

        $.ajax({
            type: 'POST',
            url: url,
            data: $(myThis).serialize(), // serializes the form's elements.
            beforeSend: function(jqXHR) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-ok success glyphicon-exclamation-sign').addClass('ajaxLoading');//remove all except loading
              },
            error: function(jqXHR, textStatus, errorThrown) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-ok success ajaxLoading').addClass('glyphicon glyphicon-exclamation-sign fail');//glyphicon glyphicon-remove

              },
            success: function(data) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-exclamation-sign fail ajaxLoading').addClass('glyphicon glyphicon-ok success');
              }
          });

        e.preventDefault(); // avoid to execute the actual submit of the form.
      })

    $('.scoreAsRead').submit(function(e) {
        var myThis = this;
        var url = '/admin/markasread'; // the script where you handle the form input.

        $.ajax({
            type: 'POST',
            url: url,
            data: $(myThis).serialize(), // serializes the form's elements.
            beforeSend: function(jqXHR) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-ok success glyphicon-exclamation-sign').addClass('ajaxLoading');//remove all except loading
              },
            error: function(jqXHR, textStatus, errorThrown) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-ok success ajaxLoading').addClass('glyphicon glyphicon-exclamation-sign fail');//glyphicon glyphicon-remove

              },
            success: function(data) {
                $(myThis).find('.loadingContainer').removeClass('glyphicon glyphicon-exclamation-sign fail ajaxLoading').addClass('glyphicon glyphicon-ok success');
              }
          });

        e.preventDefault(); // avoid to execute the actual submit of the form.
      });

    $('#confirmDeleteAllSongRounds').click(function(e){
        launchLoadingScreen();
       $('#confirmDeleteAllSongRoundsForm').submit();
    });

    function launchLoadingScreen(){
        $('#myLoadingModal').modal('show');
    }

    $('#assignSongs').click(function(e){
        launchLoadingScreen();
       $('#assignForm').submit();
    });

    $('#submit-application[disabled]').on('click', function (e) {
      e.preventDefault();
    });
  });

  /**
   * AJAXify the DELETE modal form
   *
   * @param e
   */
  function setupButtonDelete(e) {
    e.preventDefault();
    var targetForm  = $(e.currentTarget).parent().parent('form');
    var songId = targetForm.parent().parent().parent().data('songId');
    $.ajax('/submissions/'+$('#applicationId').val()+'/songs/'+songId, {
      "type": "DELETE"
    }).success(function(result) {
        sayFlashMessage(targetForm.find('.alert'), 'alert-success', 'Your ' + filesName + ' was removed!');
        targetForm.find('.alert').addClass('noBottomMargin');
        $(targetForm).find('.has-error').removeClass('has-error');
        $(targetForm).find('.song-delete').prop('disabled', true);
        $(targetForm).find('.song-dismiss').prop('disabled', true);
        $(targetForm).find('.modal-body .question').text('');
        var targetModal = $(targetForm).closest('.myDeleteModal');
        var targetWholePreviewElement = targetModal.closest('.dz-preview');
        targetModal.on('hidden.bs.modal', function(event) {
            $(targetWholePreviewElement).remove();
            checkReadyToPay();
            decreaseSongsCount();
        });
      }).fail(function(result) {
        sayFlashMessage(targetForm.find('.alert'), 'alert-danger', 'There was an error. ');
      });
  }

  $('body').on('submit', '.songForm', function (e) {
    e.preventDefault();

    var form = $(this);

    form.find('.loadingContainer')
      .removeClass('glyphicon glyphicon-ok success glyphicon-exclamation-sign')
      .addClass('ajaxLoading')
      .show(); //remove all except loading

    form.find('.alert').text('').hide();
    form.find('.song-success').text('').hide();
    form.find('.song-success-check').hide();

    form.find('.has-error').each(function () {
      $(this).removeClass('has-error');
      $(this).find('.help-block').text('');
    });

    var formData = new FormData(e.target);

    $.ajax({
      method: "POST",
      url: form.attr('action'),
      data: formData,
      processData: false,
      contentType: false
    }).success(function (response) {
        form.find('.song-success').text(response.message).show();
        form.find('.song-success-check').show();
        form.find('input[name="hasSaved"]').attr('value', 1);
        form.closest('.modal').parent().find('.myEdit').removeClass('btn-danger').addClass('btn-default');
    }).fail(function (response) {
      $.each(response.responseJSON.errors, function (key, value) {
        var group = form.find('#'+key).closest('.form-group');
        group.addClass('has-error');
        group.find('.help-block').last().text(value.shift());
        form.closest('.modal').parent().find('.myEdit').removeClass('btn-default').addClass('btn-danger');
      });
    }).complete(function () {
      form.find('.loadingContainer').hide();
        checkReadyToPay();
    });
  });

$('.myModal').on('show.bs.modal', function (e) {
    var successMessage = $(this).find('.song-success'),
        successCheck = $(this).find('.song-success-check');

    if (successMessage) {
        successMessage.text('').hide();
    }

    if (successCheck) {
        successCheck.hide();
    }
});


  /**
   * Song counts for validtion
   */
  if (typeof songCount != 'undefined') {
    $('#songCountNo').text(songCount);
    var costPerSong = 50;
    $('#songCountPrice').text(songCount * costPerSong);
  }

  /**
   * Check if there is at least song uploaded. Enable submit button if all songs are valid ones.
   *
   * @returns {boolean}
   */
  function checkReadyToPay() {
    validCount = $('input[name=\'hasSaved\']').filter('input[value=\'1\']').length;
    inValidCount = $('input[name=\'hasSaved\']').filter('input[value=\'0\']').length;
    if (parseInt(validCount) > 0 && parseInt(inValidCount) == 1) { // inValidCount checks for 1 (instead of 0), because the preview/template element is embeded for Dropzone.
      $('#submitAndPay').prop('disabled', false);
      return true;
    }
    $('#submitAndPay').prop('disabled', true);
    return false;
  }

  function listenForCategoryChanges() {
    $('*[name=\'category\']').unbind('change');
    $('*[name=\'category\']').change(function(e) {
        var target = $(this);
        showHideCueSheet(target);
      });
    $('*[name=\'category\']').each(function(index, obj) {
        showHideCueSheet($(obj));
      });
  }

  function loadAllGenreSelects() {
    $('.genreSelect').unbind('change');
    $('.genreSelect').change(function(e) {
        var selected = $(this).find('option:selected');
        var otherInputGroup = selected.closest('.form-group').siblings('.otherInput');
        if (selected.text() == 'Other') {
          otherInputGroup.show();
        } else {
          otherInputGroup.hide();
          otherInputGroup.find('input').attr('value', '');
        }
      });
  }
  loadAllGenreSelects();

  function showHideCueSheet(target) {
    var form = target.closest('form');
    var selected = target.find('option:selected');
    var hasCueSheet = selected.data('has-cuesheet');
    var help = selected.data('help');
    form.find('.categoryBroadContainer').find('.help-block').first().text(help);
    if (hasCueSheet == '1') {
      form.find('.cueSheetContainer').show();
    } else {
      form.find('*[name=\'queue_sheet\']').val('');
      form.find('.cueSheetContainer').hide();
    }
    var cueSheetName = selected.data('cuesheet-name');
    if (cueSheetName && cueSheetName.length > 0) {
      form.find('.cueSheetName').text(cueSheetName);
    } else {
      form.find('.cueSheetName').text('Cue Sheet');
    }
    var cueSheetRequired = selected.data('cuesheet-required');
    if (cueSheetRequired == '1') {
      form.find('.cueSheetContainer').find('.requiredBadge').show();
    } else {
      form.find('.cueSheetContainer').find('.requiredBadge').hide();
    }

    form.find('#datetimepicker').datepicker('destroy');
    form.find('#datetimepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: selected.data('airdate-from'),
        endDate: selected.data('airdate-to')
      });

  }
  listenForCategoryChanges();

  function loadFileInputAjaxPlaceholder(obj) {
    var filePlaceholder = $(obj);
    var fileInput = $(obj).parent().find('.fileInputShow');
    var fileId = filePlaceholder.attr('data-content');

    filePlaceholder.show();
    fileInput.hide();

    $.getJSON('/api/file/' + fileId, function(data) {
      filePlaceholder.children('.filename').text(data.name);
    });
    filePlaceholder.click(function(e) {
      fileInput.show();
      filePlaceholder.parent().find('.fileIdValue').val('');
      filePlaceholder.hide();
    });
  }

  function sayFlash(type, message) {
    var types = ['alert-danger', 'alert-warning', 'alert-success', 'alert-info'];
    for (i = 0; i < types.length; i++) {
      $('.globalFlash').removeClass(types[i]);
    }
    $('.globalFlash').text(message);
    $('.globalFlash').addClass(type);
    $('.globalFlash').show();
    $('html, body').animate({
      scrollTop: $('.globalFlash').offset().top
    }, 300);
  }

  /**
   * Take a bootstrap flash <p> and manipulate the classes for turning it into success or danger
   *
   * @param object
   * @param cssClass
   * @param message
   */
  function sayFlashMessage(object, cssClass, message) {
    object.text(message);
    object.removeClass('alert-success');
    object.removeClass('alert-danger');
    object.addClass(cssClass);
    object.show();
  }

  function incrementSongsCount() {
    songCount = songCount + 1;
    $('#songCountNo').text(songCount);
    $('#songCountPrice').text(songCount * costPerSong);
  }

  function decreaseSongsCount() {
    songCount = songCount - 1;
    $('#songCountNo').text(songCount);
    $('#songCountPrice').text(songCount * costPerSong);
  }

  function calculateWordsRemaining(target) {
      var matches = target.val().match(/\S+/g);

      if (! matches) {
          target.parent().find('.words-remaining').text(300);
          return;
      }

      var words = matches.length;
      var remaining = 300 - words;

      if (words > 300) {
          var trimmed = target.val().split(/\s+/, 300).join(' ');
          target.val(trimmed);
      }

      target.parent().find('.words-remaining').text(Math.max(remaining, 0));
  }

  checkReadyToPay();
