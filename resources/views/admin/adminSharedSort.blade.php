<div class="list-group">
    <a href="" onclick="return false" class="list-group-item disabled">
        Order By
    </a>

    <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_ASC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=a-z"])) active @endif">A-Z</a>
    <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_DESC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=z-a"])) active @endif">Z-A</a>
    <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_DATE_DESC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=date-desc"])) active @endif">Newest</a>
    <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_DATE_ASC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=date-asc"])) active @endif">Oldest</a>
    @if(isset($hasScore) and $hasScore)

        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_SCORE_DESC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=score-desc"])) active @endif">Highest Score</a>
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['sort'=>\App\Http\Controllers\AdminController::SORT_SCORE_ASC], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["sort=score-asc"])) active @endif">Lowest Score</a>
    @endif
</div>