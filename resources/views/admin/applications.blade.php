@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-3">
        @include('admin/applicationsFilters')
    </div>
    <div class="col-sm-9">
        @include('admin/sharedSearch')
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Applications</h3>
            </div>
            <div class="panel-body">
                @include('admin/applicationsTable')
            </div>
        </div>
        <div>
            {!! $applications->appends($filterArray)->render() !!}
        </div>
        <div class="paginationCount" style="text-align: center">{{ $applications->count() }} of {{ $applications->total() }} results.</div>
    </div>
@endsection