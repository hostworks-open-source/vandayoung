
<div class="list-group">
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['page'=>1, 'filter'=>''], $filterArray) !!}" class="list-group-item @if(empty($filterArray['filter'])) active @endif">
        All
    </a>
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['filter'=>App\Http\Controllers\AdminController::FILTER_ARCHIVED], $filterArray) !!}" class="list-group-item @if(str_contains(Request::fullUrl(), ["filter=archived"])) active @endif">
        Deleted
    </a>
</div>
<div class="list-group">
    <a href="" onclick="return false" class="list-group-item disabled">
        Status
    </a>

    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_UNPAID))
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_STATUS, 'state'=>\App\Application::STATE_UNPAID], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=status", "state=unpaid"])) active @endif">Started</a>
    @endif
    
    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_DEFERRED))
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_STATUS, 'state'=>\App\Application::STATE_DEFERRED], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=status", "state=deferred"])) active @endif">Deferred</a>
    @endif

    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_SUBMITTED))
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_STATUS, 'state'=>\App\Application::STATE_SUBMITTED], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=status", "state=submitted"])) active @endif">Submitted</a>
    @endif

    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_SONGS_PAID))
    <a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_STATUS, 'state'=>\App\Application::STATE_SONGS_PAID], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=status", "state=paid"])) active @endif">Paid</a>
    @endif

</div>
@include('admin/adminSharedSort')
<a href="/admin/applications?{!! \App\Helpers\General::filterQueryString(['action'=>'export'], $filterArray) !!}" class="btn btn-primary">Download as XLSX</a>
