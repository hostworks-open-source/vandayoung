

<!-- Table -->
<table class="table table-hover">
    <thead>
    <tr>
        <th>Application</th>
        <th style="text-align: center">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</th>
        @if(\Config('app.canMakePayments'))
        <th style="text-align: center">Paid</th>
        @endif
        <th style="text-align: center">Status</th>
    </tr>
    </thead>
    <tbody>
    @forelse($applications as $app)
        <tr class="clickable-row" data-href="/application/{{$app->id}}">

            <td style="text-align: left">
                <p>@if(!empty($app->performing_name)) <a href="/application/{{$app->id}}">{{ str_limit($app->performing_name, $limit = 40, $end = '...') }}</a>@else <a href="/application/{{$app->id}}">{{\Config('app.applicationPrefix')}}-{{ $app->id }}</a> @endif</p>
                <p class="smallDescription">
                    {{ date_create($app->created_at)->format('d/m/Y') }}, {{\Config('app.applicationPrefix')}}-{{ $app->id }},
                    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]) and $app and $app->user)Impersonate: <a href="/admin/impersonate/account/{{ $app->user->id }}">{{ $app->user->firstname }} {{ $app->user->surname }}</a>@endif,
                    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))Action @if(!$app->trashed())<a href="/admin/submissions/remove/{{ $app->id }}">Delete</a> @else <a href="/admin/submissions/unremove/{{ $app->id }}">Restore</a> @endif @endif
                    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                        Change To:
                        @if($app and !\Config('app.canMakePayments') and $app->status != \App\Application::STATE_SUBMITTED)
                            <a href="{{route('changeAppStatus', ['id'=>$app->id, 'status'=>\App\Application::STATE_SUBMITTED]) }}">Submitted</a>
                        @endif
                        @if($app and \Config('app.canMakePayments') and $app->status != \App\Application::STATE_SONGS_PAID)
                            <a href="{{route('changeAppStatus', ['id'=>$app->id, 'status'=>\App\Application::STATE_SONGS_PAID]) }}">Paid</a>
                        @endif
                        @if($app and \Config('app.canMakePayments') and $app->status != \App\Application::STATE_DEFERRED)
                            <a href="{{route('changeAppStatus', ['id'=>$app->id, 'status'=>\App\Application::STATE_DEFERRED]) }}">Deferred</a>
                        @endif
                        @if($app and $app->status != \App\Application::STATE_UNPAID)
                            <a href="{{route('changeAppStatus', ['id'=>$app->id, 'status'=>\App\Application::STATE_UNPAID]) }}">Started</a>
                        @endif
                    @endif

                </p>
            </td>
            <td style="text-align: center">
                @if(isset($filterArray['filter']) and $filterArray['filter'] == \App\Http\Controllers\AdminController::FILTER_ARCHIVED )
                    {{ \App\Helpers\General::filteredSongsCount($app->songs()->onlyTrashed()->get()) }}
                @else
                    {{ \App\Helpers\General::filteredSongsCount($app->songs) }}
                @endif
            </td>
            @if(\Config('app.canMakePayments'))
                <td style="text-align:center;">
                    @if($app->totalPaid > 0)
                        ${{ $app->totalPaid }}
                    @else
                        -
                    @endif
                </td>
            @endif
            <td style="text-align: center">{{ $app->formattedStatus }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="5">
                <div class="emptyTable">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <p>no applications</p>
                </div>
            </td>
        </tr>
    @endforelse
    </tbody>
</table>
