@if(isset($justAssigned))
<table class="table table-hover">
    <thead>
    <tr>
        <th width="50%" style="text-align: right;font-weight:bold;">Judge</th>
        <th width="" style="text-align: left;font-weight:bold;">Assigned</th>
    </tr>
    </thead>
    <tbody>
    @foreach($justAssigned['counts'] as $key => $assignmentCount)
            <tr>
                <td style="text-align: right">{{ $justAssigned['panel']->users->get($key)->fullName or '' }}</td>
                <td style="text-align: left">{{$assignmentCount}}</td>
            </tr>
    @endforeach
    </tbody>
</table>
@endif