@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-12">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Dashboard</h3>
            </div>
            <div class="panel-body" id="updateDetails">
                <table class="table appTable table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Today</th>
                        <th>This Week</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th><a href="/admin/applications">Applications</a></th>
                        <td>{{ $stats['applications']['today'] }}</td>
                        <td>{{ $stats['applications']['week'] }}</td>
                        <td>{{ $stats['applications']['total'] }}</td>
                    </tr>
                    <tr>
                        <th><a href="/admin/users">Users</a></th>
                        <td>{{ $stats['users']['today'] }}</td>
                        <td>{{ $stats['users']['week'] }}</td>
                        <td>{{ $stats['users']['total'] }}</td>
                    </tr>
                    <tr>
                        <th><a href="/admin/songs">All {{ ucfirst(\Config('app.filesNamePlural')) }}</a><br><sup>(by upload time)</sup> </th>
                        <td>{{ $stats['works']['today'] }}</td>
                        <td>{{ $stats['works']['week'] }}</td>
                        <td>{{ $stats['works']['total'] }}</td>
                    </tr>
                    </tr>
                    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_SUBMITTED))
                    <tr>
                        <th><a href="/admin/songs">Submitted {{ ucfirst(\Config('app.filesNamePlural')) }}</a></th>
                        <td>{{ $stats['worksSubmitted']['today'] }}</td>
                        <td>{{ $stats['worksSubmitted']['week'] }}</td>
                        <td>{{ $stats['worksSubmitted']['total'] }}</td>
                    </tr>
                    @endif

                    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_SONGS_PAID))
                    <tr>
                        <th><a href="/admin/songs">Paid {{ ucfirst(\Config('app.filesNamePlural')) }}</a></th>
                        <td>{{ $stats['worksPaid']['today'] }}</td>
                        <td>{{ $stats['worksPaid']['week'] }}</td>
                        <td>{{ $stats['worksPaid']['total'] }}</td>
                    </tr>
                    @endif

                    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_UNPAID))
                    <tr>
                        <th><a href="/admin/songs">Started {{ ucfirst(\Config('app.filesNamePlural')) }}</a></th>
                        <td>{{ $stats['worksStarted']['today'] }}</td>
                        <td>{{ $stats['worksStarted']['week'] }}</td>
                        <td>{{ $stats['worksStarted']['total'] }}</td>
                    </tr>
                    @endif

                    @if(\App\Helpers\General::stateIsAvailable(\App\Application::STATE_DEFERRED))
                    <tr>
                        <th><a href="/admin/songs">Deferred {{ ucfirst(\Config('app.filesNamePlural')) }}</a></th>
                        <td>{{ $stats['worksDeferred']['today'] }}</td>
                        <td>{{ $stats['worksDeferred']['week'] }}</td>
                        <td>{{ $stats['worksDeferred']['total'] }}</td>
                    </tr>
                    @endif

                    </tbody>
                </table>


            </div>
        </div>
    </div>
@endsection