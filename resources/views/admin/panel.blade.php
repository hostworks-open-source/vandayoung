@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-4 col-sm-offset-4 small-panel">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">@if(isset($panel))Edit Panel @else New Panel @endif</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="">
                    {!! csrf_field() !!}
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        <label class="sr-only" for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="@if(empty(old('name')) and isset($panel)){{$panel->name}}@else{{ old('name') }}@endif">
                        @foreach($errors->get('name') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if ($errors->has('judges')) has-error @endif">
                        <label class="sr-only" for="judges">Judges</label>
                        <select multiple style="width: 100%; height: 400px;" name="judges[]">
                            <optgroup label="Judges">
                                @foreach($judgeUsers as $judgeUser)
                                <option @if(is_array(old('judges')) and in_array($judgeUser->id, old('judges'))) selected="selected"  @elseif(isset($panel) and $panel->users->where('id', $judgeUser->id)->count() > 0)) selected="selected" @endif value="{{$judgeUser->id}}">{{$judgeUser->fullname}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Admins">
                                @foreach($adminUsers as $adminUser)
                                <option @if(is_array(old('judges'))) @if(in_array($adminUser->id, old('judges'))) selected="selected" @endif @elseif(isset($panel) and $panel->users->where('id', $adminUser->id)->count() > 0)) selected="selected" @endif  value="{{$adminUser->id}}">{{$adminUser->fullname}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        @foreach($errors->get('judges') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>

                    <hr>
                    @if(isset($panel)) <input type="hidden" name="id" value="{{$panel->id}}" />@endif
                    <button type="submit" class="btn btn-lg btn-block btn-primary">@if(isset($panel))Update Panel @else Create Panel @endif</button>
                </form>
            </div>
        </div>
    </div>
@endsection