@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-12">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Panels</h3>
            </div>
            <div class="panel-body" id="updateDetails">
                <table class="table appTable table-hover">
                    <thead>
                    <tr>
                        <th style="text-align: left">Name</th>
                        <th>Members</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($panels as $panel)
                        <tr class="clickable-row" data-href="/admin/panel/{{$panel->id}}">
                            <td style="text-align: left">
                                <p>
                                    {{$panel->name}}
                                </p>
                            </td>
                            <td>{{ $panel->users->count() }}</td>
                            <td>{{ date_create($panel->created_at)->format('d/m/Y') }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class="emptyTable">
                                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                    <p>no panels</p>
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>


            </div>
            <div class="panel-footer">
                <a class="btn btn-lg btn-primary pull-right" href="/admin/panel">New Panel</a>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
@endsection