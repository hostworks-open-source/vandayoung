@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-3">
        @include('admin/scoresFilter')
    </div>
    <div class="col-md-9">
            <div class="panel panel-default panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">{{ $judge->fullName }}</div>
                @include('admin/scoresTable')
            </div>
    </div>
@endsection
