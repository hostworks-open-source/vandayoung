<div class="list-group">
    @foreach($rounds as $round)
        <a href="{{ route('adminViewScores', ['id'=>$judge->id, 'roundId'=>$round->id]) }}" class="list-group-item @if($currentRound->id == $round->id) active @endif">
            {{ $round->name }}
        </a>
    @endforeach
</div>

<div>
   <form action="{{route('deleteAllSongRoundsForJudgeByRound')}}" method="post" id="confirmDeleteAllSongRoundsForm">
        <input type="hidden" name="judge" value="{{ $judge->id }}" />
        <input type="hidden" name="round" value="{{ $currentRound->id }}" />
       <a class="btn btn-default" data-toggle="modal" data-target="#myModal">Remove Round Assignments</a>
   </form>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                   Are you sure you want to remove all assignments for this round and judge?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No, Keep. </button>
                    <button type="button" class="btn btn-primary" id="confirmDeleteAllSongRounds">Yes, Delete. </button>
                </div>
            </div>
        </div>
    </div>
</div>