<table class="table table-hover">
    <thead>
    <tr>
        <th>Track</th>
        <th style="">Application</th>
        <th style="text-align: center">Score</th>
        <th style="text-align: right">Action</th>
    </tr>
    </thead>
    <tbody>
    @forelse($scores as $score)
        <?php $song = $score->song->children()->count() > 0 ? $score->song->children()->orderBy('title')->first() : $score->song; ?>
        <tr>
            <td style="width: 35%">@if($song->parent()->count() > 0)<span class="label label-info">{{$song->category->multi_track_name or ''}}</span> @endif <a href="/admin/preview/song/{{$score->song->id}}">{{$song->title or ''}}</a> @if(isset($song->category->name))<br> <p class="smallDescription">{{ $song->category->name or '' }}</p> @endif</td>
            <td><p class="smallDescription"><a href="/application/{{ $song->application->id or ''}}">{{ Config('app.applicationPrefix')}}-{{ $song->application->id or ''}}</a> - {{ $song->application->user->fullName or '' }} </p></td>
            <td style="text-align: center">{{$score->score }}</td>
            <td style="text-align: right;width: 30%">
                    <a class="btn btn-default" href="{{ route('shortListSongToRound', ['round_id'=>$currentRound->id, 'song_id'=>$score->song->id, 'action'=>$score->song->isShortListed($currentRound->id) ? 'unshortlist': 'shortlist']) }}">
                        {{ $song->isShortListed($currentRound->id) ? 'Unshortlist': 'Shortlist' }}
                    </a>
                <form action="/admin/remove/scores/from/round" method="post" style="display: inline;">
                    <input type="hidden" name="score_id" value="{{ $score->id }}" />
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="4">
                <div class="emptyTable">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <p>no scores</p>
                </div>
            </td>
        </tr>

    @endforelse
    </tbody>
</table>