<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 15px;">
        <div class="">
            <form class="form-inline" method="get" class="">

                <div class="form-group">
                    <label for="nameSearch" class="sr-only">Search</label>
                    <input type="text" name="search_term" class="form-control" id="nameSearch" placeholder="Search" value="{{$filterArray['search_term'] or ''}}">
                </div>

                <div class="form-group">
                    <select class="form-control" name="search_type">
                        @if(in_array(\App\Http\Controllers\AdminController::SEARCH_TYPE_TITLE, $availableSearchTypes))
                            <option value="{{\App\Http\Controllers\AdminController::SEARCH_TYPE_TITLE}}"  @if(isset($filterArray['search_type']) and $filterArray['search_type'] == \App\Http\Controllers\AdminController::SEARCH_TYPE_TITLE) selected="selected" @endif>Title</option>
                        @endif
                        @if(in_array(\App\Http\Controllers\AdminController::SEARCH_TYPE_NAME, $availableSearchTypes))
                            <option value="{{\App\Http\Controllers\AdminController::SEARCH_TYPE_NAME}}"  @if(isset($filterArray['search_type']) and $filterArray['search_type'] == \App\Http\Controllers\AdminController::SEARCH_TYPE_NAME) selected="selected" @endif>Name</option>
                        @endif
                        @if(in_array(\App\Http\Controllers\AdminController::SEARCH_TYPE_EMAIL, $availableSearchTypes))
                            <option value="{{\App\Http\Controllers\AdminController::SEARCH_TYPE_EMAIL}}"  @if(isset($filterArray['search_type']) and $filterArray['search_type'] == \App\Http\Controllers\AdminController::SEARCH_TYPE_EMAIL) selected="selected" @endif>Email</option>
                        @endif
                        @if(in_array(\App\Http\Controllers\AdminController::SEARCH_TYPE_APPLICATION_ID, $availableSearchTypes))
                            <option value="{{\App\Http\Controllers\AdminController::SEARCH_TYPE_APPLICATION_ID}}"  @if(isset($filterArray['search_type']) and $filterArray['search_type'] == \App\Http\Controllers\AdminController::SEARCH_TYPE_APPLICATION_ID) selected="selected" @endif>Application ID</option>
                        @endif
                    </select>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    <span class="mls"><a href="{{ route($currentRoute) }}?{!! \App\Helpers\General::filterQueryString(['search_type'=>null, 'search_term'=>null], $filterArray) !!}">Clear search</a>
                </div>
                @foreach($inputValues as $key => $value)
                    @if($key != 'page')
                        <input type="hidden" name="{{$key}}" value="{{$value}}" />
                    @endif
                @endforeach
            </form>
        </div>
    </div>
</div>
