@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')

@include('submission/previewSong', ['song'=>$song, 'judgingRounds'=>$judgingRounds, 'scoreRange'=>$scoreRange])

    @if($song->is_multi_track)
        @foreach($song->children()->orderBy('title')->get() as $cSong)
            @include('submission/previewSong', ['song'=>$cSong, 'judgingRounds'=>$judgingRounds, 'scoreRange'=>$scoreRange])
        @endforeach
    @endif
@endsection