@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-3">
        @include('admin/songsFilter')
    </div>

    <div class="col-md-9">
        @include('admin/sharedSearch')
        @include('admin/assignedTable')
    </div>

    <div class="col-md-9">
        <form class="form-inline" action="/admin/songs/to/round" id="assignForm" method="post">
            {!! csrf_field() !!}
        <div class="panel panel-default panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</div>

            @include('admin/songsTable')
        </div>

            @include('admin/songsPagination')
        </form>
    </div>
@endsection
