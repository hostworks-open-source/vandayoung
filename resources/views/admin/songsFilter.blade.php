<div class="list-group">
    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
        <a href="/admin/songs" class="list-group-item @if(empty($filterArray['filter']) and empty($filterArray['extra'])) active @endif">
            All
        </a>
        @else
        <a href="" onclick="return false" class="list-group-item disabled">
            Your Judging Area
        </a>
    @endif

    @if(\Config('app.canMakePayments') and in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
    <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['extra'=>\App\Http\Controllers\AdminController::FILTER_PAID_APPLICATION], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["extra=paid"] ))) active @endif">Paid</a>
    @endif
</div>
@include('admin/adminSharedSort')
@if($categories->count() > 0 and in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
    <div class="list-group">
        <a href="" onclick="return false" class="list-group-item disabled">
            Category
        </a>
        @foreach($categories as $category)
            <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['z'=>'z','category_id'=>$category->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["category_id={$category->id}&"] ))) active @endif">{{ $category->name }}</a>
        @endforeach
    </div>
@endif
@foreach($rounds as $round)
    <div class="list-group">
        <a href="" onclick="return false" class="list-group-item disabled">
            {{ $round->name }}
            <br>
            <span class="smallDescription">{{date_create($round->starts)->format('d/m/Y')}} - {{date_create($round->ends)->sub(new DateInterval('P1D'))->format('d/m/Y')}}</span>
        </a>
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>App\Http\Controllers\AdminController::FILTER_PENDING_ASSIGNED_TO_ME,'round'=>$round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ['filter=pending-assigned-to-me', "round={$round->id}"])) active @endif">Mine To Judge</a>
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>App\Http\Controllers\AdminController::FILTER_COMPLETED_ASSIGNED_TO_ME,'round'=>$round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ['filter=completed-assigned-to-me', "round={$round->id}"])) active @endif">My Completed</a>
        @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>App\Http\Controllers\AdminController::FILTER_ROUND_PENDING,'round'=>$round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ['filter=round-pending', "round={$round->id}"])) active @endif">Awaiting All Judgement</a>
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>App\Http\Controllers\AdminController::FILTER_ROUND_COMPLETED, 'round'=> $round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=round-completed", "round={$round->id}"])) active @endif">Completed</a>
        <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_NOT_IN_ROUND, 'round'=>$round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=not-in-round", "round={$round->id}"])) active @endif">Not In Round</a>
            <a href="{{route($currentRoute)}}?{!! \App\Helpers\General::filterQueryString(['filter'=>\App\Http\Controllers\AdminController::FILTER_SHORTLISTED, 'round'=>$round->id], $filterArray) !!}" class="list-group-item @if(all_in_string(Request::fullUrl(), ["filter=shortlisted", "round={$round->id}"])) active @endif">Shortlisted</a>
        @endif
    </div>
@endforeach
@if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
    <div style="text-align: left" class="mbm">
        <a href="{{ route('adminExportScores') }}" class="btn btn-primary">Download Scores as XLSX</a>
    </div>
@endif

