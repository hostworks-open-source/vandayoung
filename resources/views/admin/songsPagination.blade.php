<div class="advancedPagination">


    {!! $songs->appends(
        array_merge(
            $filterArray,
            [
                'round' => request('round')
            ]
        )
    )->render() !!}

</div>
<div class="paginationCount" style="text-align: center;clear:both;">
    {{ $songs->total() }} results.
</div>