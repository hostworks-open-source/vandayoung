<!-- Table -->
<table class="table table-hover">
    <thead>
    <tr>
        <th width="6%" style="text-align: right"><input type="checkbox" name="selectAll" class="selectAll" /></th>
        <th width="48%">{{ ucfirst(\Config('app.filesName')) }}</th>
        @if(isset($hasScore) and $hasScore)<th style="text-align: center">Score</th>@endif
        <th width="6%" style="text-align: right">Date</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($songs as $index => $song)

        <tr @if(isset($hasScore) and $hasScore and isset($songScores[$song->id])) class="info" @endif>
            <td width="6%" style="text-align: right">
                <input type="checkbox" name="songs[]" value="{{$song->id}}" />

            </td>
            <td width="48%" style="text-align: left">
                <p>
                    <a href="{{ route('previewSong', ['search'=>$savedSearch->id, 'id'=>$song->id]) }}" class="">
                        @if($song->is_multi_track)
                            <span class="label label-info">
                                {{$song->multi_track_name}}
                            </span>

                            {{$song->children->sortBy('title')->first()->title or ''}}
                        @else
                            {{ $song->title }}
                        @endif
                    </a>
                    <br>
                    @if(!in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                        <p class="smallDescription">
                            @if($song->category)
                                Category: {{$song->category->name or '' }}
                            @endif
                        </p>
                    @endif
                </p>
                @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                    <p class="smallDescription">
                        {{ $song->created_at->format('d/m/Y') }}
                        @if($song->application)<a href="/application/{{ $song->application->id }}">{{\Config('app.applicationPrefix')}}-{{ $song->application->id }}</a>
                        &nbsp;
                        Impersonate:
                        @if($song->application and $song->application->user)<a href="/admin/impersonate/account/{{ $song->application->user->id }}">{{ $song->application->user->firstname }} {{ $song->application->user->surname }}</a>@endif

                        @if(!\Config('app.skipApplicationPage') and !empty($song->application->performing_name))&nbsp; Artist: {{ $song->application->performing_name }}@endif

                        @if($song->application)&nbsp; Status: {{$song->application->formattedStatus}} &nbsp; @endif

                        @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]) and isset($roundId) and is_numeric($roundId))
                            Action: <a href="{{ route('shortListSongToRound', ['round_id'=>$roundId, 'song_id'=>$song->id, 'action'=>$song->isShortListed($roundId) ? 'unshortlist': 'shortlist']) }}">
                                {{ $song->isShortListed($roundId) ? 'Unshortlist': 'Shortlist' }}
                            </a>
                        @endif

                        @if($song->category)
                            {{$song->category->name or '' }}
                        @endif

                        @endif
                    </p>
                @endif
            </td>
            @if(isset($hasScore) and $hasScore)
            <td style="text-align: center;width: 5%;">
                @if(isset($songScores[$song->id]))
                    @if($songScores[$song->id] == 0)
                        <span class="glyphicon glyphicon-eye-open" style="color:#337ab7"></span>
                        @else
                        <span class="badge" style="background-color: #337ab7">
                        {{ $songScores[$song->id] }}
                        </span>
                    @endif
                @endif
            </td>
            @endif
            <td style="text-align: right" width="6%">{{ $song->created_at->format('d/m/Y') }}</td>
        </tr>
        @empty
        <tr>
            <td colspan="4">
                <div class="emptyTable">
                    <span class="glyphicon glyphicon-music" aria-hidden="true"></span>
                    <p>no {{ Config('app.filesNamePlural') }}</p>
                </div>
            </td>
        </tr>
    @endforelse
    </tbody>
    <tfoot>
    <tr><td colspan="4" style="text-align: center;padding-bottom:30px">
            @if($rounds and $rounds->count() > 0 and in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                <div style="clear:both;text-align: center;display: block;margin-bottom:20px;" class="mbm checkbox randomAssign">
                    <label><input type="checkbox" name="is_random" value="1" /> Randomly assign the entire result set seperately between each member of a panel. </label>
                    <input type="hidden" value="{{$savedSearch->id}}" name="search" />
                </div>
                <div class="form-group roundAssignment" style="">
                    <select id="judge" name="judge" class="form-control">
                        <optgroup label="Panels">

                            @foreach($panels as $panel)
                                <option value="{{ $panel->uuid }}"@if ($panel->uuid == old('judge')) selected="selected"@endif>{{ $panel->name }}</option>
                            @endforeach

                        </optgroup>
                        <optgroup label="Judges">
                            @foreach($judges as $judge)
                                <option value="{{ $judge->uuid }}"@if ($judge->uuid == old('judge')) selected="selected"@endif>{{ $judge->firstname }} {{ $judge->surname }}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Admins">
                            @foreach($admins as $admin)
                                <option value="{{ $admin->uuid }}"@if ($admin->uuid == old('judge')) selected="selected"@endif>{{ $admin->firstname }} {{ $admin->surname }}</option>
                            @endforeach
                        </optgroup>

                    </select>
                    <select id="round" name="round" class="form-control">
                        @foreach($rounds as $round)
                            <option value="{{ $round->id }}"@if ($round->id == old('judge')) selected="selected"@endif>{{ $round->name }}</option>
                        @endforeach
                    </select>
                    <a id="assignSongs" class="btn btn-primary">
                        Assign
                    </a>
                </div>
            @endif
        </td>
    </tr>
    </tfoot>
</table>