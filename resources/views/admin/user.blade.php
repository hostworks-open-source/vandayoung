@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-4 col-sm-offset-4 small-panel">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">@if(isset($editUser))Edit User @else New User @endif</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="">
                    {!! csrf_field() !!}
                    <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                        <label class="sr-only" for="firstName">Name</label>
                        <input type="text" class="form-control" id="firstName" placeholder="Name" name="firstname" value="@if(empty(old('firstname')) and isset($editUser)){{$editUser->firstname}}@else{{ old('firstname') }}@endif">
                        @foreach($errors->get('firstname') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if ($errors->has('surname')) has-error @endif">
                        <label class="sr-only" for="lastName">Surname</label>
                        <input type="text" class="form-control" id="lastName" placeholder="Surname" name="surname" value="@if(empty(old('surname')) and isset($editUser)){{$editUser->surname}}@else{{ old('surname') }}@endif">
                        @foreach($errors->get('surname') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                            <label class="sr-only" for="email">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="@if(empty(old('email')) and isset($editUser)){{$editUser->email}}@else{{ old('email') }}@endif">
                        </div>
                        @foreach($errors->get('email') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                            <label class="sr-only" for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                        @forelse($errors->get('password') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @empty
                            <span class="help-block mtm" style="text-align: center">The password must be at least 8 characters.</span>
                        @endforelse
                    </div>
                    <hr style="margin-top:0">
                        <div class="form-group @if ($errors->has('role')) has-error @endif" style="text-align: center">
                            <label class="col-md-2 control-label" for="role">Role</label>
                            <div class="">
                                <label class="radio-inline mutliLineRadio">
                                    <input type="radio" name="role" @if(old('role') == \App\User::USER_ROLE_USER) checked @elseif(isset($editUser->role) and $editUser->role == \App\User::USER_ROLE_USER) checked @endif value="{{ \App\User::USER_ROLE_USER }}"> Regular User
                                </label>
                                <label class="radio-inline mutliLineRadio">
                                    <input type="radio" name="role" @if(old('role') == \App\User::USER_ROLE_JUDGE) checked @elseif(isset($editUser->role) and $editUser->role == \App\User::USER_ROLE_JUDGE) checked @endif value="{{ \App\User::USER_ROLE_JUDGE }}"> Judge
                                </label>
                                <label class="radio-inline mutliLineRadio">
                                    <input type="radio" name="role" @if(old('role') == \App\User::USER_ROLE_ADMIN) checked @elseif(isset($editUser->role) and $editUser->role == \App\User::USER_ROLE_ADMIN) checked @endif value="{{ \App\User::USER_ROLE_ADMIN }}"> Admin
                                </label>
                                @foreach($errors->get('role') as $message)
                                    <span class="help-block">{{ $message }}</span>
                                @endforeach
                            </div>

                        </div>
                    <hr>
                    @if(isset($editUser)) <input type="hidden" name="id" value="{{$editUser->id}}" />@endif
                    <button type="submit" class="btn btn-lg btn-block btn-primary">@if(isset($editUser))Update Account @else Create an account @endif</button>
                </form>
            </div>
        </div>
    </div>
@endsection