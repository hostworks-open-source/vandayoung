<div class="list-group">
    <a href="/admin/users" class="list-group-item @if(empty($filterArray['filter'])) active @endif">
        All
    </a>
</div>
@include('admin/adminSharedSort')
<a href="/admin/user/" class="btn btn-primary">New User</a>