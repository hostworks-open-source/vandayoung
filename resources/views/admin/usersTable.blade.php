<!-- Table -->
<table class="table table-hover">
    <thead>
    <tr>
        <th>Details</th>
        <th style="text-align: center">Impersonate</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $index => $user)
        <tr>
            <td width="90%" style="text-align: left">
                <p>@if($user->role == \App\User::USER_ROLE_ADMIN)<span class="label label-default">Admin</span> @elseif($user->role == \App\User::USER_ROLE_JUDGE) <span class="label label-info">Judge</span> @else <span class="label label-success">User</span>@endif {{ $user->firstname }} {{$user->surname}} <span class="badge alert-warning">{{ $user->phone }}</span> <a href="mailto:{{ $user->email }}" class="">{{ $user->email }}</a></p>
                <p class="smallDescription">{{ $user->created_at->format('d/m/Y') }},
                    @foreach($user->applications as $app)
                        <a href="/application/{{ $app->id }}">{{\Config('app.applicationPrefix')}}-{{ $app->id }}</a>,
                    @endforeach
                    Action: <a href="/admin/user/{{$user->id}}">Edit</a>
                    @if(in_array($user->role, [\App\User::USER_ROLE_ADMIN, \App\User::USER_ROLE_JUDGE]))
                        View Scores:
                       @foreach($rounds as $round)
                           <a href="{{route('adminViewScores', ['id'=>$user->id, 'roundId'=>$round->id]) }}">{{ $round->name }}</a>
                        @endforeach
                    @endif
                </p>
            </td>
            <td style="text-align: center">
                <a href="/admin/impersonate/account/{{$user->id}}" class="btn btn-primary">Go</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>