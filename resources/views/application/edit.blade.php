@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-12">
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Your Application</h3>
            </div>

            <form method="POST" enctype="multipart/form-data">
                <div class="panel-body" id="updateDetails">

                    <div class="row">
                        {!! csrf_field() !!}
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group @if ($errors->has('gender')) has-error @endif">
                                    <label class="col-md-2 control-label" for="gender">Gender<span class="requiredBadge">*</span> </label>
                                    <div class="col-md-10">
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'male')checked="checked"@elseif(isset($application->gender) and $application->gender == 'male')checked="checked"@endif value="male"> Male
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'female')checked="checked"@elseif(isset($application->gender) and $application->gender == 'female')checked="checked"@endif value="female"> Female
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'na')checked="checked"@elseif(isset($application->gender) and $application->gender == 'na')checked="checked"@endif value="na"> N/A
                                        </label>
                                        @foreach($errors->get('gender') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>

                                </div>
                                <input type="hidden" name="on_behalf" value="0" />
                                <div class="form-group @if ($errors->has('on_behalf')) has-error @endif">
                                    <div class="col-md-10 col-md-offset-2">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="on_behalf" id="on_behalf" @if(old('on_behalf') == '1') checked="checked"@elseif(old('on_behalf') != "0" and isset($application->on_behalf) and $application->on_behalf) checked="checked"@endif value="1"> I'm submitting this work on behalf of a band or group.
                                        </label>
                                    </div>
                                    @foreach($errors->get('on_behalf') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>

                                <div class="form-group @if ($errors->has('performing_name')) has-error @endif">
                                    <label for="performing_name" class="col-md-2 control-label">Performing Name<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mts" id="performing_name" name="performing_name" value="@if(!empty(old('performing_name'))){{ old('performing_name') }}@else{{ $application->performing_name or '' }}@endif">
                                        @foreach($errors->get('performing_name') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('performing_rights_association')) has-error @endif">
                                    <label for="performing_rights_association" class="col-md-2 control-label">Performing Right Association</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mts" id="performing_rights_association" name="performing_rights_association" value="@if(!empty(old('performing_rights_association'))){{ old('performing_rights_association') }}@else{{ $application->performing_rights_association or '' }}@endif">
                                        @foreach($errors->get('performing_rights_association') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('bio')) has-error @endif">
                                    <label class="col-md-2 control-label" for="bio">Biography<span class="requiredBadge">*</span> </label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="bio" id="bio" rows="5">@if(!empty(old('bio'))){{ old('bio') }}@else{{ $application->bio or '' }}@endif</textarea>
                                        <span class="help-block">The bio must be no more than 300 words. <span id="words-remaining">300</span> words remaining.</span>
                                        @foreach($errors->get('bio') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group @if ($errors->has('profile_image')) has-error @endif">
                                    <label class="col-md-2 control-label" for="profile_image">Profile Image<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">

                                        @if(!empty(old('profile_image')))
                                            <input type="hidden" name="profile_image" class="fileIdValue" value="{{ old('profile_image') }}" />
                                            <button type="button" class="btn btn-default fileButton" data-content="{{ old('profile_image') }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span><p>Edit</p></button>
                                            @elseif(!empty($application->profile_image_file_id))
                                            <input type="hidden" name="profile_image" class="fileIdValue" value="{{ $application->profile_image_file_id }}" />
                                            <button type="button" class="btn btn-default fileButton" data-content="{{ $application->profile_image_file_id }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span><p>Edit</p></button>
                                        @endif
                                        <div class="fileInputShow">
                                            <input type="file" id="profile_image" name="profile_image" accept="image/*">
                                            <span class="help-block">Use a photo of yourself. This photo may be used by media. </span>
                                        </div>
                                        @foreach($errors->get('profile_image') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('parent_auth')) has-error @endif">
                                    <label class="col-md-2 control-label" for="parent_auth">Parental Authorisation</label>
                                    <div class="col-md-10">
                                        @if(!empty(old('parent_auth')))
                                            <input type="hidden" name="parent_auth" class="fileIdValue" value="{{ old('parent_auth') }}" />
                                            <button type="button" class="btn btn-default fileButton" data-content="{{ old('parent_auth') }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span><p>Edit</p></button>
                                        @elseif(!empty($application->parent_auth_file_id))
                                            <input type="hidden" name="parent_auth" class="fileIdValue" value="{{ $application->parent_auth_file_id }}" />
                                            <button type="button" class="btn btn-default fileButton" data-content="{{ $application->parent_auth_file_id }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span><p>Edit</p></button>
                                        @endif
                                        <div class="fileInputShow">
                                            <input type="file" id="parent_auth" name="parent_auth">
                                            <span class="help-block"><b>If you are under 18 years of age, please <a href="/docs/VY%20-%20Parental%20Authority%20Form.pdf" target="_blank">download this document</a>, fill out, and upload as part of your application.</b></span>
                                        </div>
                                        @foreach($errors->get('parent_auth') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
                <div class="panel-footer"><div style="float:left"><span class="required">Required field <sup class="requiredSizeBoost">*</sup></span></div><div style="text-align:right"><button type="submit" class="btn btn-lg btn-primary">Next Step</button></div></div>

            </form>
        </div>
    </div>
@endsection

@section('footerScripts')
    @parent

    <script>
        $(document).ready(function () {
            $('#bio').on('keyup', function () {
                calculateWordsRemaining($(this));
            });

            calculateWordsRemaining($('#bio'));
        });
    </script>
@endsection
