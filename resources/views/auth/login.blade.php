@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
<div class="col-sm-4 col-sm-offset-4 small-panel">
    <div class="panel no-margin panel-primary large-panel">
        <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body">
                    <form method="POST" action="/auth/login">
                        {!! csrf_field() !!}
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                                <label class="sr-only" for="email">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">
                            </div>
                            @foreach($errors->get('email') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                                <label class="sr-only" for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            @foreach($errors->get('password') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"@if(old('remember')) checked="checked"@endif> Remember me
                            </label>
                        </div>
                        <button type="submit" class="btn btn-lg btn-block btn-primary">Login</button>
                        <div class="mtm">
                            <p><a href="/password/email">Forgot password</a>.</p>
                        </div>
                    </form>
        </div>
    </div>
</div>
@endsection
