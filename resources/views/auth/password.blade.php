@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-4 col-sm-offset-4 small-panel">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Reset Password</h3>
            </div>
            <div class="panel-body">
                        <form method="POST" action="/password/email">
                            {!! csrf_field() !!}
                            @if (count($errors) > 0)
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="form-group @if ($errors->has('email')) has-error @endif">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                                    <label class="sr-only" for="email">Email</label>
                                    <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">
                                </div>
                                @foreach($errors->get('email') as $message)
                                    <span class="help-block">{{ $message }}</span>
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-lg btn-block btn-primary">Send Reset</button>
                        </form>
            </div>
        </div>
    </div>
@endsection