@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-4 col-sm-offset-4 small-panel">
        <div class="panel panel-primary large-panel no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Update Password</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="/password/update">
                    {!! csrf_field() !!}
                    <div class="form-group @if ($errors->has('old_password')) has-error @endif">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                            <label class="sr-only" for="old_password">Old Password</label>
                            <input type="password" class="form-control" id="old_password" value="{{ old('old_password') }}" name="old_password" placeholder="Old Password">
                        </div>
                        @foreach($errors->get('old_password') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                            <label class="sr-only" for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                        @forelse($errors->get('password') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @empty
                            <span class="help-block">The password must be at least 8 characters.</span>
                        @endforelse
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection