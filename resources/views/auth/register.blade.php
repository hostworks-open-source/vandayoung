@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
<div class="col-sm-4 col-sm-offset-4 small-panel">
    <div class="panel panel-primary no-margin large-panel">
        <div class="panel-heading">
            <h3 class="panel-title">Sign Up</h3>
        </div>
        <div class="panel-body">
                    <form method="POST" action="/auth/register">
                        {!! csrf_field() !!}
                        <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                            <label class="sr-only" for="firstName">Name</label>
                            <input type="text" class="form-control" id="firstName" placeholder="Name" name="firstname" value="{{ old('firstname') }}">
                            @foreach($errors->get('firstname') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if ($errors->has('surname')) has-error @endif">
                            <label class="sr-only" for="lastName">Surname</label>
                            <input type="text" class="form-control" id="lastName" placeholder="Surname" name="surname" value="{{ old('surname') }}">
                            @foreach($errors->get('surname') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                                <label class="sr-only" for="email">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">
                            </div>
                            @foreach($errors->get('email') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></div>
                                <label class="sr-only" for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            @forelse($errors->get('password') as $message)
                            <span class="help-block">{{ $message }}</span>
                            @empty
                            <span class="help-block mtm" style="text-align: center">The password must be at least 8 characters.</span>
                            @endforelse
                        </div>
                        <hr style="margin-top:0">
                        <div class="form-group @if ($errors->has('confirm_terms')) has-error @endif">
                            <div style="text-align: center">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="confirm_terms" id="confirm_terms" @if(old('confirm_terms') == '1') checked="checked"@endif value="1"> I agree to the <a href="{{\Config('app.termsUrl')}}" target="_blank">terms and conditions</a>.
                                </label>
                                @foreach($errors->get('confirm_terms') as $message)
                                <span class="help-block">{{ $message }}</span>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-lg btn-block btn-primary">Create an account</button>
                    </form>
        </div>
    </div>
</div>
@endsection