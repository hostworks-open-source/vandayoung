@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-12">
        <div class="panel no-margin panel-primary large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Your Account</h3>
            </div>
            <form method="POST" action="/details" enctype="multipart/form-data">
            <div class="panel-body bottom" @if(\App\Helpers\General::appsHaveClosed())style="padding-top: 0px" @endif id="updateDetails">
                    <div class="row">
                        @if(\App\Helpers\General::appsHaveClosed())
                            <div class="alert alert-info" style="text-align: center;" role="alert">
                                <p style="margin-top: 0;">The {{Config('app.siteName')}} is now closed for submissions. </p>
                            </div>
                        @endif
                        <div class="alert alert-info">
                            <strong>Please note</strong> that while publishers, producers, and directors may enter on behalf of a composer, the account must be created in the name of the composer.
                        </div>
                        {!! csrf_field() !!}
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                                    <label for="firstName" class="col-md-2 control-label">Name<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                    <input type="text" class="form-control" id="firstName" placeholder="Name" name="firstname" value="@if(!empty(old('firstname'))){{ old('firstname') }}@else{{ Auth::user()->firstname }}@endif">
                                    @foreach($errors->get('firstname') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('surname')) has-error @endif">
                                    <label for="lastName" class="col-md-2 control-label">Surname<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                    <input type="text" class="form-control" id="lastName" placeholder="Surname" name="surname" value="@if(!empty(old('surname'))){{ old('surname') }}@else{{ Auth::user()->surname }}@endif">
                                    @foreach($errors->get('surname') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('email')) has-error @endif">
                                    <label class="col-md-2 control-label" for="email">Email<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="@if(!empty(old('email'))){{ old('email') }}@else{{ Auth::user()->email }}@endif">
                                    @foreach($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                        <span class="help-block"><a href="/password/update">Update my password. </a></span>
                                    </div>
                                </div>
                                @if(\Config('app.userHasDob'))
                                <div class="form-group @if ($errors->has('dob')) has-error @endif">
                                    <label class="col-md-2 control-label" for="dob">DOB<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="dob" name="dob" value="@if(!empty(old('dob'))){{ old('dob') }}@else{{ (!empty($user->dob) and $user->dob != '0000-00-00 00:00:00' and date_create($user->dob)) ? date_create($user->dob)->format('d/m/Y') : '' }}@endif">
                                        @foreach($errors->get('dob') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                        <span class="help-block">Format: DD/MM/YYYY. </span>
                                    </div>
                                </div>
                                @endif

                                @if(\Config('app.profileHasProfileImage'))
                                    <div class="form-group profileImageContainer @if ($errors->has('profile_image')) has-error @endif">
                                        <label class="col-md-2 control-label" for="profile_image">Profile Image<span class="requiredBadge">*</span></label>
                                        <div class="col-md-10" style="text-align: left">

                                            @if(!empty(old('profile_image')))
                                                <input type="hidden" name="profile_image" class="fileIdValue" value="{{ old('profile_image') }}" />
                                                <button type="button" class="btn btn-default fileButton" data-content="{{ old('profile_image') }}"><span class="filename truncate"></span><p>Edit</p></button>
                                            @elseif(!empty($user->profile_image_file_id))
                                                <input type="hidden" name="profile_image" class="fileIdValue" value="{{ $user->profile_image_file_id }}" />
                                                <button type="button" style="width: 250px;text-align:center" class="btn btn-default fileButton" data-content="{{ $user->profile_image_file_id }}">
                                                    <span class="filename truncate" style="width: auto;float:none;"></span>
                                                    <img src="{{ $user->profile_image_url }}" alt="Profile Image" width="100%"/> <p>Edit</p>
                                                </button>
                                            @endif
                                            <div class="fileInputShow">
                                                <input type="file" id="profile_image" name="profile_image" accept="image/*">
                                                <span class="help-block">Please attach a high res image of yourself that can be used by media in related press material. </span>
                                            </div>
                                            @foreach($errors->get('profile_image') as $message)
                                                <span class="help-block">{{ $message }}</span>
                                            @endforeach

                                        </div>
                                    </div>
                                @endif
                                @if(\Config('app.profileHasAdditionalComposers'))
                                <div class="form-group @if ($errors->has('additional_composers')) has-error @endif">
                                    <label for="firstName" class="col-md-2 control-label">Additional Composers</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="additional_composers" name="additional_composers" value="@if(!empty(old('additional_composers'))){{ old('additional_composers') }}@else{{ $user->additional_composers }}@endif">
                                        @foreach($errors->get('additional_composers') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                                @if(\Config('app.profileHasGender'))
                                <div class="form-group @if ($errors->has('gender')) has-error @endif">
                                    <label class="col-md-2 control-label" for="gender">Gender<span class="requiredBadge">*</span> </label>
                                    <div class="col-md-10">
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'male')checked="checked"@elseif(isset($user->gender) and $user->gender == 'male')checked="checked"@endif value="male"> Male
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'female')checked="checked"@elseif(isset($user->gender) and $user->gender == 'female')checked="checked"@endif value="female"> Female
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="gender"@if(old('gender') == 'na')checked="checked"@elseif(isset($user->gender) and $user->gender == 'na')checked="checked"@endif value="na"> N/A
                                        </label>
                                        @foreach($errors->get('gender') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>

                                </div>
                                @endif

                                @if(\Config('app.profileHasAreYouAnApraMember'))
                                <div class="form-group @if ($errors->has('an_apra_member')) has-error @endif" id="an_apra_memberDropdown">
                                    <label class="col-md-4 control-label" for="an_apra_member">Are you an APRA Member?<span class="requiredBadge">*</span></label>

                                    <div class="col-md-8">

                                        <select class="form-control" id="an_apra_member" name="an_apra_member">
                                            <option>Select</option>
                                            <option value="0" @if(old('an_apra_member', $user->an_apra_member) == '0') selected="selected" @endif>No</option>
                                            <option value="1" @if(old('an_apra_member', $user->an_apra_member) == '1') selected="selected" @endif>Yes</option>
                                        </select>

                                        @foreach($errors->get('an_apra_member') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                                @if(\Config('app.profileHasAreWorksRegistered'))
                                <div class="form-group @if ($errors->has('works_are_registered')) has-error @endif" id="works_are_registeredDropdown">
                                    <label class="col-md-4 control-label" for="works_are_registered">Are the works you are entering registered with APRA??<span class="requiredBadge">*</span></label>

                                    <div class="col-md-8">

                                        <select class="form-control" id="works_are_registered" name="works_are_registered">
                                            <option>Select</option>
                                            <option value="0" @if(old('works_are_registered', $user->works_are_registered) == '0') selected="selected" @endif>No</option>
                                            <option value="1" @if(old('works_are_registered', $user->works_are_registered) == '1') selected="selected" @endif>Yes</option>
                                        </select>

                                        <span id="works_registered_help" class="help-block danger" style="display: none;">Please ensure you advise the composer or publisher of the works to contact APRA to register.</span>

                                        @foreach($errors->get('works_are_registered') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                @endif



                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group @if ($errors->has('address')) has-error @endif">
                                    <label for="firstName" class="col-md-2 control-label">Address 1<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                    <input type="text" class="form-control" id="address" name="address" value="@if(!empty(old('address'))){{ old('address') }}@else{{ $user->address or '' }}@endif">
                                    @foreach($errors->get('address') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>

                                </div>
                                <div class="form-group @if ($errors->has('address2')) has-error @endif">
                                    <label for="lastName" class="col-md-2 control-label">Address 2&nbsp;</label>
                                    <div class="col-md-10">
                                    <input type="text" class="form-control" id="address2" name="address2" value="@if(!empty(old('address2'))){{ old('address2') }}@else{{ $user->address2 or '' }}@endif">
                                    @foreach($errors->get('address2') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('city')) has-error @endif">
                                    <label class="col-md-2 control-label" for="city">City<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="city" name="city" value="@if(!empty(old('city'))){{ old('city') }}@else{{ $user->city or '' }}@endif">
                                    @foreach($errors->get('city') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>

                                <div class="form-group @if ($errors->has('postcode')) has-error @endif">
                                    <label class="col-md-2 control-label" for="postcode">Post Code<span class="requiredBadge">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="postcode" name="postcode" value="@if(!empty(old('postcode'))){{ old('postcode') }}@else{{ $user->postcode or '' }}@endif">
                                    @foreach($errors->get('postcode') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>

                                <div class="form-group @if ($errors->has('country')) has-error @endif">
                                    <label class="col-md-2 control-label" for="country">Country<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="country" name="country">
                                            <option></option>
                                            @foreach($countries as $country)
                                            <option value="{{ $country->id }}"@if ($country->id == old('country')) selected="selected"@elseif(isset($user->country_id) and $user->country_id == $country->id) selected="selected"@endif>{{ $country->name }}</option>
                                            @endforeach
                                        </select>

                                    @foreach($errors->get('country') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    </div>
                                </div>

                                <div class="form-group @if ($errors->has('state')) has-error @endif" id="stateText">
                                    <label class="col-md-2 control-label" for="state">State<span class="requiredBadge">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="state" name="state" value="@if(!empty(old('state'))){{ old('state') }}@else{{ $user->state or '' }}@endif">
                                        @foreach($errors->get('state') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('state')) has-error @endif" id="stateDropdown">
                                    <label class="col-md-2 control-label" for="state">State<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="state" name="state">
                                            <option></option>
                                            <option value="ACT" @if(old('state') == 'ACT' or ($user->state == 'ACT' and empty(old('state')))) selected="selected" @endif>ACT</option>
                                            <option value="NSW" @if(old('state') == 'NSW' or ($user->state == 'NSW' and empty(old('state')))) selected="selected" @endif>NSW</option>
                                            <option value="NT" @if(old('state') == 'NT' or ($user->state == 'NT' and empty(old('state')))) selected="selected" @endif>NT</option>
                                            <option value="QLD" @if(old('state') == 'QLD' or ($user->state == 'QLD' and empty(old('state')))) selected="selected" @endif>QLD</option>
                                            <option value="SA" @if(old('state') == 'SA' or ($user->state == 'SA' and empty(old('state')))) selected="selected" @endif>SA</option>
                                            <option value="TAS" @if(old('state') == 'TAS' or ($user->state == 'TAS' and empty(old('state')))) selected="selected" @endif>TAS</option>
                                            <option value="VIC" @if(old('state') == 'VIC' or ($user->state == 'VIC' and empty(old('state')))) selected="selected" @endif>VIC</option>
                                            <option value="WA" @if(old('state') == 'WA' or ($user->state == 'WA' and empty(old('state')))) selected="selected" @endif>WA</option>
                                        </select>

                                        @foreach($errors->get('state') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group @if ($errors->has('phone')) has-error @endif">
                                    <label class="col-md-2 control-label" for="phone">Phone<span class="requiredBadge">*</span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="@if(!empty(old('phone'))){{ old('phone') }}@else{{ $user->phone or '' }}@endif">
                                        <span class="help-block">Eg: +61 2 1111 1111</span>
                                        @foreach($errors->get('phone') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                @if(\Config('app.profileHasBio'))
                                <div class="form-group @if ($errors->has('bio')) has-error @endif">
                                    <label class="col-md-2 control-label" for="bio">Biography<span class="requiredBadge">*</span> </label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="bio" id="bio" rows="5">@if(!empty(old('bio'))){{ old('bio') }}@else{{ $user->bio or '' }}@endif</textarea>
                                        <span class="help-block">The bio must be no more than 300 words. <span class="words-remaining">300</span> words remaining.</span>
                                        @foreach($errors->get('bio') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>

                        </div>


                    </div>


                </div>

                <div class="panel-footer">
                    <span class="required">Required field <sup class="requiredSizeBoost">*</sup></span>
                    <button type="submit" class="btn btn-lg btn-primary pull-right">Save</button>
                    <a href="/applications/create" class="btn btn-lg btn-primary pull-right" style="margin-right: 0.5em;">Start an Application</a>
                    <div style="clear:both"></div>
                </div>



            </form>
        </div>
    </div>
@endsection

@section('footerScripts')
    @parent
    <script>
    $(document).ready(function() {
        $('#works_are_registered').change(function () {
            $(this).val() == 0 ? $('#works_registered_help').show() : $('#works_registered_help').hide();
        });

        if ($('#works_are_registered').val() == 0) {
            $('#works_registered_help').show();
        }

        $('#bio').on('keyup', function () {
            calculateWordsRemaining($(this));
        });

        calculateWordsRemaining($('#bio'));
    });
    </script>
@endsection
