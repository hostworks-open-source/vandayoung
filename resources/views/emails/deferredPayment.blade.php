@extends(\App\Helpers\General::templateWithPrefix('layouts.email'))
@section('content')
    <div style="text-align: left">
        <p>Hi {{ $user->firstname or '' }}</p>

        <p>Thank you for entering the {{ \Config('app.siteName') }}.</p>

        <p>You have chosen to pay later – please be advised that your application will not
            be processed until payment is received. All payments are due by August 10th, 2016.</p>

        <p>The reference ID for credit card payments for your application is {{\Config('app.applicationPrefix')}}-{{ $app->id }}. Please use
            this reference should you decide to pay your entry fee of ${{ $app->billAmount/100 }} online. Payment can be
            made at APRA AMCOS Online Payments site, at <a href="{{ $billingUrl }}">{{ $billingUrl }}</a>. </p>

        <p>Should you decide to pay your application by cheque (Australian residents only) or
            money order, please post your payment along with your name and contact details as
            they appear in your application to:</p>

        <p>
        {{ \Config('app.siteCompanyName') }}<br>
        C/- APRA AMCOS<br>
        Locked Bag 5000<br>
        Strawberry Hills NSW 2012<br>
        AUSTRALIA</p>

        <p>If you have any problems, please contact us at <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a></p>
        <p>Good luck!</p>
    </div>
@endsection