@extends(\App\Helpers\General::templateWithPrefix('layouts.email'))
@section('content')
    <div style="text-align: left">
    <p>Hi {{ $user->firstname or '' }}</p>

    <p>Your account for the {{\Config('app.siteName')}} has been created.</p>
    <p>You are now able to commence the application process if you haven’t already done so.</p>

        <br>
    <p style=""><a href="{{ url('details') }}">Click here to begin.</a></p>
    <br>

    <p>If you have any problems, please contact us at <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a></p>
    <p>Thank you <br>
        APRA AMCOS
    </p>
    </div>
@endsection