@extends(\App\Helpers\General::templateWithPrefix('layouts.email'))
@section('content')
    <div style="text-align: left">
    <p>Hi {{ $user->firstname or '' }}</p>
    <p>
        Click on the following link to reset your password.
    </p>
    <br>
    <p><a href="{{ url('password/reset/'.$token) }}"><b>Reset my password.</b></a></p>
    <br>
    <p>If you have any problems, please contact us at <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a></p>
    <p>Thank you</p>
    </p>
    </div>
@endsection