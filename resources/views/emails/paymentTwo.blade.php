@extends(\App\Helpers\General::templateWithPrefix('layouts.email'))
@section('content')
    <div style="text-align: left">
    <p>Thanks for your application {{ $user->firstname or '' }}</p>
    <p>
        You can update the details of this application, but you will need to create a new application if you wish to submit additional {{ str_plural(\Config('app.filesName')) }}.
    </p>
    <br>
    <p style="text-align: center"><a href="http://{{ \Config::get('app.domain')}}/details">Click here to manage your account</a><br>or<br>
        <a href="http://{{ \Config::get('app.domain')}}/applications/create">Click here to create a new application</a></p>
    <br>
    <p>If you have any problems, please contact us at <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a>
             </p>
    <p>Thank you.</p>
    </div>

    <table cellspacing="10" cellpadding="10" width="100%">
        <tr>
            <td width="300">

            </td>
            <td width="163">
                <b>Qty</b>
            </td>
            <td width="97">
                <b>Total</b>
            </td>
        </tr>


        <tr>
            <td class="center" align="center">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td class="product">
                            <span style="color: #4d4d4d; font-weight:bold;">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</span> <br />
                        </td>
                    </tr>
                </table>
            </td>
            <td class="center" align="center">
                {{ \App\Helpers\General::filteredSongsCount($application->songs) }}
            </td>
            <td class="center" align="center">
                ${{$amount or '0'}}
            </td>
        </tr>



    </table>
@endsection
