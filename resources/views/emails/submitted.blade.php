@extends(\App\Helpers\General::templateWithPrefix('layouts.email'))
@section('content')
    <div style="text-align: left">
        <p>Hi {{ $user->firstname or '' }}</p>

        <p>Thanks for your submission to the {{ \Config('app.siteName') }}.</p>
        <p>
            You can update the details of your account, but you will need to create a new application if you wish to submit additional {{ str_plural(\Config('app.filesName')) }}.
        </p>
        <br>
        <p style="text-align: center"><a href="http://{{ \Config::get('app.domain')}}/details">Click here to manage your account</a><br>or<br>
            <a href="http://{{ \Config::get('app.domain')}}/applications/create">Click here to create a new application</a></p>
        <br>
        <p>If you have any problems, please contact us at <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a>
        </p>
        <p>Thank you<br>
            APRA AMCOS</p>
    </div>
@endsection
