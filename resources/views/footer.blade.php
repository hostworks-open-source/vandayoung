<footer style="text-align: center" id="footer">
    <p>Got a question? Give us a call, send an email or talk to us <a href="{{\Config('app.liveChatUrl')}}" target="_blank">via live chat</a>
        <br>{{ \Config('app.supportPhone') }} | <a href="mailto:{{ \Config('app.supportEmail') }}">{{ \Config('app.supportEmail') }}</a></p>
</footer>