<div class="logoContainer" style="background-color: {{ \Config('app.headerBackground') }}">
    <div class="@if(\Config('app.logoHasContainer')) container @endif">
        <div style="text-align: center" class="row">
            <div class="col-md-12 col-xs-12 logoGuard">
                <a href="/applications"> {!! \Config('app.logoImage') !!} </a>
            </div>
        </div>
    </div>
</div>