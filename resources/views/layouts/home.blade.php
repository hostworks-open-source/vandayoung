<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="{{elixir('css/vanda.css')}}">
    <style>
        body {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    </style>
    <script>
        var acceptedFiles = "{{ \Config('app.acceptedFileExtensions') }}";
        var filesName = "{{ \Config('app.filesName') }}";
        var filesNamePlural = "{{ \Config('app.filesNamePlural') }}";
    </script>
    <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77365206-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body style="padding:0;">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@if(!empty(Request::session()->get('impersonatingUser')))
    <div class="row" style="text-align: center">
        <div class="col-md-12">
            <div class="flash-message">
                <p class="alert alert-warning noBottomMargin">You are impersonating this user. <a href="/admin/stop/impersonating" style="text-decoration: underline">Stop impersonating user</a>.</p>
            </div>
        </div>
    </div>
@endif
@include(\App\Helpers\General::templateWithPrefix('header'))
@include('nav')

<div class="container">

    <!-- Example row columns -->
    <div class="row">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }} mtm noBottomMargin">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
                <p class="alert mtm noBottomMargin globalFlash" style="display: none;"> </p>
        </div> <!-- end .flash-message -->
        <div class="mtm">
        @yield('content')
        </div>
    </div>

    <div class="row mtm foundry mbm" style="text-align: center">

    </div>

    @include(\App\Helpers\General::templateWithPrefix('footer'))

</div> <!-- /container -->

@include('loading')

<div style="height: 76px;width:100%"></div>

<script src="/js/vendor/bootstrap.min.js"></script>

<script src="/js/vendor/dropzone.js"></script>
<script src="/js/main.js?version=1"></script>


</body>
</html>
