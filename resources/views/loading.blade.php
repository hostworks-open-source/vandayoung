<!-- Modal -->
<div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myLoadingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center">
                <img src="/img/loading.gif" alt="Loading..." />
            </div>
        </div>
    </div>
</div>