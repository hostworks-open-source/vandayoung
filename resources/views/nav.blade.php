<nav class="navbar nav navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li @if(Request::is('details'))class="active"@endif><a href="/details">My Account</a></li>
                @if(Auth::user() && in_array(Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                    <li @if(Request::is('admin/dashboard'))class="active"@endif><a href="/admin/dashboard">Dashboard</a></li>
                    <li @if(Request::is('admin/users'))class="active"@endif><a href="/admin/users">Users</a></li>
                    <li @if(Request::is('admin/applications'))class="active"@endif><a href="/admin/applications">Applications</a></li>
                    <li @if(Request::is('admin/panels'))class="active"@endif><a href="/admin/panels">Panels</a></li>
                @endif

                @if(Auth::user() && in_array(Auth::user()->role, [\App\User::USER_ROLE_ADMIN, \App\User::USER_ROLE_JUDGE]))
                    <li @if(Request::is('admin/songs'))class="active"@endif><a href="/admin/songs">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</a></li>
                @else
                    <li @if(Request::is('applications') or Request::is('app/*') or Request::is('submissions/*') or Request::is('application/*'))class="active"@endif><a href="/applications">Applications</a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::user())
                    <li><a href="/auth/logout">Logout</a></li>
                @else
                    <li @if(Request::is('auth/register'))class="active"@endif><a href="/auth/register">Sign Up</a></li>
                    <li @if(Request::is('auth/login')) class="active"@endif><a href="/auth/login">Login</a></li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
