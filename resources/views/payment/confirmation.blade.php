@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Confirmation</h3>
            </div>
            <div class="panel-body">
                <p>Thank you {{ $user->firstname or '' }} for submitting your application. You can update the details of your application or submit new applications. You can not add new {{ str_plural(\Config('app.filesName')) }} to a paid application. </p>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
@endsection