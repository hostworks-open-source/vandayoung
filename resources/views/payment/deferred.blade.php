@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Confirmation</h3>
            </div>
            <div class="panel-body">
                <p>Thank you for submitting – please be advised that your application will not be processed until payment is received. </p>
                <p>All payments are due by August 10th, 2016. </p>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
@endsection