<?php View::share('noRow', true); ?>
@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    @include('sma/admin/header')
    <div class="col-md-3 adminLeftCol">
        @include('admin/scoresFilter')
    </div>

    <div class="col-md-9 adminRightCol">
            <div class="panel panel-default panel-primary no-margin">
                <!-- Default panel contents -->
                <div class="panel-heading"><h3 class="panel-title">{{ $judge->fullName }}</h3></div>

                @include('admin/scoresTable')
            </div>
    </div>
    @include('sma/admin/footer')
@endsection
