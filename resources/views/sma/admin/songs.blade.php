<?php View::share('noRow', true); ?>
@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    @include('sma/admin/header')
            <div class="col-md-3 adminLeftCol">
                @include('admin/songsFilter')

            </div>

            <div class="col-md-9 adminRightCol">
                @include('admin/sharedSearch')
                @include('admin/assignedTable')
            </div>

            <div class="col-md-9 adminRightCol">
                <form class="form-inline" action="/admin/songs/to/round" id="assignForm" method="post">
                    {!! csrf_field() !!}
                    <div class="panel panel-default panel-primary no-margin">
                        <!-- Default panel contents -->
                        <div class="panel-heading"><h3 class="panel-title">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</h3></div>

                        @include('admin/songsTable')
                    </div>


                    @include('admin/songsPagination')
                </form>
            </div>
    @include('sma/admin/footer')
@endsection
