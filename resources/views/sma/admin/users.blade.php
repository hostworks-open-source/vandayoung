<?php View::share('noRow', true); ?>
@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    @include('sma/admin/header')
    <div class="col-sm-3 adminLeftCol">
        @include('admin/usersFilters')
    </div>
    <div class="col-sm-9 adminRightCol">
        @include('admin/sharedSearch')
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Users</h3>
            </div>
            <div class="panel-body">
                @include('admin/usersTable')
            </div>
        </div>
        <div>
            {!! $users->render() !!}
        </div>
        <div class="paginationCount" style="text-align: center">{{ $users->count() }} of {{ $users->total() }} results.</div>
    </div>
    @include('sma/admin/footer')
@endsection