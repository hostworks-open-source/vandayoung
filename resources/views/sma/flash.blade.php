<!-- Example row columns -->

<div class="row">
    <div class="flash-message">

        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <p style="line-height:70px;height:70px;vertical-align:middle;margin: 0px 15px;padding: 0px 20px" class="alert alert-{{ $msg }} noBottomMargin">{{ Session::get('alert-' . $msg) }} <a href="#" style="line-height:70px;height:70px;" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach

    </div> <!-- end .flash-message -->
</div>
