<footer>
	<div class="container">
		<div class="col-md-12 footer-content">
			<div class="pull-left">
				<p>Got a question? Give us a call or send an email.	<br>
					<a href="{{ \Config('app.termsUrl') }}">Terms and Conditions</a> | {{ \Config('app.supportPhone') }} | <a href="mailto:{{ \Config('app.supportEmail') }}"> {{ \Config('app.supportEmail') }}</a>
				</p>
			</div>
		</div>
	</div>
</footer>