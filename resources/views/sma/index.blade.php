@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-md-12">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Applications</h3>
            </div>
            <div class="panel-body" id="updateDetails">
                <table class="table appTable table-hover">
                    <thead>
                    <tr>
                        <th style="text-align: left">Application</th>
                        <th>{{ ucfirst(str_plural(\Config('app.filesName'))) }}</th>
                        @if(\Config('app.canMakePayments'))<th>Paid</th>@endif
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($applications as $app)
                        <tr class="clickable-row" data-href="/app/{{$app->id}}">
                            <td style="text-align: left">
                                <p>
                                    @if(!empty($app->performing_name))
                                        <a href="/app/{{$app->id}}">{{ str_limit($app->performing_name, $limit = 40, $end = '...') }}</a>
                                    @elseif(!empty($app->title))
                                        <a href="/app/{{$app->id}}">{{ $app->title }}</a>
                                    @else
                                        <a href="/app/{{$app->id}}">{{\Config('app.applicationPrefix')}}-{{ $app->id }}</a>
                                    @endif
                                </p>
                                <p class="smallDescription">
                                    {{ date_create($app->created_at)->format('d/m/Y') }}, {{\Config('app.applicationPrefix')}}-{{ $app->id }}
                                </p>
                            </td>
                            <td>{{ \App\Helpers\General::filteredSongsCount($app->songs) }}</td>
                            @if(\Config('app.canMakePayments'))
                                <td>
                                    @if($app->totalPaid > 0)
                                        ${{ $app->totalPaid }}
                                    @else
                                        -
                                    @endif
                                </td>
                            @endif
                            <td>{{ $app->formattedStatus }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class="emptyTable">
                                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                    <p>no applications started</p>
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>


            </div>
            @if(\App\Helpers\General::appsHaveClosed())
            <div class="alert alert-info" style="text-align: center" role="alert">
                <p>The Screen Music Awards is now closed for submissions. Nominations will be announced in October, 2017</p>
            </div>
            @endif
            <div class="panel-footer">
                @if(!\App\Helpers\General::appsHaveClosed() or is_numeric(session('impersonatingUser')))<a class="btn btn-lg btn-primary pull-right" href="@if(Auth::user()){{ "/applications/create" }}@else{{ "/auth/register"}}@endif">Start Application</a>@endif
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
@endsection
