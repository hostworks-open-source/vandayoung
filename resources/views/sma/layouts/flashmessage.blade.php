<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }} mtm noBottomMargin">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
        <p class="alert mtm noTopMargin globalFlash" style="display: none;"> </p>
</div> <!-- end .flash-message -->