<div>
	<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
	<h2><em>drag and drop</em> or <em>click to select</em> mp4 or mov. </h2>
	<p><em>mp3 or m4a</em> for Best Soundtrack Album. </p>
	<p>(Please note: To assist in upload times, only standard definition files are required, using H.264 as the default output and a maximum height of 720. Maximum size of 4GB per file.)</p>
</div>
