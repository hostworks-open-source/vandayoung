@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <input type="hidden" name="applicationId" id="applicationId" value="{{$application->id}}" />
    <div class="col-sm-12">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Your Work</h3>
            </div>
            <div class="panel-body" id="submissions">
                @if($application->status == App\Application::STATE_SONGS_PAID)
                    <div class="flash-message">
                        <p class="alert alert-info">You have paid for your application.  Adding {{ str_plural(\Config('app.filesName')) }} has been disabled. Please click on Start Application to submit a new {{ \Config('app.filesName') }}. </p>
                    </div>
                @endif

                @if($application->status == App\Application::STATE_SUBMITTED)
                    <div class="flash-message">
                        <p class="alert alert-info">
                            You have already submitted this application. Please <a href="/applications/create">start a new application</a> to submit new {{ config('app.filesName') }}.
                        </p>
                    </div>
                @endif

                @unless($application->status == App\Application::STATE_SUBMITTED)
                    <form method="POST" action="https://{{ $googleBucketName }}.storage.googleapis.com" id="myFirstDropZone" enctype="multipart/form-data">
                        <input type="hidden" name="GoogleAccessId" value="{{ $googleEmail }}">
                        <input type="hidden" name="acl" value="bucket-owner-full-control">
                        <input type="hidden" name="policy" value="{{ $policy['policy'] }}">
                        <input type="hidden" name="signature" value="{{ $policy['signature'] }}">
                        <input type="hidden" name="x-goog-meta-SmaApplicationId" value="{{ $application->id }}">

                        <div class="dz-message needsclick">
                            <div class="row">
                                <div class="col-sm-12" id="uploadRegion">
                                    @include(\App\Helpers\General::templateWithPrefix('submission/help'))
                                </div>
                            </div>

                        </div>
                    </form>
                @endunless

                @if($application->status == App\Application::STATE_SUBMITTED)
                    <h2>Submitted {{ str_plural(config('app.filesName')) }}</h2>
                @else
                    <h2>{{ ucfirst(str_plural(config('app.filesName'))) }} <span class="required">required<sup>*</sup></span></h2>
                @endif

                @if(config('app.canMakePayments'))
                    <p class="songsCount"><span id="songCountNo">0</span> {{ str_plural(config('app.filesName')) }} uploaded = $<span id="songCountPrice">0</span> ($50 per {{ config('app.filesName') }})</p>
                @endif

                <div class="nodropzone row">
                    @foreach ($songs as $song)
                        @if($song->children->count() == 0)
                            @include('submission.song', ['song' => $song, 'hideActions' => $application->status == App\Application::STATE_SUBMITTED])
                        @endif
                    @endforeach
                </div>
                <script type="text/javascript">
                   var songCount = {{ count($songs) }};
                </script>
            </div>
            @unless($application->status == App\Application::STATE_SUBMITTED)
                <div class="panel-footer">
                    <span class="required pull-left">{{ ucfirst(config('app.filesName')) }} details are required<sup>*</sup></span>

                    <button id="submitAndPay" class="btn btn-lg btn-primary pull-right mlm" data-application-id="{{ $application->id }}" disabled>Preview Application</button>
                    <button class="btn btn-danger pull-right btn-lg" data-toggle="modal" data-target="#removeApplicationModal" @if(in_array($application->status, [\App\Application::STATE_SONGS_PAID, \App\Application::STATE_SUBMITTED])) disabled @endif>Remove Application</button>
                    <button id="duplicateDetailsButton" class="btn btn-lg btn-default pull-right mlm" type="button" data-toggle="modal" data-target="#duplicateDetailsModal" style="margin-right: 0.5em;">Duplicate details</button>
                    <div style="clear:both"></div>
                </div>
            @endunless
        </div>
    </div>
    @unless($application->status == App\Application::STATE_SUBMITTED)
        <div class="modal fade" id="removeApplicationModal" tabindex="-1" role="dialog" aria-labelledby="removeApplicationModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="removeApplicationModal">Confirm application removal</h4>
              </div>
              <div class="modal-body">
                Are you sure you want to remove this application? It can not be undone.
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, I changed my mind</button>
                <button type="button" id="removeApplication" class="btn btn-danger">Yes, Remove application</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="duplicateDetailsModal" tabindex="-1" role="dialog" aria-labelledby="duplicateDetailsModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Duplicate work details</h4>
              </div>
              <div class="modal-body">
                <p>To reduce repeated data entry, you can copy the details of one piece of work to one or more other works.</p>

                <div class="row">
                    <div class="form-group">
                        <label for="source_file_id" class="control-label col-md-3">Copy from</label>
                        <div class="col-md-9">
                            <select id="source_file_id" class="form-control" name="source_file_id"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="destination_files_group" class="form-group" style="display: none;">
                        <label class="control-label col-md-12">Copy to</label>
                        <div id="destination_files" class="col-md-12"></div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, I changed my mind</button>
                <button type="button" id="duplicateWorkDetails" class="btn btn-primary" disabled>Yes, duplicate details</button>
              </div>
            </div>
          </div>
        </div>

        <div id="preview-template" style="display: none;">
            @include('submission/song', ['song'=>null])
        </div>
    @endunless
@endsection

@section('footerScripts')
    @parent
    @unless($application->status == App\Application::STATE_SUBMITTED)
        <script>
            $(document).ready(function () {
                Dropzone.autoDiscover = false;

                var dropzone = new Dropzone('#myFirstDropZone', {
                    headers: {
                        "Accept": "",
                        "Cache-Control": "",
                        "X-Requested-With": "XMLHttpRequest"
                    },
                    "parallelUploads": 1,
                    "maxFilesize": 4096, // megabytes; ~4GB
                    "createImageThumbnails": false,
                    "previewTemplate": document.getElementById('preview-template').innerHTML,
                    "previewsContainer": '.nodropzone',
                    "acceptedFiles": acceptedFiles
                });

                var applicationFile, songFile;

                dropzone.on('sending', function (file, xhr, formData) {
                    // Attach the file to the user
                    $.ajax('/users/files', {
                        "method": "POST",
                        "async": false,
                        "data": {
                            "file": {
                                "name": file.name,
                                "size": file.size,
                                "type": file.type,
                            }
                        }
                    }).success(function (response) {
                        applicationFile = response;

                        // Attach the file to the submission
                        $.post('/submissions/{{ $application->id }}/files', {"file": response})
                            .success(function (response) {
                                var previewElement = $(file.previewElement);
                                songFile = response;

                                // Setup edit / delete modals
                                previewElement.find('.myEdit').attr('data-target', '#myModal'+response.id);
                                previewElement.find('.myDelete').attr('data-target', '#myDeleteModal'+response.id);
                                previewElement.find('.myModal').attr('id', 'myModal'+response.id);
                                previewElement.find('.myDeleteModal').attr('id', 'myDeleteModal'+response.id);
                                previewElement.find('.myDeleteModal').attr('data-song-id', response.id);
                                previewElement.find('.file').attr('value', applicationFile.id);

                                // Setup modal content
                                previewElement.find('.song-delete').click(setupButtonDelete);
                                previewElement.find('.modal-title').text(applicationFile.name);

                                // Enable edit / delete buttons
                                previewElement.find('.myEdit').prop('disabled', false);
                                previewElement.find('.myDelete').prop('disabled', false);

                                incrementSongsCount();
                            });
                    });

                    formData.append('key', applicationFile.google_storage_key);
                });

                dropzone.on('addedfile', function (file) {
                    var previewElement = $(file.previewElement);

                    if (! previewElement.hasClass('dz-error')) {
                        $('.nodropzone').prepend(previewElement);
                    }

                    checkReadyToPay();
                    loadAllGenreSelects();
                    listenForCategoryChanges();
                });

                dropzone.on('error', function (file, error, xhr) {
                    var previewElement = $(file.previewElement);

                    if (typeof error != 'undefined' && xhr == undefined) {
                        sayFlash('alert-danger', error);
                    } else {
                        sayFlash('alert-danger', 'Sorry, there was an issue saving your file.');
                    }

                    previewElement.find('input[name="hasSaved"]').attr('value', 0);
                    previewElement.closest('.dz-preview').find('.myEdit').removeClass('btn-danger').addClass('btn-default');
                    previewElement.remove();
                    checkReadyToPay();

                    $.ajax('/submissions/{{ $application->id }}/songs/'+songFile.id, {
                        type: 'DELETE'
                    });
                });

                dropzone.on('success', function () {
                    checkReadyToPay();
                });

                var applicationSongs;

                $('#duplicateDetailsModal').on('show.bs.modal', function (e) {
                    $('#destination_files_group').hide();

                    $.get('/submissions/{{ $application->id }}/files')
                        .success(function (response) {
                            applicationSongs = response;

                            $('#source_file_id').empty().append('<option value="">Choose one</option>');

                            $.each(response, function (id, name) {
                                $('#source_file_id').append(
                                    $('<option>').attr('value', id).text(name)
                                );
                            });
                        });
                });

                $('#source_file_id').on('change', function () {
                    var selected = $(this).val();

                    $('#destination_files').empty();

                    Object.keys(applicationSongs).map(function (song) {
                        if (song !== selected) {
                            $('#destination_files').append(
                                $('<div class="checkbox">').append(
                                    $('<label />').append(
                                        $('<input />', {
                                            id: 'destination_file_'+song,
                                            type: 'checkbox',
                                            value: song,
                                            name: "destination_file[]"
                                        }),
                                        applicationSongs[song]
                                    )
                                )
                            );
                        }
                    });

                    if (selected.length) {
                        $('#destination_files_group').slideDown();
                    }
                });

                $('body').on('click', '#destination_files input[type="checkbox"]', function () {
                    if ($('#destination_files input[type="checkbox"]:checked').length > 0) {
                        $('#duplicateWorkDetails').attr('disabled', false);
                    } else {
                        $('#duplicateWorkDetails').attr('disabled', true);
                    }
                });

                $('#duplicateWorkDetails').on('click', function () {
                    $.post('/submissions/{{ $application->id }}/files/duplicate', {
                        source: $('#source_file_id').val(),
                        destination: $('#destination_files input[type="checkbox"]:checked').get().map(function (el) {
                            return el.value;
                        })
                    }).success(function (response) {
                        window.location = window.location;
                        return;
                    });
                });
            });
        </script>
    @endunless
@endsection
