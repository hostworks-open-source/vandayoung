@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))

@section('content')
    <div class="col-sm-12">
        @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">State</h3>
            </div>
            <div class="panel-body" id="submissions">
                <table class="table table-hover previewApplication">
                    <tbody>
                    <tr>
                        <th class="">Id</th>
                        <td>{{\Config('app.applicationPrefix')}}-{{ $application->id }}</td>
                    </tr>
                    <tr>
                        <th class="">Status</th>
                        <td>{{ $application->formattedStatus }}</td>
                    </tr>

                    <tr>
                        <th class="">{{ ucfirst(str_plural(\Config('app.filesName'))) }}</th>
                        <td>
                            {{ \App\Helpers\General::filteredSongsCount($application->songs) }}
                        </td>
                    </tr>
                    @if(\Config('app.canMakePayments'))
                    <tr>
                        <th class="">Paid</th>
                        <td class="">@if($application->totalPaid > 0)
                                ${{ $application->totalPaid }}
                            @else
                                -
                            @endif</td>
                    </tr>
                    @endif

                    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]) and $application and $application->user)
                        <tr>
                            <th class="">Impersonate</th>
                            <td class=""><a href="/admin/impersonate/account/{{ $application->user->id }}">{{ $application->user->firstname }} {{ $application->user->surname }}</a></td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="panel-footer"></div>
        </div>
        @if(\Config('app.canMakePayments'))
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Payments</h3>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="tLeft" style="width:1%">ID</th>
                        <th class="tCenter" style="width:15%">Settlement Date</th>
                        <th class="tCenter" style="width: 20%">Status</th>
                        <th class="tCenter">Expiry</th>
                        <th class="tCenter">Reference</th>
                        <th class="tCenter">Created</th>
                        <th class="tRight" style="width:1%">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($application->transactions as $transaction)
                        <tr>
                            <td class="rLeft">{{ $transaction->txnid }}</td>
                            <td class="tCenter">{{ $transaction->settlement_date }}</td>
                            <td class="tCenter">{{ $transaction->rescode }} - {{ $transaction->restext }}</td>
                            <td class="tCenter">{{ $transaction->expiry }}</td>
                            <td class="tCenter">{{ $transaction->reference }}</td>
                            <td class="tCenter">{{ date_create($transaction->created_at) ? date_create($transaction->created_at)->format('d/m/Y H:i:s') : '' }}</td>
                            <td class="tRight">${{ number_format($transaction->amount/100, 2) }}</td>
                        </tr>
                    @empty
                        <tr>
                        <td colspan="8" style="text-align: center">
                            <div class="emptyTable">
                                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                <p>no transactions</p>
                            </div>
                        </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="panel-footer"></div>
        </div>
        @endif
        @endif
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Details @if(!in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))<a href="/details" class="inline-edit">Edit</a>@endif</h3>
            </div>
            <div class="panel-body" id="submissions">
                <table class="table table-hover previewApplication">
                    <tbody>
                    <tr>
                    <th class="">Name</th>
                    <td class="">{{ $user->firstname }}</td>
                    </tr>

                    <tr>
                    <th class="">Surname</th>
                    <td class="">{{ $user->surname }}</td>
                    </tr>

                    <tr>
                    <th class="">Email</th>
                    <td class=""><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                    </tr>

                    <tr>
                    <th class="">Phone</th>
                    <td class="">{{ $user->phone }}</td>
                    </tr>

                    @if(\Config('app.userHasDob'))
                    <tr>
                    <th class="">DOB</th>
                    <td class="">{{ (!empty($user->dob) and $user->dob != '0000-00-00 00:00:00' and date_create($user->dob)) ? date_create($user->dob)->format('d/m/Y') : '' }}</td>
                    </tr>
                    @endif

                    <tr>
                    <th class="">Address</th>
                    <td class="">{{ $user->address}}<br>{{$user->address2}}<br>{{$user->city}}, {{$user->state}}, {{$user->postcode}}, {{$user->country->name or ''}}</td>
                    </tr>

                    @if(!is_null($user->an_apra_member))
                    <tr>
                    <th class="">APRA Member</th>
                    <td class="">{{ $user->an_apra_member ? "Yes" : "No" }}</td>
                    </tr>
                    @endif

                    @if(!is_null($user->additional_composers))
                    <tr>
                    <th class="">Additional Composers</th>
                    <td class="">{{ $user->additional_composers }}</td>
                    </tr>
                    @endif
                    @if(!is_null($user->gender))
                    <tr>
                        <th class="">Gender</th>
                        <td class="">{{ ucfirst(trans($user->gender)) }}</td>
                    </tr>
                    @endif

                    @if(!empty($user->bio))
                    <tr>
                        <td colspan="2"><pre class="breakWords" style=""> {{ $user->bio }}</pre></td>
                    </tr>
                    @endif

                    @if($user->profileImage)
                        <tr>
                            <th class="">Profile Image</th>
                            <td class="previewThumb">
                                <img src="{{ $user->profile_image_url }}" class="thumbnail" alt="Profile Image" style="width: 300px;">
                                {{ $user->profileImage->name }}
                            </td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="panel-footer"></div>
        </div>
    </div>
    @if(!\Config('app.skipApplicationPage'))
    <div class="col-sm-12">
        <div class="panel panel-primary no-margin">
            <div class="panel-heading">
                <h3 class="panel-title">Application Details @if(!in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))<a href="/app/{{$application->id}}" class="inline-edit">Edit</a>@endif</h3>
            </div>
            <div class="panel-body" id="submissions">
                <table class="table table-hover previewApplication">
                    <tbody>

                    <tr>
                    <th class="">Gender</th>
                    <td class="">{{ ucfirst(trans($application->gender)) }}</td>
                    </tr>

                    <tr>
                    <th class="">On Behalf Of The Artist</th>
                    <td class="">{{ $application->on_behalf ? "Yes" : "No" }}</td>
                    </tr>

                    <tr>
                    <th class="">Performing Name</th>
                    <td class="">{{ $application->performing_name}}</td>
                    </tr>

                    <tr>
                    <th class="">Performing Right Association</th>
                    <td class="">{{ $application->performing_rights_association}}</td>
                    </tr>




                    @if($application->profileImage)
                        <tr>
                            <th class="">Profile Image</th>
                            <td class="previewThumb">
                                <img src="{{ $user->profile_image_url }}" class="thumbnail" alt="Profile Image" style="width: 300px;">
                                {{ $application->profileImage->name }}
                            </td>
                        </tr>
                    @endif
                    @if($application->parentAuth)
                        <tr>
                            <th class="">Parental Authorisation</th>
                            <td class="previewThumb" style="text-align: center">
                                <a class="thumbnail" href="/media/download/{{ $application->parentAuth->name_sha1 }}/{{ urlencode($application->parentAuth->name) }}">
                                    <img src="/media/download/{{ $application->parentAuth->name_sha1 }}/{{ urlencode($application->parentAuth->name) }}" /> {{ $application->parentAuth->name }}
                                </a>
                            </td>
                        </tr>
                    @endif

                    <tr>
                        <td colspan="2"><pre class="breakWords" style=""> {{ $application->bio }}</pre></td>
                    </tr>

                    </tbody>
                </table>

            </div>
            <div class="panel-footer"></div>
        </div>
    </div>
    @endif
    @foreach ($songs as $song)
        @if(!$song->parent)
        @include('submission/previewSong', ['song'=>$song])
        @endif
        @if($song->children)
            @foreach($song->children()->orderBy('title')->get() as $childSong)
                @include('submission/previewSong', ['song'=>$childSong])
            @endforeach
        @endif
    @endforeach
    @if(\Auth::user()->id == $user->id)

    <div class="col-sm-12">
        <div class="panel panel-primary no-margin actionContainer">
            <div class="panel-body  bottom">

            @if(\Config('app.canMakePayments'))

                @if(in_array($application->status, [\App\Application::STATE_UNPAID, \App\Application::STATE_DEFERRED]))
                    <a href="/pay/{{$application->id}}" class="mlm btn btn-lg btn-primary pull-right">Submit and Pay</a>
                    @else
                    <button class="btn btn-lg btn-primary mlm pull-right" disabled>Submit and Pay</button>
                @endif
                @if(in_array($application->status, [\App\Application::STATE_SONGS_PAID, \App\Application::STATE_DEFERRED]))
                    <a href="#" class="btn btn-lg btn-primary pull-right mlm" disabled>Submit and Pay Later</a>
                @else
                    <a href="/pay/{{$application->id}}?defer=true" class="btn btn-lg btn-primary pull-right mlm">Submit and Pay Later</a>
                @endif

            @else
                @if(in_array($application->status, [\App\Application::STATE_SUBMITTED]))
                    <button class="btn btn-lg btn-primary pull-right mlm" type="button" disabled>Submit For Judging</button>
                @else
                    <a href="/judge/submit/{{$application->id}}?submit=true"
                            id="submit-application"
                            class="btn btn-lg btn-primary pull-right"
                            type="button"
                            {{ ! $application->canBeSubmitted() ? 'disabled' : '' }}
                    >Submit For Judging</a>
                    <a href="/submissions/add/{{ $application->id }}"
                       class="btn btn-lg btn-default pull-right"
                       style="margin-right: 0.5em;"
                    >Back to application</a>
                @endif

            @endif

            </div>
        </div>
    </div>
    @else
        <div class="col-sm-12">
            <div class="panel panel-primary no-margin actionContainer">
                <div class="panel-body  bottom">
            </div>
            </div>
        </div>
    @endif
@endsection
