<?php $images = ['/img/videopreview.png'] ?>
<div class="col-sm-12">
    <div class="panel panel-primary @if(isset($index) and $index != 0){{ $marginTop or 'no-margin' }}@else no-margin @endif @if(isset($collapsePanels) and $collapsePanels) panelToggle @endif ">
        <div class="panel-heading">
            @if(isset($collapsePanels) and $collapsePanels)<span class="glyphicon glyphicon-plus" aria-hidden="true" style="float:right"></span>@endif
            <h3 class="panel-title">@if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN])) @if(!empty($song->application->performing_name)){{$song->application->performing_name }}@else{{  $song->application->user->fullname or ''}}@endif - @endif @if($song->is_multi_track) {{ $song->multi_track_name }} @else{{ $song->title }} @endif @if($song->application and $song->application->user_id == \Auth::user()->id) <a class="inline-edit" href="/submissions/add/{{$song->application->id}}">Edit</a>@endif</h3>
        </div>
        <div class="panel-body">
            @if($song->file && $song->file->canStream())
                <div class="text-center">
                    {!! $song->file->embed_code !!}
                </div>
            @endif

            <table class="table table-hover previewApplication">
                <tbody>
                <tr style="display:none" class="firefoxRequiresFlash">
                    <th style="text-align: center" colspan="2" class="alert-warning"><span><a href="https://get.adobe.com/flashplayer">Firefox requires flash for video playback. </a></span></th>
                </tr>
                @if($song->file && !$song->file->hasProcessingError() && $song->file->isProcessing())
                <tr style="text-align: center">
                    <td colspan="2" class="alert alert-info">
                        <p>This media is being processed for playback and will be available shortly.</p>
                    </td>
                </tr>
                @endif
                @if($song->isVideo() and $song->file->hasProcessingError())
                    <tr style="text-align: center">
                        <td colspan="2" class="alert alert-warning">We had trouble processing this media for playback. </td>
                    </tr>
                @endif
                @if($song->isVideo() and $song->file->isSending())
                    <tr style="text-align: center">
                        <td colspan="2" class="alert alert-danger">The upload did not complete successfully or is still in progress, upload started {{ $song->created_at }}.
                            <br>
                            If no longer in progress, please re-upload and try again.
                        </td>
                    </tr>
                @endif
                @if($song->year)
                <tr>
                    <th class="">Year</th>
                    <td class="">{{ $song->year }}</td>
                </tr>
                @endif

                @if($song->first_venue)
                <tr>
                    <th class="">First Venue</th>
                    <td class="">{{ $song->first_venue }}</td>
                </tr>
                @endif

                @if($song->additional_composers and (!in_array(\Auth::user()->role, [\App\User::USER_ROLE_JUDGE]) or (isset($song->application) and $song->application->user_id == \Auth::user()->id )))
                <tr>
                    <th class="">Additional Composers</th>
                    <td class="">{{ $song->additional_composers }}</td>
                </tr>
                @endif

                @if(\Config('app.filesHaveReleaseDate') and !empty($song->first_broadcast))
                <tr>
                    <th class="">First Broadcast</th>
                    <td class="">{{ (!empty($song->first_broadcast) and $song->first_broadcast != '0000-00-00 00:00:00' and date_create($song->first_broadcast)) ? date_create($song->first_broadcast)->format('d/m/Y') : '' }}</td>
                </tr>
                @endif

                @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]))
                    <tr>
                        <th class="">Writer</th>
                        <td class="">{{ $song->application->user->firstname or '' }} {{ $song->application->user->surname or '' }}</td>
                    </tr>
                @endif

                @if(!empty($song->other_genre) or $song->genre)
                <tr>
                    <th class="">Genre</th>
                    <td class="">@if(!empty($song->other_genre)){{ $song->other_genre }} @else {{ $song->genre->name or '' }}@endif</td>
                </tr>
                @endif

                @if($song->category)
                <tr>
                    <th class="">Category</th>
                    <td class="">{{ $song->category->name or '' }}</td>
                </tr>
                @endif

                @if($song->queue_sheet)
                <tr>
                    <th class="">@if($song->category and !empty($song->category->cuesheet_name)) {{ $song->category->cuesheet_name }} @else Cue Sheet @endif</th>
                    <td class=""><a target="_blank" href="{{ $song->queue_sheet->direct_download_url }}">{{$song->queue_sheet->name}}</a></td>
                </tr>
                @endif

                @if($song->main_score)
                <tr>
                    <th class="">Main score</th>
                    <td class=""><a target="_blank" href="{{ $song->main_score->direct_download_url }}">{{$song->main_score->name}}</a></td>
                </tr>
                @endif

                @if($song->lead_sheet)
                <tr>
                    <th class="">Lead sheet of main score</th>
                    <td class=""><a target="_blank" href="{{ $song->lead_sheet->direct_download_url }}">{{$song->lead_sheet->name}}</a></td>
                </tr>
                @endif

                @if($song->main_score_mid)
                <tr>
                    <th class="">Main score (*.mid file)</th>
                    <td class=""><a target="_blank" href="{{ $song->main_score_mid->direct_download_url }}">{{$song->main_score_mid->name}}</a></td>
                </tr>
                @endif

                @if(!empty($song->lyrics))
                <tr>
                    <td colspan="2"><pre style="margin-top:10px;" class="breakWords">{{ $song->lyrics }}</pre></td>
                </tr>
                @endif

                @if(isset($judgingRounds))
                    @if ($song->category->id == 19)
                        <tr>
                            <td colspan="2" class="alert alert-warning" style="text-align: center">
                                Applying this score, applies the same score to every song in the album.
                            </td>
                        </tr>
                    @endif
                        <tr>
                            @if($song->category)
                                <td colspan="2" class="alert alert-success" style="text-align: center">{!! $song->category->judging_help !!} </td>
                            @elseif($song->children->count() > 0)
                                 <td colspan="2" class="alert alert-success" style="text-align: center">{!! $song->children()->first()->category->judging_help !!}  </td>
                            @else
                                <td colspan="2" class="alert alert-success" style="text-align: center">{{Config('app.scoringHelp')}} </td>
                            @endif
                        </tr>
                        @foreach($judgingRounds as $songRound)
                            <tr>
                                <th>{{$songRound->round->name}} Score</th>
                                <td class="judgeScore">

                                    <form class="form-inline judgeScoreForm" style="display:inline">
                                        <div class="form-group" style="width: 150px">
                                            <div class="loadingContainer"></div>
                                            <button type="submit" class="btn btn-primary" style="float:right;" @if(!$songRound->round->isActive()) disabled @endif>
                                                Set
                                            </button>
                                            <select id="score" name="score" class="form-control" style="width: 50px;padding-left:5px;padding-right:0px;" @if(!$songRound->round->isActive()) disabled @endif>
                                                @foreach($scoreRange as $score)
                                                    <option value="{{ $score }}"@if (isset($scoresByRound[$songRound->id]) and $scoresByRound[$songRound->id]->score == $score) selected="selected"@endif>{{ $score }}</option>
                                                @endforeach
                                            </select>

                                            <input type="hidden" name="songRound" value="{{$songRound->id}}">
                                            {!! csrf_field() !!}

                                        </div>
                                    </form>
                                    @if(\Config('app.lowestScore') == 0)
                                        @if (isset($scoresByRound[$songRound->id]))
                                            <p style="display: inline">Viewed</p>
                                            @else
                                            <form class="form-inline scoreAsRead" style="display:inline;">
                                                <div class="form-group" style="line-height:30px;">
                                                    <div class="loadingContainer"></div>
                                                    <button type="submit"  @if(!$songRound->round->isActive()) disabled @endif>
                                                        Mark as viewed
                                                    </button>
                                                    <input type="hidden" name="songRound" value="{{$songRound->id}}">
                                                    {!! csrf_field() !!}

                                                </div>
                                            </form>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                @endif
                </tbody>
            </table>
            @if(isset($savedSearch) and $savedSearch and !$song->parent)
                <a href="{{route('adminListSongs', $savedSearch->queryArray)}}" class="btn btn-default">Back To {{ ucfirst(str_plural(\Config('app.filesName'))) }}</a>
                <a style="float:right;" href="{{route('adminListSongs', array_merge($savedSearch->queryArray, ['next'=>$song->id]))}}" class="btn btn-default">Next {{ ucfirst(\Config('app.filesName')) }}</a>
            @endif
        </div>
        <div class="panel-footer"></div>
    </div>

    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]) and (!$song->category or ($song->category and !$song->category->is_multi_track)))
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">Round Assignments</h3></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Round</th>
                <th style="text-align: center">Entity</th>
                <th style="text-align: right">Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($song->songRounds as $songRound)
               <tr>
                   <td>{{$songRound->round->name}}</td>
                   <td style="text-align: center">{{$songRound->entity->displayName }}</td>
                   <td style="text-align: right">
                       <form action="/admin/remove/songs/to/round" method="post">
                           <input type="hidden" name="songRoundId" value="{{ $songRound->id }}" />
                           {!! csrf_field() !!}
                           <button type="submit" class="btn btn-primary">Delete</button>
                       </form>
                   </td>
               </tr>
                @empty
                <tr>
                    <td colspan="3">
                        <div class="emptyTable">
                            <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                            <p>no assignments</p>
                        </div>
                    </td>
                </tr>

            @endforelse
            </tbody>
        </table>
    </div>
    @endif

    @if(in_array(\Auth::user()->role, [\App\User::USER_ROLE_ADMIN]) and (!$song->category or ($song->category and !$song->category->is_multi_track)))
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">Votes</h3></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Round</th>
                <th style="text-align: center">Judge</th>
                <th style="text-align: center">Score</th>
                <th style="text-align: right">Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($song->scores as $score)
               <tr>
                   <td>{{ $score->round->name }}</td>
                   <td style="text-align: center">{{$score->user->fullName }}</td>
                   <td style="text-align: center">{{$score->score }}</td>
                   <td style="text-align: right">
                       <form action="/admin/remove/scores/from/round" method="post">
                           <input type="hidden" name="score_id" value="{{ $score->id }}" />
                           {!! csrf_field() !!}
                           <button type="submit" class="btn btn-primary">Delete</button>
                       </form>
                   </td>
               </tr>
                @empty
                <tr>
                    <td colspan="4">
                        <div class="emptyTable">
                            <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                            <p>no scores</p>
                        </div>
                    </td>
                </tr>

            @endforelse
            </tbody>
        </table>
    </div>
    @endif
</div>
