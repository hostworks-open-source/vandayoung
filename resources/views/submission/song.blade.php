<div class="dz-preview col-sm-4 col-xs-12 mbm dz-file-preview @if(isset($song))dz-processing dz-success dz-complete @endif">
    <div class="dz-image" style=""><span class="glyphicon {{config('app.fileIcon')}}" aria-hidden="true" style="font-size: 60px;color:{{config('app.fileIconColor')}};margin:0; top: 50%;transform: translateY(-50%)"></span></div>

    <div class="dz-details">
        <div class="dz-size"><!--<span data-dz-size></span> --><span data-dz-name class="dz-file-name truncate">@if(!empty($song->title)){{$song->title}}@else{{ $song->file->name or ''}}@endif</span></div>
    </div>
    @if(!isset($song->id))
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
    <div class="dz-error-message" style="display: none"><span data-dz-errormessage></span></div>
    @endif

    <!-- Modal -->
    <div class="modal fade myModal" id="myModal{{ data_get($song, 'id') }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title truncate">@if(!empty($song->title)){{$song->title}}@else{{ $song->file->name or ''}}@endif</h4>
                    @if(isset($song) and $song->file and $song->file->canDirectDownload()) <p>Direct Download: <a target="_blank" href="/media/download/{{$song->file->name_sha1}}/{{urlencode($song->file->name)}}">{{$song->file->name}}</a></p> @endif
                </div>

                <form method="POST" action="/submissions/song" enctype="multipart/form-data" class="songForm">
                    <input type="hidden" name="hasSaved" value="@if(empty($song->title)){{ 0 }}@else{{ 1 }}@endif" />
                    <div class="modal-body">

                    <div class="song-success alert alert-success" style="display: none;"></div>

                    <p class="alert" style="display:none;"></p>
                        <div class="form-group" style="display:none;">
                            <input type="hidden" name="file" id="file" class="file"@if(isset($song->file->id)) value="{{ $song->file->id }}"@endif />
                            <span class="help-block"></span>
                        </div>
                            <div class="form-group">
                                <label for="title" class="control-label">Work / Series / Film Title <span class="requiredBadge">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $song->title or ''}}">
                                <span class="help-block"></span>
                            </div>

                            @if(config('app.filesHaveReleaseDate'))
                                <div class="form-group">
                                    <label class="control-label" for="first_broadcast">Date of first broadcast <span class="requiredBadge">*</span></label>

                                    <div class='input-group date' id='datetimepicker'>
                                       <input type='text' readonly class="form-control" id="first_broadcast" name="first_broadcast" max="" min="" value="{{ (!empty($song->first_broadcast) and $song->first_broadcast != '0000-00-00 00:00:00' and date_create($song->first_broadcast)) ? date_create($song->first_broadcast)->format('d/m/Y') : '' }}"/>
                                       <span class="input-group-addon">
                                           <span class="glyphicon glyphicon-calendar"></span>
                                       </span>
                                    </div>

                                    @foreach($errors->get('first_broadcast') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                    <span class="help-block">Format: DD/MM/YYYY. </span>
                                    <span class="help-block smallDesc">Please refer to the <a href="{{config('app.termsUrl')}}" target="_blank">terms and conditions</a> regarding eligibility dates. </span>
                                </div>
                            @endif

                            @if(config('app.filesHaveReleaseVenue'))
                                <div class="form-group">
                                    <label for="first_venue" class="control-label">Venue of first broadcast <span class="requiredBadge">*</span></label>
                                    <input type="text" class="form-control" id="first_venue" name="first_venue" value="{{ $song->first_venue or ''}}">
                                    <span class="help-block"></span>
                                </div>
                            @endif

                            @if(config('app.filesHaveAdditionalComposers'))
                                <div class="form-group">
                                    <label for="first_venue" class="control-label">Additional Composers</label>
                                    <input type="text" class="form-control" id="additional_composers" name="additional_composers" value="{{ $song->additional_composers or ''}}">
                                    <span class="help-block"></span>
                                </div>
                            @endif

                            @if(config('app.filesHaveCategory'))
                            <div class="form-group categoryBroadContainer">
                                <label class="control-label" for="category">Category <span class="requiredBadge">*</span></label>
                                <select class="form-control categorySelect" name="category" id="category">
                                    @foreach($optGroups as $optGroup)
                                        <optgroup label="{{$optGroup}}">
                                        @foreach($categories[$optGroup] as $category)
                                            <option data-has-cuesheet="{{$category->has_cue_sheet}}" data-help="{{$category->help}}" data-cuesheet-name="{{$category->cuesheet_name}}" data-cuesheet-required="{{$category->cuesheet_required}}" data-airdate-from="{{\date_create($category->airdate_from)->format('d/m/Y')}}" data-airdate-to="{{\date_create($category->airdate_to)->format('d/m/Y') }}" value="{{ $category->id }}"@if(isset($song->category_id) and $category->id == $song->category_id) selected="selected"@endif>{{ $category->name }}</option>

                                        @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>

                                <span class="help-block"></span>
                            </div>
                            @endif
                            @if(config('app.filesHaveGenre'))
                            <div class="form-group">
                                <label class="control-label" for="genre">Genre <span class="requiredBadge">*</span></label>
                                    <select class="form-control genreSelect" name="genre" id="genre">
                                        <option></option>
                                        <option value="36" @if(isset($song->genre_id) and 36 == $song->genre->id) selected="selected"@endif>Other</option>
                                        @foreach($genres as $genre)
                                            @if($genre->name != "Other")
                                            <option value="{{ $genre->id }}"@if(isset($song->genre_id) and $genre->id == $song->genre_id) selected="selected"@endif>{{ $genre->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                <span class="help-block"></span>
                            </div>

                            <div class="form-group otherInput" @if(isset($song) and $song->genre_id == 36) style="display:block;" @endif>
                                <label for="title" class="control-label">Name (Genre) <span class="requiredBadge">*</span></label>
                                <input type="text" class="form-control" id="other_genre" name="other_genre" value="{{ $song->other_genre or ''}}">
                                <span class="help-block"></span>
                            </div>
                        @endif

                        @if(config('app.filesHaveQueueSheet'))
                            <div class="form-group cueSheetContainer">
                                <label class="control-label" for="queue_sheet"><span style="font-weight: bold" class="cueSheetName">Queue Sheet</span> <span class="requiredBadge">*</span></label>
                                @if(!empty($song->queue_sheet_file_id))
                                    <input type="hidden" name="queue_sheet" class="fileIdValue" value="{{ $song->queue_sheet_file_id }}" />
                                    <button type="button" class="btn btn-default fileButton" style="display: block;" data-content="{{ $song->queue_sheet_file_id }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span><p>Edit</p></button>
                                @endif

                                <div class="fileInputShow">
                                    <input type="file" id="queue_sheet" name="queue_sheet" accept=".pdf, .doc, .docx, .jpg, image/jpeg, image/pjpeg, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                    <span class="help-block">Please provide in PDF, DOC, DOCX or JPG format. </span>
                                </div>
                            </div>
                        @endif


                        @if(config('app.filesHaveYear'))
                            <div class="form-group">
                                <label class="control-label" for="year">Year <span class="requiredBadge">*</span></label>
                                    <select class="form-control" id="year" name="year">
                                        <option></option>
                                        @foreach($years as $year)
                                            <option value="{{ $year }}"@if(isset($song->year) and $year == $song->year) selected="selected"@endif>{{ $year }}</option>
                                        @endforeach
                                    </select>

                                <span class="help-block"></span>
                            </div>
                        @endif
                        @if(config('app.filesHaveLyrics'))
                            <div class="form-group">
                                <label class="control-label" for="lyrics">Lyrics</label>
                                    <textarea class="form-control" name="lyrics" id="lyrics" rows="12">{{ $song->lyrics or '' }}</textarea>
                                    <span class="help-block"></span>
                            </div>
                        @endif
                        @if(config('app.filesHaveMainScore') || config('app.filesHaveLeadSheet') || config('app.filesHaveMidOfMainScore'))
                            <hr>
                            <p>
                                <strong>Additional materials required</strong><br><br>
                                A sample of your work from the score/theme is required for possible adaptation/orchestration by this year's Musical Director for the orchestral ensemble on the night.  Please supply the following.
                            </p>
                        @endif
                        @if(config('app.filesHaveMainScore'))
                            <div class="form-group">
                                <label class="control-label" for="main_score">Main score (mp3, m4a) <span class="requiredBadge">*</span></label>
                                @if(!empty($song->main_score_file_id))
                                    <input type="hidden" id="main_score" name="main_score" class="fileIdValue" value="{{ $song->main_score_file_id }}" />
                                    <button type="button" class="btn btn-default fileButton" style="display: block;" data-content="{{ $song->main_score_file_id }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span>
                                        <p>Edit</p>
                                    </button>
                                    <span class="help-block"></span>
                                @endif

                                <div class="fileInputShow">
                                    <input id="main_score" name="main_score" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        @endif
                        @if(config('app.filesHaveLeadSheet'))
                            <div class="form-group">
                                <label class="control-label" for="lead_sheet">Lead sheet of main score (PDF) <span class="requiredBadge">*</span></label>
                                @if(!empty($song->lead_sheet_file_id))
                                    <input type="hidden" name="lead_sheet" class="fileIdValue" value="{{ $song->lead_sheet_file_id }}" />
                                    <button type="button" class="btn btn-default fileButton" style="display: block;" data-content="{{ $song->lead_sheet_file_id }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span>
                                        <p>Edit</p>
                                    </button>
                                    <span class="help-block"></span>
                                @endif

                                <div class="fileInputShow">
                                    <input id="lead_sheet" name="lead_sheet" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        @endif
                        @if(config('app.filesHaveMidOfMainScore'))
                            <div class="form-group">
                                <label class="control-label" for="main_score_mid">Main score (eg. MIDI file), Max. 1MB</label>
                                @if(!empty($song->main_score_mid_file_id))
                                    <input type="hidden" name="main_score_mid" class="fileIdValue" value="{{ $song->main_score_mid_file_id }}" />
                                    <button type="button" class="btn btn-default fileButton" style="display: block;" data-content="{{ $song->main_score_mid_file_id }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"> </span><span class="filename truncate"></span>
                                        <p>Edit</p>
                                    </button>
                                    <span class="help-block"></span>
                                @endif

                                <div class="fileInputShow">
                                    <input id="main_score_mid" name="main_score_mid" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <span class="loadingContainer" style="display: none"></span>
                        <i class="song-success-check glyphicon glyphicon-ok pull-right" style="display: none; color: #00cc00;"></i>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

        <!-- Modal -->
        <div class="modal fade myDeleteModal" id="myDeleteModal{{ data_get($song, 'id') }}" data-song-id="{{ data_get($song, 'id') }}" tabindex="-1" role="dialog" aria-labelledby="myDeleteModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title truncate">@if(!empty($song->title)){{$song->title}}@else{{ $song->file->name or ''}}@endif</h4>
                    </div>

                    <form method="POST" class="deleteSongForm">
                        <input class="songId" type="hidden" value="{{ data_get($song, 'id') }}">
                        <div class="modal-body">
                            <p class="alert" style="display:none;"></p>
                            <div class="form-group" style="display:none;">
                                <span class="help-block"></span>
                            </div>
                            <p class="question">Are you sure that you want to delete this {{ config('app.filesName') }}? This action can not be undone. </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default song-dismiss" data-dismiss="modal">No, Keep.</button>
                            <button type="button" class="btn btn-primary song-delete">Yes, Delete.</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <div class="dz-success-mark"@if(isset($song->id))@endif>

        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
            <title>Check</title>
            <desc>Created with Sketch.</desc>
            <defs></defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#4DB95C" sketch:type="MSShapeGroup"></path>
            </g>
        </svg>

    </div>
    <div class="dz-error-mark">

        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
            <title>error</title>
            <desc>Created with Sketch.</desc>
            <defs></defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#EA4F4F" fill-opacity="0.816519475">
                    <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                </g>
            </g>
        </svg>

    </div>
    @unless(isset($hideActions) && $hideActions)
        <button @if(isset($song) and in_array($song->application->status, [\App\Application::STATE_SONGS_PAID, \App\Application::STATE_SUBMITTED])) disabled="disabled" @endif type="button" class="btn btn-default mts myRight mls myDelete" data-toggle="modal" @if(isset($song->id)) data-target="#myDeleteModal{{ $song->id }}"@else disabled @endif>
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
        </button>
        <button type="button" class="btn @if(!isset($song) or !$song->isValid()) btn-danger @else btn-default @endif mts myRight myEdit" data-toggle="modal" @if(isset($song->id)) data-target="#myModal{{ $song->id }}"@else disabled @endif @if($song and in_array($song->application->status, [\App\Application::STATE_SONGS_PAID, \App\Application::STATE_SUBMITTED])) disabled="disabled" @endif>
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Details<span style="font-weight:bold;">*</span>
        </button>
    @endunless

</div>
