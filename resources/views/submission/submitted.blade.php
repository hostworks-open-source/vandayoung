@extends(\App\Helpers\General::templateWithPrefix('layouts.home'))
@section('content')
    <div class="col-sm-12">
        <div class="panel panel-primary no-margin large-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Confirmation</h3>
            </div>
            <div class="panel-body">
                <p>Thank you {{ $user->firstname or '' }} for submitting your application. </p>
                <p>You can update the details of your account, but you will need to create a new application if you wish to submit additional works. </p>
            </div>
        </div>
    </div>
@endsection