<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';
    public $generalHelper;

    public function setUp(){
        $this->generalHelper = \Mockery::mock('overload:App\Helpers\General');
        $this->generalHelper->shouldReceive('templateWithPrefix')->andReturnUsing(function($templateName){
            return $templateName;
        });
        $this->generalHelper->shouldReceive('appsHaveClosed')->andReturnUsing(function(){
            return false;
        });
        parent::setUp();
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function secureUrl($uri){
        return $this->app['url']->secure(ltrim($uri, '/'));
    }
}
